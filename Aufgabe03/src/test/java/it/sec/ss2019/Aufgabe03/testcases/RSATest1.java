package it.sec.ss2019.Aufgabe03.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe03.Keys;
import it.sec.ss2019.Aufgabe03.RSA;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class RSATest1 extends Lib4Tests {
	
    final private String FILENAME = "RSA-Tests-1.txt";

    private GetTests tests = null;
    
//    @Test
//    public void madeToFail() {
//    	
//    	/*
//    	 * ERINNERUNG
//    	 */
//    	assertEquals(true, false, "Tests are passing, still needs a scrutiny");
//    	/*
//    	 * Simple encrypt() und decrypt() Methoden müssen angpasst werden
//    	 * (somit auch die Tests anschliessend)
//    	 * Keygen Methode muss noch angepasst werden
//    	 */
//    }
    
    @Test
    public void testRSA() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
    	
    	HashMap<String, String> cur = null;
    	
    	RSA rsa = new RSA();
    	
    	BigInt p;
    	BigInt q;
    	BigInt e;
    	    	
    	String strP = "";
    	String strQ = "";
    	String strE = "";
    	
    	Keys keys = null;
    	    	
    	int size = 0;

    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	// Wozu? 
        	size = Integer.parseInt(cur.get("s"));
        	
        	strP = cur.get("P");
        	strQ = cur.get("Q");
        	strE = cur.get("E");
        	
        	// Da wir "p" und "q" bekommen fuer den Test benoetigen 
        	p = BigInt.fromString(strP.charAt(0), strP.substring(1));
        	q = BigInt.fromString(strQ.charAt(0), strQ.substring(1));
        	e = BigInt.fromString(strE.charAt(0), strE.substring(1));
        	
        	
        	
        	// size hier ist ein bisschen sinnlos, da damit nicht gearbeitet wird
        	rsa = new RSA();
        	keys = rsa.generateRSAKeysMesser(p, q, e, size);

        	assertEquals(cur.get("F"), rsa.phi(p, q).toMesserStringNumber(), "Phi calculation test" + " lineNo=" + cur.get("Line"));
        	assertEquals(cur.get("D"), keys.privateKey().getD().toMesserStringNumber(), "D test" + " lineNo=" + cur.get("Line"));

        }
    	
    }

//    @Test
//    public void testRSAHex() {
//    	
//        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
//        Iterator<HashMap<String, String>> ti = tests.iterator();
//    	
//    	HashMap<String, String> cur = null;
//    	
//    	RSA rsa = new RSA();
//    	
//    	BigInt p;
//    	BigInt q;
//    	BigInt e;
//
//    	String strP = "";
//    	String strQ = "";
//    	String strE = "";
//    	
//    	String strF = "";
//    	String strD = "";
//    	
//    	Keys keys = null;
//    	
//    	int size = 0;
//
//    	
//        while (ti.hasNext()) {
//        	
//        	cur = ti.next();
//        	
//        	// Wozu? 
//        	size = Integer.parseInt(cur.get("s"));
//        	
//        	strP = reduceHexString(cur.get("p"));
//        	strQ = reduceHexString(cur.get("q"));
//        	strE = reduceHexString(cur.get("e"));
//
//        	
//        	p = BigInt.fromString16(strP.charAt(0), strP.substring(1));
//        	q = BigInt.fromString16(strQ.charAt(0), strQ.substring(1));
//        	e = BigInt.fromString16(strE.charAt(0), strE.substring(1));
//
//        	// size hier ist ein bisschen sinnlos, da damit nicht gearbeitet wird
//        	rsa = new RSA();
//        	keys = rsa.generateRSAKeysMesser(p, q, e, size);
//
//        	strF = reduceHexString(cur.get("f"));
//        	strD = reduceHexString(cur.get("d"));
//        	
//        	assertEquals(strF, rsa.phi(p, q).toMesserString16(), "Phi calculation test" + " lineNo=" + cur.get("Line"));
//        	assertEquals(strD, keys.privateKey().getD().toMesserString16(), "D test" + " lineNo=" + cur.get("Line"));
//
//        }
//
//    }

    
}
