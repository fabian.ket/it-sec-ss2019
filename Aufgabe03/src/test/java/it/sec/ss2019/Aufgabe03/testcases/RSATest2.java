package it.sec.ss2019.Aufgabe03.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe03.Block;
import it.sec.ss2019.Aufgabe03.Keys;
import it.sec.ss2019.Aufgabe03.RSA;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class RSATest2 extends Lib4Tests {
	
    final private String FILENAME = "RSA-Tests-2.txt";

    private GetTests tests = null;
    
//    @Test
//    public void madeToFail() {
//    	
//    	/*
//    	 * ERINNERUNG
//    	 */
//    	assertEquals(true, false, "Tests are passing, still needs a scrutiny");
//    	/*
//    	 * Simple encrypt() und decrypt() Methoden müssen angpasst werden
//    	 * (somit auch die Tests anschliessend)
//    	 * Keygen Methode muss noch angepasst werden
//    	 */
//    }
    
    @Test
    public void testRSA() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
    	
    	HashMap<String, String> cur = null;
    	
    	RSA rsa = new RSA();
    	
    	BigInt p;
    	BigInt q;
    	BigInt e;
    	BigInt plainBig;
    	    	
    	String strP = "";
    	String strQ = "";
    	String strE = "";
    	
    	String plain = "";
    	
    	Block block = null;
    	
    	
    	Keys keys = null;
    	
    	int size = 0;
    	
//    	String result = "";
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	       	
        	// Wozu? 
        	size = Integer.parseInt(cur.get("s"));
        	
        	strP = cur.get("P");
        	strQ = cur.get("Q");
        	strE = cur.get("E");
        	
        	// Da wir "p" und "q" bekommen fuer den Test benoetigen 
        	p = BigInt.fromString(strP.charAt(0), strP.substring(1));
        	q = BigInt.fromString(strQ.charAt(0), strQ.substring(1));
        	e = BigInt.fromString(strE.charAt(0), strE.substring(1));
        	
        	
        	// size hier ist ein bisschen sinnlos, da damit nicht gearbeitet wird
        	rsa = new RSA();
        	keys = rsa.generateRSAKeysMesser(p, q, e, size);

        	assertEquals(cur.get("D"), keys.privateKey().getD().toMesserStringNumber(), "D test" + " lineNo=" + cur.get("Line"));
        	
        	
        	// VERSCHLUESSELUNGS TESTS AB HIER
        	
        	plain = cur.get("G");
        	plainBig = BigInt.fromString(plain.charAt(0), plain.substring(1));
        	assertEquals(cur.get("H"), rsa.encryptRSA(keys.publicKey(), plainBig).toMesserStringNumber(), "First encryption test - G to H" + " lineNo=" + cur.get("Line"));

        	plain = cur.get("I");
        	plainBig = BigInt.fromString(plain.charAt(0), plain.substring(1));
        	assertEquals(cur.get("J"), rsa.encryptRSA(keys.publicKey(), plainBig).toMesserStringNumber(), "First encryption test - I to J" + " lineNo=" + cur.get("Line"));
        	
        	plain = cur.get("K");
        	plainBig = BigInt.fromString(plain.charAt(0), plain.substring(1));
        	assertEquals(cur.get("L"), rsa.encryptRSA(keys.publicKey(), plainBig).toMesserStringNumber(), "First encryption test - K to L" + " lineNo=" + cur.get("Line"));
			
        	
        	// FOR BLOCK
        	/*
        	plain = cur.get("G");
        	block = new Block(plain.substring(1));
        	assertEquals(cur.get("H"), rsa.encryptRSA(keys.publicKey(), block).toMesserString(), "First encryption test - G to H" + " lineNo=" + cur.get("Line"));

        	plain = cur.get("I");
        	block = new Block(plain.substring(1));
        	assertEquals(cur.get("J"), rsa.encryptRSA(keys.publicKey(), block).toMesserString(), "First encryption test - I to J" + " lineNo=" + cur.get("Line"));
        	
        	plain = cur.get("K");
        	block = new Block(plain.substring(1));
        	assertEquals(cur.get("L"), rsa.encryptRSA(keys.publicKey(), block).toMesserString(), "First encryption test - K to L" + " lineNo=" + cur.get("Line"));
			*/ 

        }
    	
    }
    
//    @Test
//    public void testRSAHex() {
//    	
//        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
//        Iterator<HashMap<String, String>> ti = tests.iterator();
//    	
//    	HashMap<String, String> cur = null;
//    	
//    	RSA rsa = new RSA();
//    	
//    	BigInt p;
//    	BigInt q;
//    	BigInt e;
//    	BigInt plainBig;
//    	
//    	
//    	String strP = "";
//    	String strQ = "";
//    	String strE = "";
//    	
//    	String plain = "";
//    	String cypher = "";
//    	
//    	Block block = null;
//    	
//    	Keys keys = null;
//    	
//    	int size = 0;
//
//    	
//        while (ti.hasNext()) {
//        	
//        	cur = ti.next();
//        	
//        	// Wozu? 
//        	size = Integer.parseInt(cur.get("s"));
//        	
//        	strP = reduceHexString(cur.get("p"));
//        	strQ = reduceHexString(cur.get("q"));
//        	strE = reduceHexString(cur.get("e"));
//
//        	
//        	p = BigInt.fromString16(strP.charAt(0), strP.substring(1));
//        	q = BigInt.fromString16(strQ.charAt(0), strQ.substring(1));
//        	e = BigInt.fromString16(strE.charAt(0), strE.substring(1));
//
//        	// size hier ist ein bisschen sinnlos, da damit nicht gearbeitet wird
//        	rsa = new RSA();
//        	keys = rsa.generateRSAKeysMesser(p, q, e, size);
//
//        	assertEquals(cur.get("D"), keys.privateKey().getD().toMesserStringNumber(), "D test" + " lineNo=" + cur.get("Line"));
//        	
//        	
//        	// VERSCHLUESSELUNGS TESTS AB HIER
//        	
//        	/*
//        	plain = reduceHexString(cur.get("g"));
//        	cypher = reduceHexString(cur.get("h"));
//        	plainBig = BigInt.fromString16(plain.charAt(0), plain.substring(1));
//        	assertEquals(cypher, rsa.encryptRSA(keys.publicKey(), plainBig).toMesserString16(), "First encryption hex test - G to H" + " lineNo=" + cur.get("Line"));
//
//        	plain = reduceHexString(cur.get("i"));
//        	cypher = reduceHexString(cur.get("j"));
//        	plainBig = BigInt.fromString16(plain.charAt(0), plain.substring(1));
//        	assertEquals(cypher, rsa.encryptRSA(keys.publicKey(), plainBig).toMesserString16(), "First encryption hex test - I to J" + " lineNo=" + cur.get("Line"));
//        	
//        	plain = reduceHexString(cur.get("k"));
//        	cypher = reduceHexString(cur.get("l"));
//        	plainBig = BigInt.fromString16(plain.charAt(0), plain.substring(1));
//        	assertEquals(cypher, rsa.encryptRSA(keys.publicKey(), plainBig).toMesserString16(), "First encryption hex test - K to L" + " lineNo=" + cur.get("Line"));
//        	*/
//        	
//        	
////        	plain = reduceHexString(cur.get("g"));
////        	cypher = reduceHexString(cur.get("h"));
////        	block = new Block(plain.substring(1));
////        	assertEquals(cypher, rsa.encryptRSA(keys.publicKey(), block).toMesserString16(), "First encryption hex test - G to H" + " lineNo=" + cur.get("Line"));
////
////        	plain = reduceHexString(cur.get("i"));
////        	cypher = reduceHexString(cur.get("j"));
////        	block = new Block(plain.substring(1));
////        	assertEquals(cypher, rsa.encryptRSA(keys.publicKey(), block).toMesserString16(), "First encryption hex test - I to J" + " lineNo=" + cur.get("Line"));
////        	
////        	plain = reduceHexString(cur.get("k"));
////        	cypher = reduceHexString(cur.get("l"));
////        	block = new Block(plain.substring(1));
////        	assertEquals(cypher, rsa.encryptRSA(keys.publicKey(), block).toMesserString16(), "First encryption hex test - K to L" + " lineNo=" + cur.get("Line"));
//
//
//        }
//    	
//
//    	
//    	
//    }
    

    
}
