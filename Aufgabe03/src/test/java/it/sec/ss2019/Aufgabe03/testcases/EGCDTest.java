package it.sec.ss2019.Aufgabe03.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class EGCDTest extends Lib4Tests {
	
    final private String FILENAME = "egcd-Tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testEGCD() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
    	
    	HashMap<String, String> cur = null;
    	
    	String strA = "";
    	String strB = "";
    	
    	BigInt a;
    	BigInt b;
    	
		BigInt[] result = new BigInt[3];
    	
        while (ti.hasNext()) {       	
        	
        	cur = ti.next();
        	
        	strA = cur.get("A");
        	strB = cur.get("B");

        	a = BigInt.fromString(strA.charAt(0), strA.substring(1));
        	b = BigInt.fromString(strB.charAt(0), strB.substring(1));
        	
        	
        	try {
				result = BigInt.egcdSimple(a, b);
			} catch (DivideByZeroException e) {
				e.printStackTrace();
			}

        	
        	assertEquals(cur.get("G"), result[0].toMesserStringNumber(), "Greatest Common Divisor Test" + " lineNo=" + cur.get("Line"));
        	assertEquals(cur.get("U"), result[1].toMesserStringNumber(), "Coefficient for 'A' Test" + " lineNo=" + cur.get("Line"));
        	assertEquals(cur.get("V"), result[2].toMesserStringNumber(), "Coefficient for 'B' Test" + " lineNo=" + cur.get("Line"));

        }
    }
    

    
}
