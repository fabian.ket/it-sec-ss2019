package it.sec.ss2019.Aufgabe03;


public class Block {
	
	private byte[] msg;
	
	
	public Block(String m) {
		this.msg = m.getBytes();
	}
	
	public Block(byte[] m) {
		this.msg = m;
	}
	
	public Block(byte[] m, int l) {
		this.msg = m;
	}
	
	
	public byte[] msg() {
		return msg;
	}
	
	public int length() {
		return msg.length;
	}
	
	@Override
	public String toString() {
		String re = "";
		for(int i = 0; i < length(); i++) {
			re += (char) msg[i];
		}
		return re;
	}
	
	/*
	 * TODO: MUSS UEBERPRUFEN, OB NEGATIV ODER POSITIV
	 */
	
	public String toMesserString() {
		
		String result = toString();
		
		if (result.startsWith("-")) {
			result = result.substring(1);
		}
		
		//  return ( (this.sign) ? "+" : "-") + result;
		return "+" + toString();
	}
	
	
	
	public String toMesserString16() {
		
		String result = toString();
//				
//		if (result.startsWith("-")) {
//			result = result.substring(1);
//		}
//		
//		return ( (this.sign) ? "+" : "-") + "0x" + result;
		return "+0x" + result;
	}

}
