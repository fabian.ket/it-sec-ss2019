
package it.sec.ss2019.Aufgabe03;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;

public class Main  {
	
	
    public static void main( String[] args ) {

    	
    	BigInt base = new BigInt(2, 2);
    	BigInt exponent = new BigInt(2, 3);
    	BigInt modulus = new BigInt(2, 85);
    	
		int size = 512;
    	
    	RSA rsa = new RSA();

//    	rsa.generateRSAKeys(e, size);
    	
    	
    	BigInt p = new BigInt(2, 5);
    	BigInt q = new BigInt(2, 17);
    	BigInt e = new BigInt(2, 3);
    	BigInt n = BigInt.mul(p, q);
    	BigInt d = BigInt.ZERO;
    	
    	BigInt msg = new BigInt(2, 2);
    	
		try {
			d = BigInt.invers(e, rsa.phi(p, q));
		} catch (DivideByZeroException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
    	
    	Keys keys = new Keys(p, q, n, e, d);
    	
//    	System.out.println(
//    			rsa.encryptRSA(keys.publicKey(), msg)
//    			);
//    	
//    	BigInt eMsg = rsa.encryptRSA(keys.publicKey(), msg);
//    	
//    	System.out.println(
//    			rsa.encryptRSA(keys.publicKey(), eMsg)
//    			);

    	
    	keys = rsa.generateRSAKeys(e, size);
    }
    
    
}

