package it.sec.ss2019.Aufgabe03;

import it.sec.ss2019.Aufgabe01.BigInt;

public class Keys {
	
	/*
	 * Die Struktur Keys beinhaltet: p, q, e, d, n;
	 * 		->	publicKey: e, n; 
	 * 		->	secretKey: p, q, d, n
	 */
	
	// Laut Vorlesung (Video)
	// "p", "q" und "d" ->	sind geheim
	// "n" und "e" 		->	sind einsehbar
	private PublicKey publicKey;
	private PrivateKey privateKey;
	
	public Keys(BigInt p, BigInt q, BigInt n, BigInt e, BigInt d) {
		publicKey = new PublicKey(e, n);
		privateKey = new PrivateKey(p, q, d, n);
	}
	
	public PublicKey publicKey() {
		return publicKey;
	}
	
	public PrivateKey privateKey() {
		return privateKey;
	}
	
	

}
