package it.sec.ss2019.Aufgabe03;

import it.sec.ss2019.Aufgabe01.BigInt;

public class PublicKey {
	
	private BigInt e;
	private BigInt n;
	
	public PublicKey(BigInt e, BigInt n) {
		this.e = e;
		this.n = n;
	}
	
	public BigInt getE() {
		return e;
	}
	
	public BigInt getN() {
		return n;
	}

}
