package it.sec.ss2019.Aufgabe03;

import it.sec.ss2019.Aufgabe01.BigInt;

public class PrivateKey {
	
	private BigInt p;
	private BigInt q;
	private BigInt d;
	private BigInt n;
	
	public PrivateKey(BigInt p, BigInt q, BigInt d, BigInt n) {
		this.p = p;
		this.q = q;
		this.d = d;
		this.n = n;
	}
	
	// Getter Methoden von der RSA Logik eher weniger sinvoll...
	// Aber wir brauchen die Werte...
	public BigInt getP() {
		return p;
	}
	
	public BigInt getQ() {
		return q;
	}
	
	public BigInt getD() {
		return d;
	}
	
	public BigInt getN() {
		return n;
	}

}
