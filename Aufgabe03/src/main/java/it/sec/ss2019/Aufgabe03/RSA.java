package it.sec.ss2019.Aufgabe03;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;

public class RSA {
	
	/*
	 * Methode für die Messer-Tests mit entsprechenden Anpassungen.
	 */
	public Keys generateRSAKeysMesser(BigInt p, BigInt q, BigInt e, int size) {
		
		// TODO:	ES WIRD NOCH NICHT MIT SIZE GEARBEITET
		// TODO:	PRIMZAHLEN MUESSEN ZUFAELLIG ERSTELLT WERDEN
		// 			DUERFEN AUSSERDEM NICHT GLEICH SEIN
		//			MAX. 30 BIT UNTERSCHIED (IN GROESSE)
		//			0.1 < abs(log2 p - log2 q) < 30
		// TODO:	WSH DAS "p" UND "q" KEINE PRIMZAHLEN SIND, SOLLTE KLEINER
		// 			ALS 2^(-100) SEIN
		// TODO:	KLEINSMOEGLICHER WERT FUER "e" IST 2^(16+1) = "65537"
		// TODO:	PRODUKT VON "p" UND "q" MUSS SO GROSS SEIN WIE "size", 
		//			ODER ETWAS GROESSER
		// TODO:	"phi" und "e" DUERFEN KEIN ggT HABEN, DA SONST "d" NICHT
		//			BESTIMMT WERDEN KANN

		BigInt n = BigInt.mul(p, q);
		BigInt phi = phi(p, q);
		
		// TODO: Implementieren!
		BigInt ggT = BigInt.gcdBin(e, phi);
		
		BigInt d = BigInt.ZERO;
		try {
			d = BigInt.invers(e, phi);
		} catch (DivideByZeroException dbz) {
			dbz.printStackTrace();
		}
		
		return new Keys(p, q, n, e, d);
	}
	
	// Real Deal (not for the Messer Tests)
	public Keys generateRSAKeys(BigInt e, int size) {
		
		// TODO:	KLEINSMOEGLICHER WERT FUER "e" IST (2^16)+1 = "65537"
		BigInt minE = new BigInt(2, 65537);
		if(BigInt.lt(e, minE)) {
			e = minE;
		}
		

		// TODO:	WSH DAS "p" UND "q" KEINE PRIMZAHLEN SIND, SOLLTE KLEINER
		// 			ALS 2^(-100) SEIN
		BigInt p = BigInt.ZERO;
		BigInt q = BigInt.ZERO;
		
		int bitDiff = 0;
		int maxBitDiff = 30;
		
		// TODO:	PRIMZAHLEN MUESSEN ZUFAELLIG ERSTELLT WERDEN
		// 			DUERFEN AUSSERDEM NICHT GLEICH SEIN
		//			MAX. 30 BIT UNTERSCHIED (IN GROESSE)
		//			0.1 < abs(log2 p - log2 q) < 30
		while(BigInt.eq(p, q)) {
			p = BigInt.getRandomOdd(size);
			q = BigInt.getRandomOdd(size);
			bitDiff = Math.abs(p.getBitCount() - q.getBitCount());
			if((bitDiff <= maxBitDiff && bitDiff > 0)) {
				p = BigInt.ZERO;
				q = BigInt.ZERO;
			}
		}
		System.out.println(p);
		System.out.println(q);
		
		
		// TODO:	PRODUKT VON "p" UND "q" MUSS SO GROSS SEIN WIE "size", 
		//			ODER ETWAS GROESSER
		BigInt n = BigInt.mul(p, q);
		
		BigInt phi = phi(p, q);
		
		// TODO:	"phi" und "e" DUERFEN KEIN ggT HABEN, DA SONST "d" NICHT
		//			BESTIMMT WERDEN KANN
		BigInt ggT = BigInt.gcdBin(e, phi);
		
		BigInt d = BigInt.ZERO;
		if(BigInt.eq(ggT, BigInt.ONE)) {
			try {
				d = BigInt.invers(e, phi);
			} catch (DivideByZeroException dbz) {
				dbz.printStackTrace();
			}
		}

		
		return new Keys(p, q, n, e, d);
	}
	
	public BigInt phi(BigInt p, BigInt q) {
		return BigInt.mul(BigInt.sub(p, BigInt.ONE), BigInt.sub(q, BigInt.ONE));
	}
	
	
	public PublicKey getPublicRSA(Keys k) {
		return k.publicKey();
	}
	
	// secretKey wurde PrivateKey
	public PrivateKey getSecretRSA(Keys k) {
		return k.privateKey();		
	}
	
	
	
	/*
	 * NUR DER EINFACHHEIT HALBER ERST EINMAL 
	 */
// #############################################################################	
	public BigInt encryptRSA(PublicKey p, BigInt plain) {		
		BigInt encrypted = BigInt.ZERO;
		try {
			encrypted = BigInt.powerMod(plain, p.getE(), p.getN());
		} catch (DivideByZeroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return encrypted;
	}
	
	public BigInt decryptRSA(PrivateKey p, BigInt cipher) {
		BigInt decrypted = BigInt.ZERO;
		try {
			decrypted = BigInt.powerMod(cipher, p.getD(), p.getN());
		} catch (DivideByZeroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return decrypted;
	}	
// #############################################################################	

	public Block encryptRSA(PublicKey p, Block plain) {
		Block block = null;
		
		try {
			block = powerMod(plain, p.getE(), p.getN());
		} catch (DivideByZeroException e) {
			e.printStackTrace();
		}

		return block;
	}

	public Block decryptRSA(PrivateKey p, Block cipher) {
		Block block = null;
		
		try {
			block = powerMod(cipher, p.getD(), p.getN());
		} catch (DivideByZeroException e) {
			e.printStackTrace();
		}

		return block;
	}
	
	public static Block powerMod(Block block, BigInt y, BigInt m) throws DivideByZeroException {
		// return divmod(power(x, y), m)[1];
		
		BigInt result = new BigInt(2, 1);
		
		// TODO: Irgendwie zu hacky, needs a fix ASAP
		BigInt tmp = BigInt.fromString(block.toString());
		
		while (BigInt.gt(y, BigInt.ZERO)) {
			if (BigInt.odd(y)) {
				result = BigInt.divmod(BigInt.mul(result, tmp), m)[1];
			}
			
			tmp = BigInt.divmod(BigInt.square(tmp), m)[1];
			y = BigInt.shiftRight(y);
		}
		
		return new Block(result.toString());
	}
	
}
