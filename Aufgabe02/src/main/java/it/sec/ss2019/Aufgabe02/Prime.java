package it.sec.ss2019.Aufgabe02;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;


/**
 * TODO: FRAGEN BEANTWORTEN
 * 
 * 1. Es werden die drei Tests (Fermat, Euler, Miller-Rabin) mit zufälligen Proben (und den Primzahlen bis 37 (siehe oben)) 
 *    sowie vorher die Probedivision mit allen Primzahlen unter 100 durchgeführt. Dies wird ca. 100 Mal mit (a) echten Primzahlen, 
 *    (b) Pseudoprimzahlen und (c) zusammengesetzten Zahlen durchgeführt. Bestimmen Sie in wie vielen Prozent der Fälle die 
 *    jeweiligen Tests irren bzw. richtig liegen.
 *    
 * 2. Müssen tatsächlich 50 Runden beim Miller-Rabin-Test durchgeführt werden? Lohnen sich die letzten 30 überhaupt? 
 *    Bestimmen Sie bei ca. 100 Versuchen, in wie vielen Fällen das Feststellen einer Nicht-Primzahl in den Bereichen 1-19, 20-29, 
 *    30-39 und 40 bis 50 Runden erfolgt. Dazu realisieren Sie auch die Fälle (a) bis (c).
 *
 */

public class Prime {
	
	
	
	public static interface Witnessable {
		boolean witness(BigInt n, int base);
		boolean witness(BigInt n, BigInt base);
	}

	
	public static boolean isPrimeFermat(BigInt prime, BigInt base) {
		Witnessable witnesser = new WitnessFermat();
		return testPrimeWithBase(prime, witnesser, base);
	}
	public static boolean isPrimeFermat(BigInt prime, BigInt[] bases) {
		Witnessable witnesser = new WitnessFermat();
		return testPrimeWithBases(prime, witnesser, bases);
	}
	
	public static boolean isPrimeEuler(BigInt prime, BigInt base) {
		Witnessable witnesser = new WitnessEuler();
		return testPrimeWithBase(prime, witnesser, base);
	}
	public static boolean isPrimeEuler(BigInt prime, BigInt[] bases) {
		Witnessable witnesser = new WitnessEuler();
		return testPrimeWithBases(prime, witnesser, bases);
	}
	
	public static boolean isPrimeMR(BigInt prime, BigInt base) {
		Witnessable witnesser = new WitnessMR();
		return testPrimeWithBase(prime, witnesser, base);
	}
	public static boolean isPrimeMR(BigInt prime, BigInt[] bases) {
		Witnessable witnesser = new WitnessMR();
		return testPrimeWithBases(prime, witnesser, bases);
	}
	
	public static boolean isPrimeDiv(BigInt prime, int base) {
		Witnessable witnesser = new WitnessDiv();
		return testPrimeWithBase(prime, witnesser, base);
	}
	public static boolean isPrimeDiv(BigInt prime, int[] bases) {
		Witnessable witnesser = new WitnessDiv();
		return testPrimeWithBases(prime, witnesser, bases);
	}
	public static boolean isPrimeDiv(BigInt prime, BigInt base) {
		Witnessable witnesser = new WitnessDiv();
		return testPrimeWithBase(prime, witnesser, base);
	}
	public static boolean isPrimeDiv(BigInt prime, BigInt[] bases) {
		Witnessable witnesser = new WitnessDiv();
		return testPrimeWithBases(prime, witnesser, bases);
	}
	
	
	public static boolean testPrimeWithBase(BigInt n, Witnessable f, int base) {
//		if (BigInt.eq(n, BigInt.ZERO) || BigInt.eq(n, BigInt.ONE))
//			return false;
//		if (BigInt.eq(n, BigInt.TWO) || BigInt.eq(n, BigInt.THREE))
//			return true;
//		if (BigInt.even(n))
//			return false;
		
		if (f.witness(n, base)) {
			return false;
		}
		
		return true;
	}
	public static boolean testPrimeWithBases(BigInt n, Witnessable f, int[] bases) {
		for (int base : bases) {
			if (!testPrimeWithBase(n, f, base)) {
				return false;
			}
		}
		
		return true;
	}
	
	
	public static boolean testPrimeWithBase(BigInt n, Witnessable f, BigInt base) {
//		if (BigInt.eq(n, BigInt.ZERO) || BigInt.eq(n, BigInt.ONE))
//			return false;
//		if (BigInt.eq(n, BigInt.TWO) || BigInt.eq(n, BigInt.THREE))
//			return true;
//		if (BigInt.even(n))
//			return false;
		
		if (f.witness(n, base)) {
			return false;
		}
		
		return true;
	}
	public static boolean testPrimeWithBases(BigInt n, Witnessable f, BigInt[] bases) {
		for (BigInt base : bases) {
			if (!testPrimeWithBase(n, f, base)) {
				return false;
			}
		}
		
		return true;
	}
	
	public static boolean testPrimeGlobal(BigInt n, int k, Witnessable f) {
		if (BigInt.eq(n, BigInt.ZERO) || BigInt.eq(n, BigInt.ONE))
			return false;
		if (BigInt.eq(n, BigInt.TWO) || BigInt.eq(n, BigInt.THREE))
			return true;
		if (BigInt.even(n))
			return false;
		
		for (int i = 0; i < k; ++i) {
			if (f.witness(n, null)) {
				return false;
			}
		}
		return true;
	}
	
	// a^(n-1) <=> 1 (mod n), a > 0, gcd(a,b) == 1, n == prime
	// a^n <=> a (mod n), a > 0, n == prime
	public static class WitnessFermat implements Witnessable {
		
		@Override
		public boolean witness(BigInt n, BigInt base) {
			
			if (BigInt.eq(n, BigInt.ZERO) || BigInt.eq(n, BigInt.ONE))
				return true;
			if (BigInt.eq(n, BigInt.TWO))
				return false;
			if (BigInt.eq(n, BigInt.THREE))
				return true;
			if (BigInt.eq(n, base))
				return false;
			
//			if (BigInt.even(n))
//			 	return true;
			
			if (BigInt.gt(n, BigInt.THREE)) {
				BigInt _a;
				
				if (base != null) { // && BigInt.gt(n, base)) {
					_a = base;
				} else {
					// TODO: currently only for one cell big BigInts
					// int a = rand.nextInt(n.get(0) - 3) + 2;
					_a = BigInt.add(BigInt.getRandomOdd(n.getBitCount() - 1), BigInt.TWO);
				}
				
//				if ( BigInt.gt(BigInt.gcdBin(_a, n), BigInt.ONE) )
//					return true;
				
				
				try {
					boolean result = !BigInt.eq(BigInt.powerMod(_a, BigInt.sub(n, 1), n), new BigInt(2, 1)); 
					return result;
				} catch(DivideByZeroException e) {
					
				} 
			}
			
			return false;
		}

		@Override
		public boolean witness(BigInt n, int base) {
			return witness(n, new BigInt(2, base));
		}
	}
	
	// a^((n - 1) / 2) <=> +- 1 (mod n)
	public static class WitnessEuler implements Witnessable {
		@Override
		public boolean witness(BigInt n, BigInt base) {
			
			if (BigInt.eq(n, BigInt.ZERO) || BigInt.eq(n, BigInt.ONE))
				return true;
			if (BigInt.eq(n, BigInt.TWO))
				return false;
			if (BigInt.eq(n, base))
				return false;
//			if (BigInt.even(n))
//				return true;
			
			BigInt _a;
			
			if (base != null) {
				_a = base;
			} else {
				// TODO: currently only for one cell big BigInts 
				// int a = rand.nextInt(Math.max(3, n.get(0) - 1) - 2) + 3;
				_a = BigInt.max(new BigInt(BigInt.THREE), BigInt.add(BigInt.getRandomOdd(n.getBitCount() - 1), BigInt.THREE));
			}
			
			if ( BigInt.eq(BigInt.gcdBin(_a, n), BigInt.ONE) ) {
				BigInt tmp;
				try {
					tmp = BigInt.powerMod(_a, BigInt.shiftRight(BigInt.sub(n,  BigInt.ONE)), n); // ((int)Math.pow(a, (n - 1) / 2)) % n;
				}
				catch (DivideByZeroException e) {
					tmp = null;
				}
				return !(BigInt.eq(tmp, BigInt.ONE) || BigInt.eq(tmp, BigInt.MINUS_ONE) || BigInt.eq(tmp, BigInt.sub(n, 1)));
			} else {
				return true;
			}
		}
		
		@Override
		public boolean witness(BigInt n, int base) {
			return witness(n, new BigInt(2, base));
		}
	}
	
	public static class WitnessMR implements Witnessable {

		private BigInt[] computeDS(BigInt n_minus_1) {
			BigInt d = new BigInt(n_minus_1);
			BigInt s = new BigInt(2, 0);
			
			while(BigInt.even(d)) {
				d = BigInt.shiftRight(d);
				s = BigInt.add(s, BigInt.ONE);
			}
			
			d.reduce();
			d.checkZero();
			
			s.reduce();
			s.checkZero();
			
			BigInt result[] = { d, s };
			return result;
		}
		
		@Override
		public boolean witness(BigInt n, BigInt base) { 
			
			if (BigInt.eq(n, BigInt.ZERO) || BigInt.eq(n, BigInt.ONE))
				return true;
			if (BigInt.eq(n, BigInt.TWO) || BigInt.eq(n, BigInt.THREE))
				return false;
			if (BigInt.even(n))
				return true;
			if (BigInt.eq(n, base))
				return false;
			
			BigInt _a;
			
			if (base != null) {
				_a = base;
			} else {
				// TODO: currently only for one cell big BigInts
				// int a = rand.nextInt(n.get(0) - 3) + 2;
				_a = BigInt.add(BigInt.getRandomOdd(n.getBitCount() - 1), BigInt.TWO);
			}
			
			if (!BigInt.eq(BigInt.gcdBin(_a, n), BigInt.ONE))
				return true;
			
			BigInt[] ds = computeDS(BigInt.sub(n, BigInt.ONE));
			BigInt d = ds[0];
			BigInt s = ds[1];
			
			BigInt val = null;
			
			try {
				val = BigInt.powerMod(_a, d, n);
			} catch (DivideByZeroException e) {

			}
			
			if (BigInt.eq(val, BigInt.ONE)) {
				return false;
			} else {
				for (BigInt i = new BigInt(2, 0); BigInt.lt(i, s); i = BigInt.add(i, BigInt.ONE)) {
					if (BigInt.eq(val, BigInt.sub(n, BigInt.ONE)) ||  BigInt.eq(val, BigInt.MINUS_ONE))
						return false;
					
					try {
						val = BigInt.powerMod(val, BigInt.TWO, n);
					} catch (DivideByZeroException e) {

					}
					
					if (BigInt.eq(val, BigInt.ONE))
						return true;
				}
				return true;
			}
		}
		
		@Override
		public boolean witness(BigInt n, int base) {
			return witness(n, new BigInt(2, base));
		}
	}
	
	public static class WitnessDiv implements Witnessable {

		@Override
		public boolean witness(BigInt n, BigInt base) {
			
			if (BigInt.eq(n, BigInt.ZERO) || BigInt.eq(n, BigInt.ONE))
				return true;
			if (BigInt.eq(n, BigInt.TWO) || BigInt.eq(n, BigInt.THREE))
				return false;
			if (BigInt.even(n))
				return true;
			if (BigInt.eq(n, base))
				return false;
			
			BigInt _a;
			
			if (base != null) {
				_a = base;
			} else {
				_a = BigInt.getRandomOdd(n.getBitCount() - 1);
			}
			
			try {
				if (BigInt.eq(BigInt.divmod(n, _a)[1], BigInt.ZERO)) {
					return true;
				} else {
					return false;
				}
			} catch(DivideByZeroException e) {
				
			}
			
			return false;
		}
		
		@Override
		public boolean witness(BigInt n, int base) {
			return witness(n, new BigInt(2, base));
		}
	}
	
	
	public static boolean witnessDeter(int n) {
		int limit = (int) Math.max(3, Math.floor(Math.sqrt(n)));
		
		for (int i = 3; i <= limit; i = i + 2) {
			if (n % i == 0)
				return true;
		}
		
		return false;
	}
	public static boolean testPrimeDeter(int n) {
		if (n == 0 || n == 1)
			return false;
		if (n == 2 || n == 3)
			return true;
		if (n % 2 == 0)
			return false;
		
		return !witnessDeter(n);
	}
}
