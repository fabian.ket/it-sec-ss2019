package it.sec.ss2019.Aufgabe02;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;

/**
 * Hello world!
 *
 */
public class Main {
    public static void main( String[] args ) {
        BigInt odd = BigInt.getRandomOdd(1564);
        System.out.println("Bitcount: " + odd.getBitCount());
        
        
        BigInt b1 = BigInt.fromString('+', "281474976710656");
        BigInt b2 = BigInt.fromString('-', "0");
        
        BigInt pow = BigInt.power(BigInt.fromString("13"), BigInt.fromString("30"));
        BigInt square = BigInt.square(BigInt.fromString("10"));
        
        BigInt powMod = null;
        try {
			powMod = BigInt.powerMod(BigInt.fromString("13"), BigInt.fromString("30"), BigInt.fromString("15"));
		} catch (DivideByZeroException e) {}
        
        System.out.println(odd);
        System.out.println(b1);
        System.out.println(b2);
        System.out.println(pow);
        System.out.println(square);
        System.out.println(powMod);
        
        System.out.println("===================");
        
        System.out.println("Zero bits: " + odd.zeroBits());
        System.out.println("Zero bits: " + b1.zeroBits());
        System.out.println("Zero bits: " + b2.zeroBits());
        System.out.println("Zero bits: " + pow.zeroBits());
        System.out.println("Zero bits: " + square.zeroBits());
        System.out.println("Zero bits: " + powMod.zeroBits());
        
        System.out.println("===================");
        
        System.out.println(BigInt.even(odd));
        System.out.println(BigInt.even(b1));
        System.out.println(BigInt.even(b2));
        System.out.println(BigInt.even(pow));
        System.out.println(BigInt.even(square));
        System.out.println(BigInt.even(powMod));
        
        System.out.println("===================");
        
        BigInt rightShift = BigInt.fromString("18446744073709551616");
        rightShift = BigInt.shiftRight(rightShift);
        System.out.println(rightShift);
        
        rightShift = BigInt.shiftRight(rightShift, 15);
        System.out.println(rightShift);
        
        System.out.println("===================");
        
        BigInt gcd1 = BigInt.fromString("34856874365894354759834759835983458465845684594879457943875643875438756874356843658743658436458723648964324523453853452386453245234382645327845324328532457439875635457957");
        BigInt gcd2 = BigInt.fromString("7897147884844944894894979612361651984986461898464646734654328452345823547823547234523423845827345328645623845234523845238456345345723846254375938453498563442398423542847");
        
        System.out.println("GCD from gcd1 and gcd2 is: " + BigInt.gcdBin(gcd1, gcd2));
        
        
        Prime.WitnessFermat wf = new Prime.WitnessFermat();
        Prime.WitnessEuler we = new Prime.WitnessEuler();
        Prime.WitnessMR wmr = new Prime.WitnessMR();
        
        //    97
        //   563
        //  1021
        //  1543
        //  2557
        //  5023
        // 10061
        // 22003
        // 56533
        // 65521
        
        // non primes
        // 75361
        BigInt number = BigInt.fromString("75361");
        int tests = 10;
        boolean result;
        
        
//        result = Prime.testPrimeGlobal(number, tests, wf);
//        System.out.println("Number: " + number + " is prime: " + result + " fermat");
        
//        result = Prime.testPrimeGlobal(number, tests, we);
//        System.out.println("Number: " + number + " is prime: " + result + " euler");
        
//        result = Prime.testPrimeGlobal(number, tests, wmr);
//        System.out.println("Number: " + number + " is prime: " + result + " miller-rabin");
        
        
//        System.out.println("Number: " + number + " is prime: " + Prime.isPrimeFermat(number, (BigInt)null) + " fermat");
//        System.out.println("Number: " + number + " is prime: " + Prime.isPrimeEuler(number, (BigInt)null) + " euler");
//        System.out.println("Number: " + number + " is prime: " + Prime.isPrimeMR(number, (BigInt)null) + " miller-rabin");
        
        
        System.out.println("====================================");
        
        BigInt noPrime = BigInt.fromString("449065");
        BigInt prime = BigInt.fromString("899809343");
        
        BigInt noPrime_minus_1 = BigInt.sub(noPrime, 1);
        
        BigInt[] bases = new BigInt[12]; 
        bases[0x0] = BigInt.fromString("2");
        bases[0x1] = BigInt.fromString("3");
        bases[0x2] = BigInt.fromString("5");
        bases[0x3] = BigInt.fromString("7");
        bases[0x4] = BigInt.fromString("11");
        bases[0x5] = BigInt.fromString("13");
        bases[0x6] = BigInt.fromString("17");
        bases[0x7] = BigInt.fromString("19");
        bases[0x8] = BigInt.fromString("23");
        bases[0x9] = BigInt.fromString("29");
        bases[0xA] = BigInt.fromString("31");
        bases[0xB] = BigInt.fromString("37");
        
        
        // bases[4] = BigInt.fromString("879192541");
        // bases[5] = BigInt.fromString("1662803");
        
//        System.out.println("Bases test: " + Prime.isPrimeFermat(noPrime, bases));
//        System.out.println("Global test: " + Prime.testPrimeGlobal(noPrime, 20, new Prime.WitnessFermat()));
        
        
        try {
			System.out.println(BigInt.powerMod(BigInt.fromString("1662803"), noPrime_minus_1, noPrime));
			
//			BigInt power = BigInt.power(bases[5], noPrime_minus_1);
//			BigInt mod = BigInt.divmod(power, noPrime)[1];
//			
//			System.out.println(power);
//			System.out.println(mod);
			
		} catch (DivideByZeroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        BigInt[] arr = {
    			new BigInt(2, 2),
    			new BigInt(2, 3),
    			new BigInt(2, 5),
    			new BigInt(2, 7),
    			new BigInt(2, 11),
    			new BigInt(2, 13),
    			new BigInt(2, 17),
    			new BigInt(2, 19),
    			new BigInt(2, 23),
    			new BigInt(2, 29),
    			new BigInt(2, 31),
    			new BigInt(2, 37),
	
    	};
        
        BigInt bigPrime = BigInt.fromString("436045695945694506456459643843593469457065796945844043");
        
        System.out.println("Prime: " + Prime.isPrimeEuler(new BigInt(2, 13), arr));
        System.out.println("BigPrime: " + Prime.isPrimeEuler(bigPrime, arr));
        
        
    }
}
