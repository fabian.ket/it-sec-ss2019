package it.sec.ss2019.Aufgabe02.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class GcdTest extends Lib4Tests {
	final private String FILENAME = "gcd-Tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testGcd() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
    	HashMap<String, List<String>> cur = null;
    	
    	String strA;
    	char signA;
    	
    	String strB;
    	char signB;
    	
    	BigInt a;
    	BigInt b;
    	
    	String strG;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	strA = cur.get("A").get(0);
        	signA = strA.charAt(0);
        	strA = strA.substring(1);
        	
        	strB = cur.get("B").get(0);
        	signB = strB.charAt(0);
        	strB = strB.substring(1);
        	
        	a = BigInt.fromString(signA, strA);
        	b = BigInt.fromString(signB, strB);

        	strG = reduceHexString(cur.get("g").get(0));
        	
        	assertEquals(strG, BigInt.gcdBin(a, b).toMesserString16(), "gcd test" + " lineNo=" + cur.get("Line"));
        }
    	
    }
}
