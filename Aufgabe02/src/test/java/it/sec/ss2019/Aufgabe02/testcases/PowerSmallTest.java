package it.sec.ss2019.Aufgabe02.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class PowerSmallTest extends Lib4Tests {
	final private String FILENAME = "Power-Small-Tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testPowerSmall() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
    	HashMap<String, List<String>> cur = null;
    	
    	String strA;
    	char signA;
    	
    	BigInt a;
    	
    	String strB;
    	String strC;
    	String strD;
    	String strE;
    	String strF;
    	String strG;
    	String strH;
    	String strI;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	strA = cur.get("A").get(0);
        	signA = strA.charAt(0);
        	strA = strA.substring(1);
        	
        	a = BigInt.fromString(signA, strA);

        	strB = reduceHexString(cur.get("b").get(0));
        	strC = reduceHexString(cur.get("c").get(0));
        	strD = reduceHexString(cur.get("d").get(0));
        	strE = reduceHexString(cur.get("e").get(0));
        	strF = reduceHexString(cur.get("f").get(0));
        	strG = reduceHexString(cur.get("g").get(0));
        	strH = reduceHexString(cur.get("h").get(0));
        	strI = reduceHexString(cur.get("i").get(0));
        	
        	assertEquals(strB, BigInt.power(a, 0).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        	assertEquals(strC, BigInt.power(a, 1).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        	assertEquals(strD, BigInt.power(a, 2).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        	assertEquals(strE, BigInt.power(a, 3).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        	assertEquals(strF, BigInt.power(a, 4).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        	assertEquals(strG, BigInt.power(a, 5).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        	assertEquals(strH, BigInt.power(a, 6).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        	assertEquals(strI, BigInt.power(a, 7).toMesserString16(), "small power test" + " lineNo=" + cur.get("Line"));
        }
    	
    }
}