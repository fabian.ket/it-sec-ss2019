package it.sec.ss2019.Aufgabe02.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe02.Prime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class FermatPseudoPrimeTest extends Lib4Tests {
	
    final private String FILENAME = "FermatPseudoPrime-Tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testFermatPseudoPrime() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
    	HashMap<String, List<String>> cur = null;
    	
    	String strPseudoPrime;
    	char signPseudoPrime;
    	
    	char signBase;
    	
    	BigInt pseudoPrime;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	strPseudoPrime = cur.get("P").get(0);
        	signPseudoPrime = strPseudoPrime.charAt(0);
        	strPseudoPrime = strPseudoPrime.substring(1);
        	
        	List<BigInt> bases = new ArrayList<>();
        	
        	for (String strBase : cur.get("A")) {
            	signBase = strBase.charAt(0);
            	strBase = strBase.substring(1);
            	bases.add(BigInt.fromString(signBase, strBase));
        	}        	
        	
        	pseudoPrime = BigInt.fromString(signPseudoPrime, strPseudoPrime);
        	
        	BigInt[] baseArray = bases.toArray(new BigInt[0]);
        	
        	// TODO: ask about test 16
        	boolean result = (cur.get("t").get(0).equals("Fermat-PseudoPrime-16")) ? false : true;
        	
        	assertEquals(result, Prime.isPrimeFermat(pseudoPrime, baseArray), "pseudo prime test fermat" + " lineNo=" + cur.get("Line"));
        	// assertEquals(true, Prime.testPrimeGlobal(pseudoPrime, 20, new Prime.WitnessFermat()), "pseudo prime test fermat" + " lineNo=" + cur.get("Line"));
        }
    	
    }
    

    
}
