package it.sec.ss2019.Aufgabe02.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class PowerModPrimeTest extends Lib4Tests {
    final private String FILENAME = "PowerMod-Prim-Tests.txt";
    private GetTests tests = null;
    
    @Test
    public void testPowerModPrim() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
    	HashMap<String, List<String>> cur = null;
    	
    	String strA;
    	char signA;
    	
    	String strB;
    	char signB;
    	
    	String strM;
    	char signM;
    	
    	String strC;
    	char signC;
    	
    	BigInt a;
    	BigInt b;
    	BigInt m;
    	BigInt c;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	strA = cur.get("A").get(0);
        	signA = strA.charAt(0);
        	strA = strA.substring(1);
        	
        	strB = cur.get("B").get(0);
        	signB = strB.charAt(0);
        	strB = strB.substring(1);
        	
        	strM = cur.get("M").get(0);
        	signM = strM.charAt(0);
        	strM = strM.substring(1);
        	
        	strC = cur.get("C").get(0);
        	signC = strC.charAt(0);
        	strC = strC.substring(1);    	
        	
        	a = BigInt.fromString(signA, strA);
        	b = BigInt.fromString(signB, strB);
        	m = BigInt.fromString(signM, strM);
        	c = BigInt.fromString(signC, strC);
        	
        	try {
				assertEquals(c.toString(), BigInt.powerMod(a, b, m).toString(), "power mod prime test" + " lineNo=" + cur.get("Line"));
			} catch (DivideByZeroException e) {
				assertTrue(false);
			}
        }
    }
}
