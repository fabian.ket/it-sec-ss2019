/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe02.testcases;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
public class TestLine {
    // abc = data EOLN
    public String name;
    public String data;
    public int lineNo;
    
    TestLine(String name,String data, int lineNo) {
        this.name= name;
        this.data= data;
        this.lineNo= lineNo;
    }
    TestLine(String name,String data) {
        this.name= name;
        this.data= data;
        this.lineNo= 0;
    }
}
