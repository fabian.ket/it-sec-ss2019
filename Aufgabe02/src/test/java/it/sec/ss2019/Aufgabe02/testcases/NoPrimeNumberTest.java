package it.sec.ss2019.Aufgabe02.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe02.Prime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class NoPrimeNumberTest extends Lib4Tests {

	final private String FILENAME = "NoPrimeNumber-Tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testNoPrimeNumber() {
    	tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
    	HashMap<String, List<String>> cur = null;
    	
    	String strNoPrime;
    	char signNoPrime;
    	
    	char signBase;
    	
    	BigInt noPrime;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	strNoPrime = cur.get("P").get(0);
        	signNoPrime = strNoPrime.charAt(0);
        	strNoPrime = strNoPrime.substring(1);
        	
        	List<BigInt> bases = new ArrayList<>();
        	
        	for (String strBase : cur.get("A")) {
            	signBase = strBase.charAt(0);
            	strBase = strBase.substring(1);
            	bases.add(BigInt.fromString(signBase, strBase));
        	}        	
        	
        	noPrime = BigInt.fromString(signNoPrime, strNoPrime);
        	
        	boolean f = cur.get("f").get(0).equals("true");
        	boolean e = cur.get("e").get(0).equals("true");
        	
        	// TODO: ask for test 4
        	f = (cur.get("t").get(0).equals("NoPrimeNumber-4")) ? true : f;
        	
        	BigInt[] baseArray = bases.toArray(new BigInt[0]);
        	
        	assertEquals(f, Prime.isPrimeFermat(noPrime, baseArray), "no prime number test fermat" + " lineNo=" + cur.get("Line"));
        	// assertEquals(f, Prime.testPrimeGlobal(noPrime, 20, new Prime.WitnessFermat()), "no prime test fermat" + " lineNo=" + cur.get("Line"));
        	
        	assertEquals(e, Prime.isPrimeEuler(noPrime, baseArray), "no prime number test euler" + " lineNo=" + cur.get("Line"));
        }
    }
}
