package it.sec.ss2019.Aufgabe02.testcases;


import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class SquareTest extends Lib4Tests {
    final private String FILENAME = "Square-Tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testSquare() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
    	HashMap<String, List<String>> cur = null;
    	
    	String strA;
    	char signA;
    	
    	String strB;
    	char signB;
    	
    	String strC;
    	char signC;
    	
    	String strD;
    	char signD;
    	
    	String strE;
    	char signE;
    	
    	String strF;
    	char signF;
    	
    	String strG;
    	char signG;
    	
    	String strH;
    	char signH;
    	
    	BigInt a;
    	BigInt b;
    	BigInt c;
    	BigInt d;
    	BigInt e;
    	BigInt f;
    	BigInt g;
    	BigInt h;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	strA = cur.get("A").get(0);
        	signA = strA.charAt(0);
        	strA = strA.substring(1);
        	
        	strB = cur.get("B").get(0);
        	signB = strB.charAt(0);
        	strB = strB.substring(1);
        	
        	strC = cur.get("C").get(0);
        	signC = strC.charAt(0);
        	strC = strC.substring(1);
        	
        	strD = cur.get("D").get(0);
        	signD = strD.charAt(0);
        	strD = strD.substring(1);
        	
        	strE = cur.get("E").get(0);
        	signE = strE.charAt(0);
        	strE = strE.substring(1);
        	
        	strF = cur.get("F").get(0);
        	signF = strF.charAt(0);
        	strF = strF.substring(1);
        	
        	strG = cur.get("G").get(0);
        	signG = strG.charAt(0);
        	strG = strG.substring(1);
        	
        	strH = cur.get("H").get(0);
        	signH = strH.charAt(0);
        	strH = strH.substring(1);
        	
        	a = BigInt.fromString(signA, strA);
        	b = BigInt.fromString(signB, strB);
        	c = BigInt.fromString(signC, strC);
        	d = BigInt.fromString(signD, strD);
        	e = BigInt.fromString(signE, strE);
        	f = BigInt.fromString(signF, strF);
        	g = BigInt.fromString(signG, strG);
        	h = BigInt.fromString(signH, strH);
        	
        	a = BigInt.square(a);
        	assertEquals(b.toString(), a.toString(), "square test" + " lineNo=" + cur.get("Line"));
        	
        	a = BigInt.square(a);
        	assertEquals(c.toString(), a.toString(), "square test" + " lineNo=" + cur.get("Line"));
        	
        	a = BigInt.square(a);
        	assertEquals(d.toString(), a.toString(), "square test" + " lineNo=" + cur.get("Line"));
        	
        	a = BigInt.square(a);
        	assertEquals(e.toString(), a.toString(), "square test" + " lineNo=" + cur.get("Line"));
        	
        	a = BigInt.square(a);
        	assertEquals(f.toString(), a.toString(), "square test" + " lineNo=" + cur.get("Line"));
        	
        	a = BigInt.square(a);
        	assertEquals(g.toString(), a.toString(), "square test" + " lineNo=" + cur.get("Line"));
        	
        	a = BigInt.square(a);
        	assertEquals(h.toString(), a.toString(), "square test" + " lineNo=" + cur.get("Line"));

        }
    	
    }
}
