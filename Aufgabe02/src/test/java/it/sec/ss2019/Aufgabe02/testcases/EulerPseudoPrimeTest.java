package it.sec.ss2019.Aufgabe02.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe02.Prime;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class EulerPseudoPrimeTest extends Lib4Tests {
	
    final private String FILENAME = "EulerPseudoPrime-Tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testEulerPseudoPrime() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
    	HashMap<String, List<String>> cur = null;
    	
    	String strPseudoPrime;
    	char signPseudoPrime;
    	
    	char signBase;
    	
    	BigInt pseudoPrime;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	strPseudoPrime = cur.get("P").get(0);
        	signPseudoPrime = strPseudoPrime.charAt(0);
        	strPseudoPrime = strPseudoPrime.substring(1);
        	
        	List<BigInt> bases = new ArrayList<>();
        	
        	for (String strBase : cur.get("A")) {
            	signBase = strBase.charAt(0);
            	strBase = strBase.substring(1);
            	bases.add(BigInt.fromString(signBase, strBase));
        	}        	
        	
        	pseudoPrime = BigInt.fromString(signPseudoPrime, strPseudoPrime);
        	
        	BigInt[] baseArray = bases.toArray(new BigInt[0]);
        	
        	assertEquals(true, Prime.isPrimeEuler(pseudoPrime, baseArray), "pseudo prime test euler" + " lineNo=" + cur.get("Line"));
        }
    	
    }
    

    
}
