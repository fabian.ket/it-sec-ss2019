
package it.sec.ss2019.Aufgabe04;

//BYTES | Min: 73 Max: 129 Avg: 97.65625
//BYTES | [0]: 94 [17]: 83 [93]: 89 [113]: 100 [173]: 91 [203]: 96 [255]: 102


public class Main  {
    public static void main( String[] args ) {

    	
    	int nExamples = 30000;
    	double[] feq = BlumBlumShub.createByteFrequencyOverNExamples(nExamples);
    	BlumBlumShub.plotFrequency(feq, nExamples);
    	
    	
//    	min = Integer.MAX_VALUE;
//    	max = Integer.MIN_VALUE;
//    	avg = 0;
//    	
//    	frequency = new int[65536];
//    	
//    	for (int i = 0; i < 1000000; i++) {
//    		byte b0 = bbs.randomBBS();
//    		byte b1 = bbs.randomBBS();
//    		
//    		byte[] bytes = { b0, b1 };
//    		
//    		frequency[BlumBlumShub.bytesToShort(bytes) & 0xffff]++;
//    		
//    		if (i % 1000 == 0) {
//    			System.out.println(i / 1000000d * 100 + "% done");
//    		}
//    	}
//    	
//    	for (int i : frequency) {
//    		if (i < min)
//    			min = i;
//    		
//    		if (i > max)
//    			max = i;
//    		
//    		avg += i;
//    	}
//    	
//    	avg /= frequency.length;
//    	
//    	System.out.println("SHORTS | Min: " + min + " Max: " + max + " Avg: " + avg);
    }
}
