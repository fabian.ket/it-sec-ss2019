package it.sec.ss2019.Aufgabe04;

import javax.swing.JFrame;

import org.math.plot.Plot2DPanel;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;
import it.sec.ss2019.Aufgabe02.Prime;

public class BlumBlumShub {
	
	public static final BigInt ZERO    = new BigInt(2, 0);
	public static final BigInt ONE     = new BigInt(2, 1);
	public static final BigInt TWO     = new BigInt(2, 2);
	public static final BigInt THREE   = new BigInt(2, 3);
	public static final BigInt FOUR    = new BigInt(2, 4);
	
	private BigInt p;
	private BigInt q;
	private BigInt n;
	private BigInt seed;
	
	
	public BlumBlumShub() {
		prepare();
	}
	
	public BlumBlumShub(BigInt p, BigInt q, BigInt seed) {
		initRandomBBS(p, q, seed);
	}
	
	public BigInt getN() {
		return n;
	}
	
	
	public void initRandomBBS(BigInt p, BigInt q, BigInt seed) {
		
		// Startwert "s"
		// Bitlaenge "size" von "n = p * q"		
		// "n" sollte min. 200 Stellen besitzen (dezimal)
		// "p" und "q" ungefaehr gleich gross, aber nicht zu nah
		// z.B. 2 < p/q < 1000
		// "p - 1" und " p + 1" bzw "q - 1" und " q + 1"
		// sollten einen grossen Prim-Faktor haben, der groesser als 
		// die 4. Wurzel aus "n" ist
		
		this.p = p;
		this.q = q;
		this.n = BigInt.mul(p, q);
		this.seed = seed;

	}
	
	public void prepare() {
		try {
			p = nextPrime(BigInt.getRandomOdd(128), primesTestArray());
		} catch (DivideByZeroException e) {
			return;
		}
		
		do {
			try {
				q = nextPrime(BigInt.getRandomOdd(128), primesTestArray());
			} catch (DivideByZeroException e) {
				return;
			}
		} while (BigInt.eq(p, q));
		
		n = BigInt.mul(p, q);
		
		do {
			seed = BigInt.getRandomOdd(n.getBitCount() - 1);
		} while (!BigInt.eq(BigInt.gcdBin(seed, n), BigInt.ONE));
	}
	
	public byte randomBBS() {
		byte r = 0b0000_0000;
		
		for (int i = 0; i < Byte.SIZE; i++) {
			try {
				seed = BigInt.powerMod(seed, TWO, n);
			} catch (DivideByZeroException e) {
				return -1;
			}
			
			r |= (BigInt.odd(seed)) ? 0b1 << (Byte.SIZE - (i + 1)) : 0b0;
		}
		
		return r;
	}
	
	public static short shortFromBBSNumAndShort(BigInt xn, short n) {
		return (short) (((BigInt.odd(xn)) ? (n << 1) | 0b1 : (n << 1)) & 0xffff);  
	}
	
	public static String shortToBitString(short n) {
		String r = "";
		
		int mask = 0x8000;
		
		for (int i = 0; i < Short.SIZE; i++) {
			r += ((n & (mask >>> i)) != 0) ? "1" : (!r.equals("")) ? "0" : r; 
		}
		
		return r;
	}
	
	
	public BigInt calcNxtBBSNum(BigInt xn) {
		BigInt xn1 = new BigInt(ZERO);
		try { 
			xn1 = BigInt.powerMod(xn, TWO, n);  // divmod(BigInt.power(xn, TWO), n)[1];
		} catch (DivideByZeroException e) { e.printStackTrace(); }
		return xn1;
	}
	

	
	public static BigInt nextPrime(BigInt number, BigInt[] base) throws DivideByZeroException {
		BigInt tmp = new BigInt(number);
		
		while(true) {
			
			// Die "2" ist die einzige gerade Primzahl
			if(BigInt.odd(tmp) && BigInt.gt(tmp, number)) {
				if(modFourEqThree(tmp)) {
					if(Prime.isPrimeEuler(tmp, base)) {
						break;
					}
				} 
				tmp = BigInt.add(tmp, TWO);
			} else {
				tmp = BigInt.add(tmp, ONE);
			}
		}

		return tmp;
		
	}
	
	private static boolean modFourEqThree(BigInt value) {
		boolean check = false;
		try {
			check = BigInt.eq(BigInt.divmod(value, FOUR)[1], THREE);
		} catch (DivideByZeroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return check;
	}
	
	// Gibt einen String mit den gesetzten Bits, OHNE anfuehrende Nullen, zurueck
	public String lastBitsOfBBSNums(BigInt num, int count) {
		String re = "";
		BigInt tmp = new BigInt(num);
		
		tmp = calcNxtBBSNum(tmp);
		for(int i = 0; i < count; i++) {
			
			
			tmp = calcNxtBBSNum(tmp);
			re += BigInt.bit(tmp, 0);
//			System.out.println(i + " " + BigInt.bit(tmp, 0));
		}
		
		return re;
	}
	
//	public BigInt randomBBS() {
//		// liefert das naechste "pseudozufaellige byte"
//		byte pseudorandom = 0;
//		
//		return pseudorandom;
//	}

	
	
	// #########
	// Hier extra Methoden (nicht auf der Folie)
	// kleinste gemeinsame Vielfache: kgV(a,b) = | a * b | / ggT(a,b)
	public BigInt kgV(BigInt a, BigInt b) {
		BigInt tmp = null;
		try {
			tmp = BigInt.divmod(BigInt.mul(a, b).toAbsolute(), BigInt.gcdBin(a, b))[0];
		} catch (DivideByZeroException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tmp;
	}
	
	
    public static BigInt[] primesTestArray() {
    	BigInt[] arr = {
    			new BigInt(2, 2),
    			new BigInt(2, 3),
    			new BigInt(2, 5),
    			new BigInt(2, 7),
    			new BigInt(2, 11),
    			new BigInt(2, 13),
    			new BigInt(2, 17),
    			new BigInt(2, 19),
    			new BigInt(2, 23),
    			new BigInt(2, 29),
    			new BigInt(2, 31),
    			new BigInt(2, 37),
    	};
    	
    	return arr;
    }
    
    public static short bytesToShort(byte[] bytes) {
    	short r = 0;
    	
    	r |= (bytes[1] & 0xff) << Byte.SIZE;
    	r |= bytes[0] & 0xff;
    	
    	return r;
    }
    
    public static int bytesToInt(byte[] bytes) {
    	int r = 0;
    	
    	r |= (bytes[3] & 0xff << (Byte.SIZE * 3));
    	r |= (bytes[2] & 0xff << (Byte.SIZE * 2));
    	r |= (bytes[1] & 0xff << (Byte.SIZE * 1));
    	r |= (bytes[0] & 0xff);
    	
    	return r;
    }
    
    public static double[] createByteFrequencyOverNExamples(double n) {
    	double[] frequency = new double[256];
    	
    	BlumBlumShub bbs = new BlumBlumShub();
    	
    	for (int i = 0; i < n; i++) {
    		byte b = bbs.randomBBS();
    		
    		frequency[b & 0xff]++;
    		
    		if (i % 100 == 0) {
    			System.out.println(i / n * 100 + "% done");
    		}
    	}
    	
    	double min = Integer.MAX_VALUE;
    	double max = Integer.MIN_VALUE;
    	double avg = 0;
    	
    	for (double i : frequency) {
    		if (i < min)
    			min = i;
    		
    		if (i > max)
    			max = i;
    		
    		avg += i;
    	}
    	
    	avg /= frequency.length;
    	
    	System.out.println("BYTES | Min: " + min + " Max: " + max + " Avg: " + avg);
    	System.out.println("BYTES | [0]: " + frequency[0] + 
    			                 " [17]: " + frequency[17] + 
    			                 " [93]: " + frequency[93] +
    			                " [113]: " + frequency[113] +
    			                " [173]: " + frequency[173] +
    			                " [203]: " + frequency[203] +
    			                " [255]: " + frequency[255]);
    	
    	return frequency;
    }
    
    public static void plotFrequency(double[] frequency, int nr_examples) {
    	double[] x = new double[frequency.length];
    	for (int i = 0; i < x.length; i++) {
    		x[i] = i + 1;
    	}
    	
    	Plot2DPanel panel = new Plot2DPanel();
    	panel.addBarPlot("Line", x, frequency);
    	
    	JFrame  frame= new JFrame("Histogram over " + nr_examples + " examples");
    	frame.setContentPane(panel);
    	frame.setSize(1280, 720);
    	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    	frame.setVisible(true);
    }
}
