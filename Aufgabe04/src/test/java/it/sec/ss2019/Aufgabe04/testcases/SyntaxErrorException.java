/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe04.testcases;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
public class SyntaxErrorException extends RuntimeException {

    /**
     * Creates a new instance of <code>SyntaxErrorException</code> without detail
     * message.
     */
    public SyntaxErrorException() {
    }

    /**
     * Constructs an instance of <code>SyntaxErrorException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public SyntaxErrorException(final String msg) {
        super(msg);
    }
    
}
