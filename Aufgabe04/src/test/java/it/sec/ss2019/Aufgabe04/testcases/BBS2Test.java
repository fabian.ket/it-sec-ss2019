package it.sec.ss2019.Aufgabe04.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;
import it.sec.ss2019.Aufgabe04.BlumBlumShub;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class BBS2Test extends Lib4Tests {
	
    final private String FILENAME = "BBS-Tests-2.txt";

    private GetTests tests = null;
    
    @Test
    public void testBBS2() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	HashMap<String, List<String>> cur = null;  
    	HashMap<String, String> test = null;   
    	
    	String num = "";
    	String result = "";
    	
    	BigInt A = BigInt.ZERO;
    	BigInt P = BigInt.ZERO;
    	BigInt Q = BigInt.ZERO;
    	BigInt N = BigInt.ZERO;
    	BigInt fourForDivision = new BigInt(2, 4);
    	
    	BigInt[] primes = primesTestArray();
    	

        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	
        	num = cur.get("A").get(0);
        	System.out.println(cur.get("t"));
        	A = BigInt.fromString(num.charAt(0), num.substring(1));
        	

        	try {
        		
        		// Berechnung von "P" genau wie im BBS-1-Test
				P = BlumBlumShub.nextPrime(A, primes);
				assertEquals(cur.get("P").get(0), P.toMesserStringNumber(), "BBS Test 2 - P - Test" + " lineNo=" + cur.get("Line"));
				
				
				// Berechnung "Q" nach: "naechste Primzahl nach p + 25% (von p) mit %4=3"
				Q = BigInt.add(P, BigInt.divmod(P, fourForDivision)[0]);
				Q = BlumBlumShub.nextPrime(Q, primes);
				
				assertEquals(cur.get("Q").get(0), Q.toMesserStringNumber(), "BBS Test 2 - Q - Test" + " lineNo=" + cur.get("Line"));
				
				// Test der Multiplikation von "P*Q"
				N = BigInt.mul(P, Q);
				assertEquals(cur.get("N").get(0), N.toMesserStringNumber(), "BBS Test 2 - N - Test" + " lineNo=" + cur.get("Line"));

			} catch (DivideByZeroException e) { e.printStackTrace(); }
        	


        }
        System.out.println("Test finished");
    	
    }
    
    
    public BigInt[] primesTestArray() {
    	BigInt[] arr = {
    			new BigInt(2, 2),
    			new BigInt(2, 3),
    			new BigInt(2, 5),
    			new BigInt(2, 7),
//    			new BigInt(2, 11),
//    			new BigInt(2, 13),
//    			new BigInt(2, 17),
//    			new BigInt(2, 19),
//    			new BigInt(2, 23),
//    			new BigInt(2, 29),
//    			new BigInt(2, 31),
//    			new BigInt(2, 37),
	
    	};
    	
    	return arr;
    	
    }
    

    

    
}
