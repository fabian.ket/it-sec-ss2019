package it.sec.ss2019.Aufgabe04.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe04.BlumBlumShub;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class BBS3Test extends Lib4Tests {
	
    final private String FILENAME = "BBS-Tests-3.txt";

    private GetTests tests = null;
    
    @Test
    public void testBBS3() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	HashMap<String, List<String>> cur = null;  
    	
    	String ps = "";
    	String qs = "";
    	String sd = "";
    	String bytes = "";
    	
    	BigInt p = new BigInt(BigInt.ZERO);
    	BigInt q = new BigInt(BigInt.ZERO);
    	BigInt s = new BigInt(BigInt.ZERO);
    	
    	BigInt n = new BigInt(BigInt.ZERO);
    	
    	BlumBlumShub bbs = null;

        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	System.out.println(cur.get("t"));
        	
        	ps = cur.get("P").get(0);
        	qs = cur.get("Q").get(0);
        	sd = cur.get("R").get(0);
        	// Nur die Bitfolge ohne "+0b" am Anfang
        	bytes = cur.get("B").get(0).substring(3);
        	
        	System.out.println(bytes.length());
        	
        	p = BigInt.fromString(ps.charAt(0), ps.substring(1));
        	q = BigInt.fromString(qs.charAt(0), qs.substring(1));
        	s = BigInt.fromString(sd.charAt(0), sd.substring(1));
        	
        	bbs = new BlumBlumShub(p, q, s);
        	n = bbs.getN();
        	
        	assertEquals(cur.get("N").get(0), n.toMesserStringNumber(), "BBS Test 3 - N - Test" + " lineNo=" + cur.get("Line"));
        	
        	short bits = 0;
        	
        	for(int i = 0; i < Short.SIZE; i++) {
        		s = bbs.calcNxtBBSNum(s);
        		
        		bits = BlumBlumShub.shortFromBBSNumAndShort(s, bits);
        		
        		assertEquals(cur.get("Z").get(i), s.toMesserStringNumber(), "BBS Test 3 - Z - Test" + " lineNo=" + cur.get("Line"));
        	}
        	
        	
        	assertEquals(bytes, BlumBlumShub.shortToBitString(bits), "BBS Test 3 - Bits - Test" + " lineNo=" + cur.get("Line"));

        }
        System.out.println("Test finished");
    	
    }
    


}
