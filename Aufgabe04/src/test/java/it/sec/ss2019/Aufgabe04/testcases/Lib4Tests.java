
package it.sec.ss2019.Aufgabe04.testcases;

/*
 * Copyright (C) 2018 Burkhard Messer burkhard.messer@htw-berlin.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.HashMap;

/**
 *
 * @author Burkhard Messer burkhard.messer@htw-berlin.de
 */
public class Lib4Tests {

//    private static String path= null;
    protected final static String EOLN = System.getProperty("line.separator");
    
    // TODO: Note: Angepasst, damit hier alles ordentlich gefunden wird...
//    protected final static String PACKAGENAME= "de.htw_berlin.f4.itsec.bigint";
    protected final static String PACKAGENAME= "it.sec.ss2019.Aufgabe04";
    protected final static String DIRECTORY= "Files";

//    protected boolean between(int i, int minInclusive, int maxInclusive) {
//        return (i >= minInclusive && i <= maxInclusive);
//    }

//    private String correctData(String value, String base) {
//        // empty string is allowed
//        if(value.length()==0) {
//            return value;
//        }
//        char sign = '+';
//        int startindex = 0;
//        if (value.charAt(0) == '+') {
//            startindex = 1;
//        }
//        if (value.charAt(0) == '-') {
//            sign = '-';
//            startindex = 1;
//        }
//        if (value.substring(startindex, startindex + 1).equals(base)) {
//            startindex = startindex + 2;
//        }
//        if(base.equals("0x")) {
//            int len= value.substring(startindex).length();
//            if((len&1)==1) {
//                throw new RuntimeException("wrong test data: size hex value odd");
//            }
//        }
//        return sign + base + value.substring(startindex);
//    }
//
//    protected String correctHex(String value) {
//        return correctData(value, "0x");
//    }
//
//    protected String correctOctal(String value) {
//        return correctData(value, "0o");
//    }
    protected String reduceHexString(String hexString) {
        // +0x00000000yyyy to +0xyyyy
        int i;
        int len= hexString.length()-1;  // minimum is 1 char
        char sign= hexString.charAt(0);
        for(i=3; i<len;i++) {           // without +0x
            if(hexString.charAt(i)!='0') {
                break;
            }
        }
        return hexString.substring(0, 3)+hexString.substring(i);
    }
    
    protected boolean onlyTestCase(HashMap<String, String> cur,String name) {
        return cur.get("t").equals(name);
    }
}
