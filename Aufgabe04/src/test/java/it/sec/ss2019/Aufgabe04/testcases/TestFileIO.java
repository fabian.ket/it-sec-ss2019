/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe04.testcases;

import de.htw_berlin.f4.cb.io.IDataSource;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */

public class TestFileIO implements Iterator {
    private IDataSource ds = null;
    char[] chars= null;     // line
    private int col= 0;     // column
    private int max= 0;     // max index in chars
    private int lineNo= 0;  // number of line
    
    TestFileIO(IDataSource dsParam)  {
        ds = dsParam;
        skipComments();
    }
    private void skipBlanks() {
        if(col<=max) {
            while(chars[col]==' ') {
                col++;
                if(col>max) {
                    return;
                }
            }
        }
    }
    private void skipComments() {
        boolean comment= true;
        do {
            if(ds.eoi()) {
                return;    
            } 
            chars = ds.readLine();
            lineNo++;
            col= 0;
            max= chars.length-1;
            skipBlanks();
            if(col<=max) {
                comment= (chars[col]=='#');
            }
        } while (comment);
    }
    private String collectName() {
        int startcol= col;
        while(chars[col]!='=') {
            col++;
            if(col>max) {
                throw new SyntaxErrorException("syntax error at line "+lineNo);
            }
        }
        String name= new String(chars,startcol,col-startcol);
        col++; // skip '='
        return name.trim();
    }
    private String collectData() {
        // a=EOLN is allowed
        if(col>max) {
            return "";  // empty string
//            throw new SyntaxErrorException("syntax error at line "+lineNo);
        }
        String data= new String(chars,col,max-col+1);
        return data.trim();
    }
    
    @Override
    public boolean hasNext() {
        return !ds.eoi();
    }

    @Override
    public TestLine next() {
        if(ds.eoi()) {
            throw new NoSuchElementException();
        }
        TestLine result = new TestLine(collectName(),collectData(),lineNo);
        skipComments();
        return result;
    }
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
