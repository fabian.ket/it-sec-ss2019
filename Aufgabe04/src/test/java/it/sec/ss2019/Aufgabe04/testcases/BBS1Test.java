package it.sec.ss2019.Aufgabe04.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;
import it.sec.ss2019.Aufgabe04.BlumBlumShub;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class BBS1Test extends Lib4Tests {
	
    final private String FILENAME = "BBS-Tests-1.txt";

    private GetTests tests = null;
    
    @Test
    public void testBBS1() {

        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	HashMap<String, List<String>> cur = null;  
    	HashMap<String, String> test = null;   
    	
    	String num = "";
    	String result = "";
    	
    	BigInt number = BigInt.ZERO;
    	
    	BigInt[] primes = primesTestArray();
    	

        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	
//        	if(cur.get("t").get(0).equals("BBS-p-10")) {
            	num = cur.get("A").get(0);
            	System.out.println(cur.get("t"));
            	number = BigInt.fromString(num.charAt(0), num.substring(1));
            	

            	try {
            		
            		for(int i = 0; i < cur.get("P").size(); i++) {
        				number = BlumBlumShub.nextPrime(number, primes);
        				assertEquals(cur.get("P").get(i), number.toMesserStringNumber(), "BBS Test 1" + " lineNo=" + cur.get("Line"));
            		}

    			} catch (DivideByZeroException e) { e.printStackTrace(); }
//        	}
        	


        }
        System.out.println("Test finished");
    	
    }
    

    
    public BigInt[] primesTestArray() {
    	BigInt[] arr = {
    			new BigInt(2, 2),
    			new BigInt(2, 3),
    			new BigInt(2, 5),
    			new BigInt(2, 7),
//    			new BigInt(2, 11),
//    			new BigInt(2, 13),
//    			new BigInt(2, 17),
//    			new BigInt(2, 19),
//    			new BigInt(2, 23),
//    			new BigInt(2, 29),
//    			new BigInt(2, 31),
//    			new BigInt(2, 37),
	
    	};
    	
    	return arr;
    	
    }

    
}
