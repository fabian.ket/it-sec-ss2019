package it.sec.ss2019.Aufgabe05.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe05.Block;
import it.sec.ss2019.Aufgabe05.Hash256;
import it.sec.ss2019.Aufgabe05.Hash256er;
import it.sec.ss2019.Aufgabe05.IHasher;
import it.sec.ss2019.Aufgabe05.SHA2;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class SHATest extends Lib4Tests {
	
    final private String FILENAME = "sha256-tests.txt";

    private GetTests tests = null;
    
    @Test
    public void testSHA() {

    	
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        
    	IHasher hasher = new Hash256er();
    	SHA2 context = null;
    	
    	SHA2 sha2 = new SHA2();
    	Hash256 hash256 = null;
    	
    	String hexMsg = "";
    	
    	HashMap<String, String> cur = null;
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	context = hasher.hashInit();
        	
        	hexMsg = cur.get("p");

        	// Getting the message and converting it to HEX (necessary)
        	Block[] blocks = SHA2.getMsgBlocks(hexMsg);
        	
        	for (int i = 0; i < blocks.length - 1; ++i) {
        		hasher.hashUpdate(context, blocks[i]);
        	}
        	
        	hash256 = hasher.hashFinal(context, blocks[blocks.length - 1]);

        	
        	// Test for the hash of the message
        	String result = sha2.hash256(hexMsg, SHA2.INPUT_TYPE.HEX);
        	assertEquals(cur.get("h"), result, "sha256(message) test" + " lineNo=" + cur.get("Line"));
        	assertEquals(cur.get("h"), hash256.toString(), "sha256(message) test" + " lineNo=" + cur.get("Line"));


        	// Test for the hash of hash "sha256(sha256(...))"
        	result = sha2.hash256(result, SHA2.INPUT_TYPE.HEX);
        	String x0Weg = cur.get("d").substring(3);
        	assertEquals(x0Weg, result, "sha256(sha256(message)) test" + " lineNo=" + cur.get("Line"));


        }
    	
    }
    
}
