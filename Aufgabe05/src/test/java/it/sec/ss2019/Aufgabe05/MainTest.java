package it.sec.ss2019.Aufgabe05;

import de.htw_berlin.f4.cb.io.IDataSource;
import it.sec.ss2019.Aufgabe05.plongIO.DataFileTest;
import it.sec.ss2019.Aufgabe05.plongIO.FileNameInPackageTest;

public class MainTest {

	public static void main(String[] args) {
		Testcases tc = new Testcases();
		tc.testApp();
		
		 // Packagename sollte automatisch geholt werden...
		String packageName = "it.sec.ss2019.Aufgabe05";
		String filename = "sha256-tests.txt";
		
		FileNameInPackageTest fn = new FileNameInPackageTest(true, packageName);
		IDataSource ds = new DataFileTest(fn.getFileName(filename));

		while(!ds.eoi()) {
			String ln = ds.readLineAsString();
			System.out.print(ln + "\n");
		}
		

	}

}
