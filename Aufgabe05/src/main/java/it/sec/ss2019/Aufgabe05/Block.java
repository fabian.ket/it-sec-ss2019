package it.sec.ss2019.Aufgabe05;

public class Block {
    private int[] content = new int[16];

    public Block() { }

    private boolean indexOk(int index) {
        boolean result = true;
        if(index >= content.length || index < 0)
            result = false;
        return result;
    }

    public int get(int index) throws IndexOutOfBoundsException {
        if(!indexOk(index))
            throw new IndexOutOfBoundsException("Index: " + index + " is out of bounds!");
        return content[index];
    }

    public void set(int index, int value) throws IndexOutOfBoundsException {
        if(!indexOk(index))
            throw new IndexOutOfBoundsException("Index: " + index + " is out of bounds!");
        content[index] = value;
    }
}
