package it.sec.ss2019.Aufgabe05;

import it.sec.ss2019.Aufgabe05.SHA2.INPUT_TYPE;

public class Hash256er implements IHasher {
	
    @Override
    public SHA2 hashInit() {
        SHA2 sha2 = new SHA2();
        sha2.init_hash();

        return sha2;
    }

    @Override
    public void hashUpdate(SHA2 context, Block b) {
        context.msgScheduling(b);
        context.init_reg();
        context.msgCompression();
        context.computeHashValue();
    }

    @Override
    public Hash256 hashFinal(SHA2 context, Block b) {
        // TODO: Padding here???
        context.msgScheduling(b);
        context.init_reg();
        context.msgCompression();
        context.computeHashValue();

        return context.getHash();
    }

    @Override
    public Hash256 getHash(SHA2 context, String msg, INPUT_TYPE inType) { return context.stringToHash256(msg, inType); }
}
