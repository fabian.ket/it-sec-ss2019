package it.sec.ss2019.Aufgabe05;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Main {
    public static void main(String[] args) {
    	
		
			/*
			##########################################################################################
		 	## Alles was nicht in der ASCII Tabelle zu finden ist, an Zeichen, macht noch Probleme 	##
			##########################################################################################
			https://www.movable-type.co.uk/scripts/sha256.html
			*/
    	
    	// �etta reddast
    	
//    	hashCompare(empty, sha256_empty);
//    	hashCompare(abc, sha256_abc);
//    	hashCompare(abc_long, sha256_abc_long);
//
//    	hashCompare(franz, sha256_franz);
//    	hashCompare(frank, sha256_frank);
//    	
//    	hashCompare(sixtyFiveChars, sha256_sixtyFiveChars);
//    	hashCompare(sixtyFourChars, sha256_sixtyFourChars);
//    	hashCompare(sixtyThreeChars, sha256_sixtyThreeChars);
//    	
//    	
//    	hashCompare(bullshit, sha256_bullshit);
//    	hashCompare(theta_reddast, sha256_theta_reddast);
//    	
//    	
//    	
//    	System.out.println();
//    	System.out.println("	: " + hashStr);
//    	System.out.println("	: " + sha256_bullshit);
////    	System.out.println("	: " + sha256_theta_reddast);
//    	System.out.println(
////    			compareEquality(hashStr, sha256_theta_reddast)
//    			compareEquality(hashStr, sha256_bullshit)
//    			);
//    	
//    	
//    	
//    	String hasherString01 = "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKL";
//    	String hasherString02 = "MNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX";
//    	String hasherString03 = "YZjhglkhjfgjhlfgjlhglkfghjfgjhfgjhklfgjhlkfgjhklfgjlhkf";
    	String hasherString01 = "0000000000000000000000000000000000000000000000000000000000000000";
    	String hasherString02 = "000000000000000000000000000000000000000000000000";
    	
    	String abc = "abc";
//    	String bd = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    	String bd = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
//    	String bd = "d4817aa5497628e7c77e6b606107042bbba3130888c5f47a375e6179be789fbb";
//    	String bd = "ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad";
    	
    	System.out.println("+++++++++========+++++++++");
    	
    	IHasher hasher = new Hash256er();
    	Block block;
    	
    	SHA2 context = hasher.hashInit();
    	
    	
    	System.out.println();
//    	byte[] bytes = SHA2.stringToHex(hasherString01);
//    	byte[] bytes = SHA2.stringToHex(bd);
//    	block = context.bytesToBlock(bd.getBytes());
//    	context.msgScheduling(block);
//    	block = context.bytesToBlock(bytes);
//    	block = context.bytesToBlock(hasherString01.getBytes());
//    	System.out.println(context.getHash().toString());
//    	hasher.hashUpdate(context, block);
    	
//    	byte[] test = new byte[64];
//    	for(int i = 0; i < bd.getBytes().length; i++) {
//    		System.out.println( SHA2.intToByte(block.get(i)) );
//    	}
    	
    	
//    	bytes = SHA2.stringToHex(hasherString02);
//    	block = context.bytesToBlock(bytes);
//    	block = context.bytesToBlock(hasherString02.getBytes());
//    	System.out.println(context.getHash().toString());
//    	hasher.hashUpdate(context, block);
    	
//    	block = context.bytesToBlock(hasherString03.getBytes());
//    	System.out.println(context.getHash().toString());
    	
//    	byte[] bytes = SHA2.stringToHex(bd);
//    	block = context.bytesToBlock(bytes);
//    	System.out.println(context.getHash().toString());
//    	Hash256 hash256 = hasher.hashFinal(context, block);
//    	context.msgScheduling(block);
//    	hash256 = hasher.hashFinal(context, block);
//    	
//    	
//    	System.out.println(hash256);
//    	hasher.hashUpdate(context, block);
//    	System.out.println(hash256);
    	
    	
//    	System.out.println();
//    	context = hasher.hashInit();
//    	bytes = SHA2.stringToHex(hash256.toString());
//    	block = context.bytesToBlock(bytes);
//    	hash256 = hasher.hashFinal(context, block);
//    	System.out.println(hash256);
    	
    	String str109 = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    	String str110 = "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    	String str111 = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    	String str112 = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    	
    	String nistPDF = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
    	String randBS = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopqdkjfkjsdfgjhgfsdgfjsgfjhdsgfsdgfsdgfdsgfjhdsgfadsjgfjh";
    	
    	System.out.println(context.hash256(str109));
    	System.out.println(context.hash256(str110));
    	System.out.println(context.hash256(str111));
    	System.out.println(context.hash256(str112));
    	System.out.println(context.hash256(nistPDF, SHA2.INPUT_TYPE.STRING));
    	System.out.println(context.hash256(randBS, SHA2.INPUT_TYPE.STRING));
    	
    }
    
    public static void hashCompare(String message, String hashComparison) {
    	SHA2 sha2 = new SHA2();
    	String hash_msg = sha2.hash256(message, SHA2.INPUT_TYPE.STRING);
        System.out.println("+++++");
        System.out.println("Msg 	: " + message);
        System.out.println("Hash	: " + hash_msg);
        System.out.println("Comp	: " + hashComparison);
        System.out.println("Same	: " + compareEquality(hash_msg, hashComparison));
    }
    
    public static boolean compareEquality(String s1, String s2) {
    	boolean check = false;
    	if (s1 != null) {
    		check = s1.equals(s2);
    	}
    	return check;
    }
    
}
