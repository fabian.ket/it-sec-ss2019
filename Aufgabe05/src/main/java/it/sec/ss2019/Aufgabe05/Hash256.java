package it.sec.ss2019.Aufgabe05;

public class Hash256 {

    private int[] hash = new int[8];

    public Hash256(int[] hash) {
    	this.hash = hash.clone(); 
    }

    @Override
    public String toString() {
        return String.format("%08x",  hash[0]) +
                String.format("%08x", hash[1]) +
                String.format("%08x", hash[2]) +
                String.format("%08x", hash[3]) +
                String.format("%08x", hash[4]) +
                String.format("%08x", hash[5]) +
                String.format("%08x", hash[6]) +
                String.format("%08x", hash[7]);
    }
}
