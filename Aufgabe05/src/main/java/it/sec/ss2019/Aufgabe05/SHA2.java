package it.sec.ss2019.Aufgabe05;

public class SHA2 {
	
	public enum INPUT_TYPE { HEX, STRING };
	
	// Bits per Block / Bits per Byte 
	public static final int BLOCK_SIZE = 512 / 8;
	
    private int[] hash = new int[8];
    private int[] tmp_reg = new int[8];
    private int[] W = new int[64];
    
	private boolean padding1Set = false;
	private boolean paddingLengthSet = false;
	private long msgLength = 0;
    
    
    private static final int[] K = {
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
            0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
            0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
            0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
            0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
            0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
            0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
            0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };

    public void init_hash() {
        hash[0] = 0x6a09e667;
        hash[1] = 0xbb67ae85;
        hash[2] = 0x3c6ef372;
        hash[3] = 0xa54ff53a;
        hash[4] = 0x510e527f;
        hash[5] = 0x9b05688c;
        hash[6] = 0x1f83d9ab;
        hash[7] = 0x5be0cd19;
        
        padding1Set = false;
        paddingLengthSet = false;
        msgLength = 0;
    }

    public Hash256 getHash() { return new Hash256(hash); }
    
    public void init_reg() {
    	tmp_reg[0] = hash[0];
    	tmp_reg[1] = hash[1];
    	tmp_reg[2] = hash[2];
    	tmp_reg[3] = hash[3];
    	tmp_reg[4] = hash[4];
    	tmp_reg[5] = hash[5];
    	tmp_reg[6] = hash[6];
    	tmp_reg[7] = hash[7];
    }
    
    
    private int ch(int x, int y, int z) {
    	return (x & y) ^ ((~x) & z);
    }
    private int maj(int x, int y, int z) {
    	return (x & y) ^ (x & z) ^ (y & z);
    }
    
    
    private int BSIG0(int x) {
    	return rotr(2, x) ^ rotr(13, x) ^ rotr(22, x);
    }
    private int BSIG1(int x) {
    	return rotr(6, x) ^ rotr(11, x) ^ rotr(25, x);
    }
    
    
    private int SSIG0(int x) {
    	return rotr(7, x) ^ rotr(18, x) ^ shr(3, x);
    }
    private int SSIG1(int x) {
    	return rotr(17, x) ^ rotr(19, x) ^ shr(10, x);
    }
    
    
    private int shr(int n, int x) {
    	return x>>>n;
    }
    private int rotr(int n, int x) {
    	int w = 32;
    	return (x>>>n) | (x<<(w-n));
    }
    
    public int W(int index) { return W[index]; }

    public static Block[] getMsgBlocks(String m) {
    	return getMsgBlocks(m, INPUT_TYPE.HEX);
    }
    
    public static Block[] getMsgBlocks(String m, INPUT_TYPE inputType) {
    	
    	byte[] rawBytes = null;
    	
    	switch (inputType) {
    		case HEX: {
    			rawBytes = stringToHex(m);
    			break;
    		}
    		case STRING: {
    			rawBytes = m.getBytes();
    			break;
    		}
    	}
    	// byte[] rawBytes = m.getBytes();
    	// byte[] rawBytes = stringToHex(m);	
    	byte[] bytes =  new byte[rawBytes.length + 1];
    	
    	// add padding 1
    	for (int i = 0; i < bytes.length; i++) {
    		try {
    			bytes[i] = rawBytes[i];
    		} catch (IndexOutOfBoundsException e) {
    			bytes[i] = (byte) 0b1000_0000;
    		}
    	}
    	
    	// + 1 Byte Padding 1 & + 8 Byte length
    	int blockCount = ((bytes.length + 8) / BLOCK_SIZE);
    	blockCount = ( (bytes.length + 8) % BLOCK_SIZE == 0 ) ? blockCount : blockCount + 1;
    	
    	// ints per Block, 64 Byte / 4 Byte = 16
    	Block[] blocks = new Block[blockCount];

    	for (int i = 0; i < blocks.length; i++) {
    		blocks[i] = new Block();
		}
    	
    	for (int i = 0; i < blockCount; i++) {
    		int i0  = bytesToInt(getSubBytes(bytes,  0 + (i*BLOCK_SIZE),  4 + (i*BLOCK_SIZE)));
    		int i1  = bytesToInt(getSubBytes(bytes,  4 + (i*BLOCK_SIZE),  8 + (i*BLOCK_SIZE)));
    		int i2  = bytesToInt(getSubBytes(bytes,  8 + (i*BLOCK_SIZE), 12 + (i*BLOCK_SIZE)));
    		int i3  = bytesToInt(getSubBytes(bytes, 12 + (i*BLOCK_SIZE), 16 + (i*BLOCK_SIZE)));
    		int i4  = bytesToInt(getSubBytes(bytes, 16 + (i*BLOCK_SIZE), 20 + (i*BLOCK_SIZE)));
    		int i5  = bytesToInt(getSubBytes(bytes, 20 + (i*BLOCK_SIZE), 24 + (i*BLOCK_SIZE)));
    		int i6  = bytesToInt(getSubBytes(bytes, 24 + (i*BLOCK_SIZE), 28 + (i*BLOCK_SIZE)));
    		int i7  = bytesToInt(getSubBytes(bytes, 28 + (i*BLOCK_SIZE), 32 + (i*BLOCK_SIZE)));
    		int i8  = bytesToInt(getSubBytes(bytes, 32 + (i*BLOCK_SIZE), 36 + (i*BLOCK_SIZE)));
    		int i9  = bytesToInt(getSubBytes(bytes, 36 + (i*BLOCK_SIZE), 40 + (i*BLOCK_SIZE)));
    		int i10 = bytesToInt(getSubBytes(bytes, 40 + (i*BLOCK_SIZE), 44 + (i*BLOCK_SIZE)));
    		int i11 = bytesToInt(getSubBytes(bytes, 44 + (i*BLOCK_SIZE), 48 + (i*BLOCK_SIZE)));
    		int i12 = bytesToInt(getSubBytes(bytes, 48 + (i*BLOCK_SIZE), 52 + (i*BLOCK_SIZE)));
    		int i13 = bytesToInt(getSubBytes(bytes, 52 + (i*BLOCK_SIZE), 56 + (i*BLOCK_SIZE)));
    		int i14 = bytesToInt(getSubBytes(bytes, 56 + (i*BLOCK_SIZE), 60 + (i*BLOCK_SIZE)));
    		int i15 = bytesToInt(getSubBytes(bytes, 60 + (i*BLOCK_SIZE), 64 + (i*BLOCK_SIZE)));
    		
    		blocks[i].set( 0,  i0);
    	    blocks[i].set( 1,  i1);
    	    blocks[i].set( 2,  i2);
    	    blocks[i].set( 3,  i3);
    	    blocks[i].set( 4,  i4);
    	    blocks[i].set( 5,  i5);
    	    blocks[i].set( 6,  i6);
    	    blocks[i].set( 7,  i7);
    	    blocks[i].set( 8,  i8);
    	    blocks[i].set( 9,  i9);
    	    blocks[i].set(10, i10);
    	    blocks[i].set(11, i11);
    	    blocks[i].set(12, i12);
    	    blocks[i].set(13, i13);
    	    blocks[i].set(14, i14);
    	    blocks[i].set(15, i15);
    	}
    	
    	// add msg length in last 64 Bit
        long msgLength = rawBytes.length * 8L;
        blocks[blocks.length-1].set(14, (int)(msgLength >>> 32));
        blocks[blocks.length-1].set(15, (int)(msgLength >>>  0));

        return blocks;
    }
    
    // index0 included, index1 excluded
    public static byte[] getSubBytes(byte[] bytes, int index0, int index1) {

    	if(bytes == null)
    		return null;
    	
    	int size = index1 - index0;
    	
    	if(size <= 0)
    		return null;
    	
    	byte[] result = new byte[size];
    	
    	for(int i = 0; i < size; i++) {
    		try {
    			result[i] = bytes[i + index0];
    		} catch(IndexOutOfBoundsException e) {
    			
    			result[i] = 0x00;
    		}
    	}
    	
    	return result;
    }
    
    public void msgScheduling(Block M) {
    	for(int j = 0; j < W.length; j++) {
    		if(j < 16) {
    			W[j] = M.get(j);
    		} else {
    			W[j] = SSIG1(W[j - 2]) + W[j - 7] + SSIG0(W[j - 15]) + W[j - 16];
    		}
    	}
	}
    
    public void msgCompression() {
    	
    	int[] t = {0, 0};
    	
    	for(int i = 0; i < W.length; i++) {
    		t[0] = tmp_reg[7] + BSIG1(tmp_reg[4]) + ch(tmp_reg[4], tmp_reg[5], tmp_reg[6]) + K[i] + W[i];
    		t[1] = BSIG0(tmp_reg[0]) + maj(tmp_reg[0], tmp_reg[1], tmp_reg[2]);
    		tmp_reg[7] = tmp_reg[6];
    		tmp_reg[6] = tmp_reg[5];
    		tmp_reg[5] = tmp_reg[4];
    		tmp_reg[4] = tmp_reg[3] + t[0];
    		tmp_reg[3] = tmp_reg[2];
    		tmp_reg[2] = tmp_reg[1];
    		tmp_reg[1] = tmp_reg[0];
    		tmp_reg[0] = t[0] + t[1];
    	}
    	
    }
    
    public void computeHashValue() {
		hash[0] += tmp_reg[0];
		hash[1] += tmp_reg[1];
		hash[2] += tmp_reg[2];
		hash[3] += tmp_reg[3];
		hash[4] += tmp_reg[4];
		hash[5] += tmp_reg[5];
		hash[6] += tmp_reg[6];
		hash[7] += tmp_reg[7];
    }
    
    public static int bytesToInt(byte[] bytes) {
    	
    	int value = 0;
    	byte b;
    	
    	int shift = 24;
    	
    	for(int i = 0; i < 4; i++) {
    		try {
    			b = bytes[i];
    		} catch (IndexOutOfBoundsException ioobe) {
    			b = 0;
    		}
    		
    		value |= (b & 0xFF) << shift;
    		shift -= 8;
    	}
    	
    	return value;
    }
    
    public static byte[] intToByte(int i) {
    	return new byte[] {
    		(byte) (i >>> 24),
    		(byte) (i >>> 16),
    		(byte) (i >>>  8),
    		(byte) (i >>>  0)
    	};
    }
    
    // TODO: don't use this shit
    public Block bytesToBlock(byte[] bytes) {
    	Block block = new Block();
    	
    	if (bytes.length > 64)
    		return null;
    	
    	msgLength += bytes.length;
    	
    	if (bytes.length == 64) {
    		
    		int i0  = bytesToInt(getSubBytes(bytes,  0,  4));
    		int i1  = bytesToInt(getSubBytes(bytes,  4,  8));
    		int i2  = bytesToInt(getSubBytes(bytes,  8, 12));
    		int i3  = bytesToInt(getSubBytes(bytes, 12, 16));
    		int i4  = bytesToInt(getSubBytes(bytes, 16, 20));
    		int i5  = bytesToInt(getSubBytes(bytes, 20, 24));
    		int i6  = bytesToInt(getSubBytes(bytes, 24, 28));
    		int i7  = bytesToInt(getSubBytes(bytes, 28, 32));
    		int i8  = bytesToInt(getSubBytes(bytes, 32, 36));
    		int i9  = bytesToInt(getSubBytes(bytes, 36, 40));
    		int i10 = bytesToInt(getSubBytes(bytes, 40, 44));
    		int i11 = bytesToInt(getSubBytes(bytes, 44, 48));
    		int i12 = bytesToInt(getSubBytes(bytes, 48, 52));
    		int i13 = bytesToInt(getSubBytes(bytes, 52, 56));
    		int i14 = bytesToInt(getSubBytes(bytes, 56, 60));
    		int i15 = bytesToInt(getSubBytes(bytes, 60, 64));
    		
    		block.set( 0,  i0);
    		block.set( 1,  i1);
    		block.set( 2,  i2);
    		block.set( 3,  i3);
    		block.set( 4,  i4);
    		block.set( 5,  i5);
    		block.set( 6,  i6);
    		block.set( 7,  i7);
    		block.set( 8,  i8);
    		block.set( 9,  i9);
    		block.set(10, i10);
    		block.set(11, i11);
    		block.set(12, i12);
    		block.set(13, i13);
    		block.set(14, i14);
    		block.set(15, i15);
    	}
    	
    	else if ( ((bytes.length + 1 + 8) <= 64) && !padding1Set) {
    		
    		padding1Set = true;
    		paddingLengthSet = true;
    		
    		byte[] paddedBytes =  new byte[64];
        	
        	// add padding 1
        	for(int i = 0; i < bytes.length; i++) {
        		paddedBytes[i] = bytes[i];
        	}
        	
        	paddedBytes[bytes.length] = (byte) 0b1000_0000;
        	
        	long length = msgLength * 8L;
        	paddedBytes[56] = (byte)(length >>> 56);
        	paddedBytes[57] = (byte)(length >>> 48);
        	paddedBytes[58] = (byte)(length >>> 40);
        	paddedBytes[59] = (byte)(length >>> 32);
        	paddedBytes[60] = (byte)(length >>> 24);
        	paddedBytes[61] = (byte)(length >>> 16);
        	paddedBytes[62] = (byte)(length >>>  8);
        	paddedBytes[63] = (byte)(length >>>  0);
        	
        	int i0  = bytesToInt(getSubBytes(paddedBytes,  0,  4));
    		int i1  = bytesToInt(getSubBytes(paddedBytes,  4,  8));
    		int i2  = bytesToInt(getSubBytes(paddedBytes,  8, 12));
    		int i3  = bytesToInt(getSubBytes(paddedBytes, 12, 16));
    		int i4  = bytesToInt(getSubBytes(paddedBytes, 16, 20));
    		int i5  = bytesToInt(getSubBytes(paddedBytes, 20, 24));
    		int i6  = bytesToInt(getSubBytes(paddedBytes, 24, 28));
    		int i7  = bytesToInt(getSubBytes(paddedBytes, 28, 32));
    		int i8  = bytesToInt(getSubBytes(paddedBytes, 32, 36));
    		int i9  = bytesToInt(getSubBytes(paddedBytes, 36, 40));
    		int i10 = bytesToInt(getSubBytes(paddedBytes, 40, 44));
    		int i11 = bytesToInt(getSubBytes(paddedBytes, 44, 48));
    		int i12 = bytesToInt(getSubBytes(paddedBytes, 48, 52));
    		int i13 = bytesToInt(getSubBytes(paddedBytes, 52, 56));
    		int i14 = bytesToInt(getSubBytes(paddedBytes, 56, 60));
    		int i15 = bytesToInt(getSubBytes(paddedBytes, 60, 64));
    		
    		block.set( 0,  i0);
    		block.set( 1,  i1);
    		block.set( 2,  i2);
    		block.set( 3,  i3);
    		block.set( 4,  i4);
    		block.set( 5,  i5);
    		block.set( 6,  i6);
    		block.set( 7,  i7);
    		block.set( 8,  i8);
    		block.set( 9,  i9);
    		block.set(10, i10);
    		block.set(11, i11);
    		block.set(12, i12);
    		block.set(13, i13);
    		block.set(14, i14);
    		block.set(15, i15);
    	}
    	else if ( ((bytes.length + 1) <= 64) && !padding1Set ) {
    		padding1Set = true;
    		
    		byte[] paddedBytes =  new byte[64];
        	
        	// add padding 1
        	for(int i = 0; i < bytes.length; i++) {
        		paddedBytes[i] = bytes[i];
        	}
        	
        	paddedBytes[bytes.length] = (byte) 0b1000_0000;
        	
        	int i0  = bytesToInt(getSubBytes(paddedBytes,  0,  4));
    		int i1  = bytesToInt(getSubBytes(paddedBytes,  4,  8));
    		int i2  = bytesToInt(getSubBytes(paddedBytes,  8, 12));
    		int i3  = bytesToInt(getSubBytes(paddedBytes, 12, 16));
    		int i4  = bytesToInt(getSubBytes(paddedBytes, 16, 20));
    		int i5  = bytesToInt(getSubBytes(paddedBytes, 20, 24));
    		int i6  = bytesToInt(getSubBytes(paddedBytes, 24, 28));
    		int i7  = bytesToInt(getSubBytes(paddedBytes, 28, 32));
    		int i8  = bytesToInt(getSubBytes(paddedBytes, 32, 36));
    		int i9  = bytesToInt(getSubBytes(paddedBytes, 36, 40));
    		int i10 = bytesToInt(getSubBytes(paddedBytes, 40, 44));
    		int i11 = bytesToInt(getSubBytes(paddedBytes, 44, 48));
    		int i12 = bytesToInt(getSubBytes(paddedBytes, 48, 52));
    		int i13 = bytesToInt(getSubBytes(paddedBytes, 52, 56));
    		int i14 = bytesToInt(getSubBytes(paddedBytes, 56, 60));
    		int i15 = bytesToInt(getSubBytes(paddedBytes, 60, 64));
    		
    		block.set( 0,  i0);
    		block.set( 1,  i1);
    		block.set( 2,  i2);
    		block.set( 3,  i3);
    		block.set( 4,  i4);
    		block.set( 5,  i5);
    		block.set( 6,  i6);
    		block.set( 7,  i7);
    		block.set( 8,  i8);
    		block.set( 9,  i9);
    		block.set(10, i10);
    		block.set(11, i11);
    		block.set(12, i12);
    		block.set(13, i13);
    		block.set(14, i14);
    		block.set(15, i15);
    	}
    	else if (padding1Set && !paddingLengthSet) {
    		paddingLengthSet = true;
    		
    		byte[] paddedBytes =  new byte[64];
    		
    		long length = msgLength * 8L;
        	paddedBytes[56] = (byte)(length >>> 56);
        	paddedBytes[57] = (byte)(length >>> 48);
        	paddedBytes[58] = (byte)(length >>> 40);
        	paddedBytes[59] = (byte)(length >>> 32);
        	paddedBytes[60] = (byte)(length >>> 24);
        	paddedBytes[61] = (byte)(length >>> 16);
        	paddedBytes[62] = (byte)(length >>>  8);
        	paddedBytes[63] = (byte)(length >>>  0);
        	
        	int i0  = bytesToInt(getSubBytes(paddedBytes,  0,  4));
    		int i1  = bytesToInt(getSubBytes(paddedBytes,  4,  8));
    		int i2  = bytesToInt(getSubBytes(paddedBytes,  8, 12));
    		int i3  = bytesToInt(getSubBytes(paddedBytes, 12, 16));
    		int i4  = bytesToInt(getSubBytes(paddedBytes, 16, 20));
    		int i5  = bytesToInt(getSubBytes(paddedBytes, 20, 24));
    		int i6  = bytesToInt(getSubBytes(paddedBytes, 24, 28));
    		int i7  = bytesToInt(getSubBytes(paddedBytes, 28, 32));
    		int i8  = bytesToInt(getSubBytes(paddedBytes, 32, 36));
    		int i9  = bytesToInt(getSubBytes(paddedBytes, 36, 40));
    		int i10 = bytesToInt(getSubBytes(paddedBytes, 40, 44));
    		int i11 = bytesToInt(getSubBytes(paddedBytes, 44, 48));
    		int i12 = bytesToInt(getSubBytes(paddedBytes, 48, 52));
    		int i13 = bytesToInt(getSubBytes(paddedBytes, 52, 56));
    		int i14 = bytesToInt(getSubBytes(paddedBytes, 56, 60));
    		int i15 = bytesToInt(getSubBytes(paddedBytes, 60, 64));
    		
    		block.set( 0,  i0);
    		block.set( 1,  i1);
    		block.set( 2,  i2);
    		block.set( 3,  i3);
    		block.set( 4,  i4);
    		block.set( 5,  i5);
    		block.set( 6,  i6);
    		block.set( 7,  i7);
    		block.set( 8,  i8);
    		block.set( 9,  i9);
    		block.set(10, i10);
    		block.set(11, i11);
    		block.set(12, i12);
    		block.set(13, i13);
    		block.set(14, i14);
    		block.set(15, i15);
    	}
    	
    	return block;
    }
    
    public String hash256(String m) {
    	return hash256(m, INPUT_TYPE.HEX);
    }
    
    public String hash256(String m, INPUT_TYPE inType) {
    	return stringToHash256(m, inType).toString();
    }
    
    public static byte[] stringToHex(String str) {
    	
    	int size = str.length() / 2;
    	size = (str.length() % 2 == 0) ? size : size + 1;
    	
    	byte[] hexBytes = new byte[size];
    	
    	// TODO: uneven str length problem with substring IndexOutOfBounds
    	for (int i = 0; i < hexBytes.length - 1; i++) {
    		hexBytes[i] = (byte) Integer.parseInt(str.substring((i * 2), (i * 2) + 2), 16);
		}
    	
    	int i = hexBytes.length - 1;
    	hexBytes[i] = ( (str.length() % 2) != 0) ? (byte) Integer.parseInt(str.substring(i * 2, i * 2 + 1), 16) : (byte) Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16); 
    	// hexBytes[i] = (byte) Integer.parseInt(str.substring((i * 2), (i * 2) + 2), 16);
    	
    	return hexBytes;
    }
    
    public Hash256 stringToHash256(String m) {
    	return stringToHash256(m, INPUT_TYPE.HEX);
    }

	public Hash256 stringToHash256(String m, INPUT_TYPE inType) {

		init_hash();

		Block[] blocks = getMsgBlocks(m, inType);

		for(Block block : blocks) {
			msgScheduling(block);
			init_reg();
			msgCompression();
			computeHashValue();
		}

		return new Hash256(hash);
	}
}
