package it.sec.ss2019.Aufgabe05;

public interface IHasher {
	
	SHA2 hashInit();

    void hashUpdate(SHA2 context, Block b);

    Hash256 hashFinal(SHA2 context, Block b);

    Hash256 getHash(SHA2 context, String msg, SHA2.INPUT_TYPE inType);
}
