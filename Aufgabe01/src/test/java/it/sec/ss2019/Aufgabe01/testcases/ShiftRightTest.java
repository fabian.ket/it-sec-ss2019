/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe01.testcases;

import java.util.HashMap;
import java.util.Iterator;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
@RunWith(JUnitPlatform.class)
public class ShiftRightTest extends Lib4Tests {

    private final static String FILENAME = "Shift-Right-Tests.txt";

    private static GetTests tests = null;

    public ShiftRightTest() {
        
    }
    @BeforeAll
    public static void setUp() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
    }
    @AfterEach
    public void tearDown() {
        tests = null;
    }

    @Test
    public void testRightLeft() {
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            String result;
            HashMap<String, String> cur = ti.next();
            
            int size = Integer.valueOf(cur.get("s"));
            
            String hexA = cur.get("a");
            char signA = hexA.charAt(0);
            hexA = hexA.substring(1);
            
            BigInt a = BigInt.fromString16(signA, hexA);
            a = BigInt.shiftRight(a);
            result = a.toMesserString16(size);
            assertEquals(cur.get("b"), result, "ShiftRight test b" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftRight(a);
            result = a.toMesserString16(size);
            assertEquals(cur.get("c"), result, "ShiftRight test c" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftRight(a);
            result = a.toMesserString16(size);
            assertEquals(cur.get("d"), result, "ShiftRight test d" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftRight(a);
            result = a.toMesserString16(size);
            assertEquals(cur.get("e"), result, "ShiftRight test e" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftRight(a);
            result = a.toMesserString16(size);
            assertEquals(cur.get("f"), result, "ShiftRight test f" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftRight(a);
            result = a.toMesserString16(size);
            assertEquals(cur.get("g"), result, "ShiftRight test g" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftRight(a);
            result = a.toMesserString16(size);
            assertEquals(cur.get("h"), result, "ShiftRight test h" + " lineNo=" + cur.get("Line"));
        }
    }

}
