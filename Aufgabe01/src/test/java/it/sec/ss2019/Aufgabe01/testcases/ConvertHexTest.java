
package it.sec.ss2019.Aufgabe01.testcases;

import java.util.HashMap;
import java.util.Iterator;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.SizeOverflowException;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
@RunWith(JUnitPlatform.class)
public class ConvertHexTest extends Lib4Tests {

    final private String FILENAME = "Convert-Hex-Tests.txt";

    private GetTests tests = null;

    public ConvertHexTest() {
    }
    
//    @Disabled
    @Test
    public void testHex2Hex() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
//        System.out.println("####### test "+2048+" convert hex to hex");
//            System.out.println(cur);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
//            short sz = Short.valueOf(cur.get("s"));
            int size = Integer.valueOf(cur.get("s"));
            short sz = 2048;
            BigInt bi = new BigInt(sz, 0);
            // TODO: ANGEPASST
//            bi.toBigInt(cur.get("h"));
            String hexString = cur.get("h");
            char sign = hexString.charAt(0);
            hexString = hexString.substring(1);
            
            bi = BigInt.fromString16(sign, hexString);
            // TODO: CHANGED, Note for tracking
//            String result = bi.toString16(OutputFormat.hex);
            String result = bi.toMesserString16(size);
            assertEquals(cur.get("h"), result, "hex to hex test" + " lineNo=" + cur.get("Line"));
        }
    }
    
//    @Disabled
    @Test
    public void testDec2Hex() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
//        System.out.println("####### test "+2048+" convert hex to dec");
//            System.out.println(cur);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
            short sz = 2048;
//            if(!onlyTestCase(cur,"dec-Hex-7")) {
//                continue;
//            }
//            System.out.println("        test "+cur.get("t"));
            BigInt bi = new BigInt(sz, 0);
            
            // TODO: ANGEPASST
//            bi.toBigInt(cur.get("d"));
            int size = Integer.valueOf(cur.get("s"));
            String decString = cur.get("d");
            char sign = decString.charAt(0);
            decString = decString.substring(1);
            bi = BigInt.fromString(sign, decString);
            
            bi = BigInt.fromString(sign, decString);
            // TODO: CHANGED, Note for tracking
//            String result = bi.toString16(OutputFormat.hex);
            String result = bi.toMesserString16(size);
            assertEquals(cur.get("h"), result, "hex to dec test" + " lineNo=" + cur.get("Line"));
        }
    }
    
//    @Disabled
    @Test
    public void testDec2Dec() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
//        System.out.println("####### test "+2048+" convert dec to dec");
//            System.out.println(cur);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
            short sz = 2048;
//            if(!onlyTestCase(cur,"dec-Hex-11")) {
//                continue;
//            }
//            System.out.println("        test "+cur.get("t"));
            BigInt bi = new BigInt(sz, 0);
            // TODO: ANGEPASST
//            bi.toBigInt(cur.get("d"));
            
            String decString = cur.get("d");
            char sign = decString.charAt(0);
            decString = decString.substring(1);
            bi = BigInt.fromString(sign, decString);
            // TODO: CHANGED, Note for tracking
//            String result = bi.toString10();
            String result = bi.toMesserString();
            assertEquals(cur.get("d"), result, "dec to dec test" + " lineNo=" + cur.get("Line"));
        }
    }


    @Test
    public void testHexDecOverflow() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
//        System.out.println("####### test "+256+" convert hex to dec");
//            System.out.println(cur);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
            
            short sz = Short.valueOf(cur.get("s"));     //allocate 256 bit
            
            // test 14 must throw an exception
            if(!onlyTestCase(cur,"dec-Hex-14")) {
                continue;
            }
//            System.out.println("        test "+cur.get("t"));
            
            
            BigInt bi;
            try {
            	
            	// 
            	// 16 = (ARE YOU THE) EXPONENT_OF_BASE
            	int sizeBI = (sz/4) / 16;
            	
                bi = new BigInt(sizeBI, 0);
                // TODO: CHANGED, Note for tracking
//                bi.toBigInt(cur.get("d"));
                String value = cur.get("d");
//                char sign = value.charAt(0);
//                value = value.substring(1);

                //bi = BigInt.fromString(sign, decString);
                
                // BigInt 
                BigInt.messerDecHexOverflow(bi, value);
                
                
                
                
                
                
                fail("missing SizeOverflowException");
             // TODO: CHANGED, Note for tracking
            } catch (SizeOverflowException e) {
                
            }
        }
    }
}
