package it.sec.ss2019.Aufgabe01.plongIO;

import de.htw_berlin.f4.cb.io.FileNameInPackage;

public class FileNameInPackageTest extends FileNameInPackage {

	public FileNameInPackageTest() {
		super();
	}
	
	public FileNameInPackageTest(String pack) {
		super(pack);
	}
	
	public FileNameInPackageTest(String pack, String dir) {
		super(pack, dir);
	}
	
	public FileNameInPackageTest(boolean bool, String pack) {
		super(bool, pack);
	}

}
