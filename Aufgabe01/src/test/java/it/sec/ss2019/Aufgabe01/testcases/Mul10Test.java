/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe01.testcases;

import java.util.HashMap;
import java.util.Iterator;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
@RunWith(JUnitPlatform.class)
public class Mul10Test extends Lib4Tests {
	
    final private String FILENAME = "Mul10-Tests.txt";

    private GetTests tests = null;

    public Mul10Test() {
        
    }
    @Test
    public void testMultiplicationBy10() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
            
            int size = Integer.valueOf(cur.get("s"));
            
            String a = cur.get("a");
            char signA = a.charAt(0);
            a = a.substring(1);
            
            BigInt r;
            r = BigInt.mul10(BigInt.fromString16(signA, a));
            String result = r.toMesserString16(size);
            assertEquals(cur.get("b"), result, "multiplication by 10 test" + " lineNo=" + cur.get("Line"));
            
            r = BigInt.mul10(r); // a:= 10*a
            result = r.toMesserString16(size);
            assertEquals(cur.get("c"), result, "multiplication by 10 test" + " lineNo=" + cur.get("Line"));
            
            r = BigInt.mul10(r); // a:= 10*a
            result = r.toMesserString16(size);
            assertEquals(cur.get("d"), result, "multiplication by 10 test" + " lineNo=" + cur.get("Line"));
            
            r = BigInt.mul10(r); // a:= 10*a
            result = r.toMesserString16(size);
            assertEquals(cur.get("e"), result, "multiplication by 10 test" + " lineNo=" + cur.get("Line"));
            
            r = BigInt.mul10(r); // a:= 10*a
            result = r.toMesserString16(size);
            assertEquals(cur.get("f"), result, "multiplication by 10 test" + " lineNo=" + cur.get("Line"));
            
            r = BigInt.mul10(r); // a:= 10*a
            result = r.toMesserString16(size);
            assertEquals(cur.get("g"), result, "multiplication by 10 test" + " lineNo=" + cur.get("Line"));
            
            r = BigInt.mul10(r); // a:= 10*a
            result = r.toMesserString16(size);
            assertEquals(cur.get("h"), result, "multiplication by 10 test" + " lineNo=" + cur.get("Line"));
        }
        tests= null;
    }
    
}