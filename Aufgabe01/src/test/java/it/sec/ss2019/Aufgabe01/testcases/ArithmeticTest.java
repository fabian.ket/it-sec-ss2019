/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe01.testcases;

import java.util.HashMap;
import java.util.Iterator;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;
import it.sec.ss2019.Aufgabe01.DivideByZeroException;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
@RunWith(JUnitPlatform.class)
public class ArithmeticTest extends Lib4Tests {
	
	
    final private String FILENAME = "Arithmetic-Tests.txt";

    private GetTests tests = null;

    public ArithmeticTest() {
        
    }
    
//    @Disabled
    @Test
    public void testAddition() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
//            if(!onlyTestCase(cur,"Arith-HexDec-27")) {
//                continue;
//            }
            
//            System.out.println("####### test "+cur.get("t")+" addition");
//            System.out.println(cur);
            // TODO: CHANGED FOR TRACKING, OLD = BACKUP
//            BigInt a = new BigInt(Short.valueOf(cur.get("s")), cur.get("a"));
//            BigInt b = new BigInt(Short.valueOf(cur.get("s")), cur.get("b"));
//            BigInt c = new BigInt(Short.valueOf(cur.get("s")), 0);
//            BigInt.add(c, a, b);
            int size = Integer.valueOf(cur.get("s"));
            String hexStringA = cur.get("a");
            char hexSignA = hexStringA.charAt(0);
            hexStringA = hexStringA.substring(1);
            
            String hexStringB = cur.get("b");
            char hexSignB = hexStringB.charAt(0);
            hexStringB = hexStringB.substring(1);
            
            BigInt a = BigInt.fromString16(hexSignA, hexStringA);
            BigInt b = BigInt.fromString16(hexSignB, hexStringB);
            
//            BigInt a = new BigInt(Short.valueOf(cur.get("a")));
//            BigInt b = new BigInt(Short.valueOf(cur.get("b")));
            BigInt c = BigInt.add(a, b);
            // C = a ... b
//            c = BigInt.add(c, a, b);

//            // TODO: ANGEPASST
////            bi.toBigInt(cur.get("h"));

//            // TODO: CHANGED, Note for tracking
//            String result = bi.toString16(OutputFormat.hex);
            String result = c.toMesserString16(size);

            
            // TODO: CHANGED FOR TRACKING, OLD = BACKUP 
//            String result = c.toString16(OutputFormat.allHex);
//            String result = c.toString16();
//            prt("a-value",reduceHexString(cur.get("a")));
//            prt("b-value",reduceHexString(cur.get("b")));
//            prt("c-value",reduceHexString(result));
//            prt("S-value",reduceHexString(cur.get("+")));
            assertEquals(cur.get("+"), result, "addition test" + " lineNo=" + cur.get("Line"));
        }
        tests= null;
    }
    
//    @Disabled
    @Test
    public void testSubtraction() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
//            if(!onlyTestCase(cur,"Arith-HexDec-27")) {
//                continue;
//            }
            
//            System.out.println("####### test "+cur.get("t")+" subtraction");
//            System.out.println(cur);
            // TODO: CHANGED FOR TRACKING, OLD = BACKUP
//            BigInt a = new BigInt(Short.valueOf(cur.get("s")), cur.get("a"));
//            BigInt b = new BigInt(Short.valueOf(cur.get("s")), cur.get("b"));
//            BigInt c = new BigInt(Short.valueOf(cur.get("s")), 0);
//            BigInt.sub(c, a, b);
            int size = Integer.valueOf(cur.get("s"));
            String hexStringA = cur.get("a");
            char hexSignA = hexStringA.charAt(0);
            hexStringA = hexStringA.substring(1);
            
            String hexStringB = cur.get("b");
            char hexSignB = hexStringB.charAt(0);
            hexStringB = hexStringB.substring(1);
            
            BigInt a = BigInt.fromString16(hexSignA, hexStringA);
            BigInt b = BigInt.fromString16(hexSignB, hexStringB);
            BigInt c = BigInt.sub(a, b);
            String result = c.toMesserString16(size);
            // TODO: CHANGED FOR TRACKING, OLD = BACKUP
//            String result = c.toString16(OutputFormat.allHex);
//            String result = c.toString16();
//            prt("a-value",reduceHexString(cur.get("a")));
//            prt("b-value",reduceHexString(cur.get("b")));
//            prt("c-value",reduceHexString(result));
//            prt("S-value",reduceHexString(cur.get("-")));
            assertEquals(cur.get("-"), result, "subtraction test" + " lineNo=" + cur.get("Line"));
        }
        tests= null;
    }
    
    @Test
    public void testMultiplication() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
//            if(!onlyTestCase(cur,"Arith-HexDec-35")) {
//                continue;
//            }
            
//            System.out.println("####### test "+cur.get("t")+" multiplication");
//            System.out.println(cur);
            // TODO: CHANGED FOR TRACKING, OLD = BACKUP
//            BigInt a = new BigInt(Short.valueOf(cur.get("s")), cur.get("a"));
//            BigInt A = new BigInt(Short.valueOf(cur.get("s")), cur.get("a"));
            
            String hexStringA = cur.get("a");
            char hexSignA = hexStringA.charAt(0);
            hexStringA = hexStringA.substring(1);
            
            String decStringA = cur.get("A");
            char decSignA = decStringA.charAt(0);
            decStringA = decStringA.substring(1);
            
            BigInt a = BigInt.fromString16(hexSignA, hexStringA);
            BigInt A = BigInt.fromString(decSignA, decStringA);
            assertEquals(a.toString16(), A.toString16(), "convert to dec test" + " lineNo=" + cur.get("Line"));
            // TODO: CHANGED FOR TRACKING, OLD = BACKUP
//            BigInt b = new BigInt(Short.valueOf(cur.get("s")), cur.get("b"));
//            BigInt B = new BigInt(Short.valueOf(cur.get("s")), cur.get("b"));
            
            String hexStringB = cur.get("b");
            char hexSignB = hexStringB.charAt(0);
            hexStringB = hexStringB.substring(1);
            
            String decStringB = cur.get("B");
            char decSignB = decStringB.charAt(0);
            decStringB = decStringB.substring(1);
            
            BigInt b = BigInt.fromString16(hexSignB, hexStringB);
            BigInt B = BigInt.fromString(decSignB, decStringB);
            assertEquals(b.toString16(), B.toString16(), "convert to dec test" + " lineNo=" + cur.get("Line"));
            
//            BigInt c = new BigInt(Short.valueOf(cur.get("s")),0);
            BigInt c = BigInt.mul(a, b);
            // C = a ... b
            int size = Integer.valueOf(cur.get("s"));
            // TODO: CHANGED FOR TRACKING, OLD = BACKUP
//            String result = c.toString16(OutputFormat.allHex);
            String result = c.toMesserString16(size);
//            BigNumber.prt("a-value",reduceHexString(cur.get("a")));
//            BigNumber.prt("b-value",reduceHexString(cur.get("b")));
//            BigNumber.prt("result ",reduceHexString(result));
//            BigNumber.prt("S-value",reduceHexString(cur.get("*")));
            assertEquals(cur.get("*"), result, "multiplication test" + " lineNo=" + cur.get("Line"));
        }
        tests= null;
    }
    
//    @Disabled
    @Test
    public void testDivision() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
//            if(!onlyTestCase(cur,"Arith-HexDec-40")) {
//                continue;
//            }
            if(cur.get("/").equals("")) {   // skip division by 0
                continue;
            }
            
            int size = Integer.valueOf(cur.get("s"));

            String hexStringA = cur.get("a");
            char hexSignA = hexStringA.charAt(0);
            hexStringA = hexStringA.substring(1);
            
            String decStringA = cur.get("A");
            char decSignA = decStringA.charAt(0);
            decStringA = decStringA.substring(1);
            
            BigInt a = BigInt.fromString16(hexSignA, hexStringA);
            BigInt A = BigInt.fromString(decSignA, decStringA);
            assertEquals(a.toString16(), A.toString16(), "convert to dec test" + " lineNo=" + cur.get("Line"));
            
            String hexStringB = cur.get("b");
            char hexSignB = hexStringB.charAt(0);
            hexStringB = hexStringB.substring(1);
            
            String decStringB = cur.get("B");
            char decSignB = decStringB.charAt(0);
            decStringB = decStringB.substring(1);
            
            BigInt b = BigInt.fromString16(hexSignB, hexStringB);
            BigInt B = BigInt.fromString(decSignB, decStringB);
            assertEquals(b.toString16(), B.toString16(), "convert to dec test" + " lineNo=" + cur.get("Line"));
            
            BigInt[] divmod = null;
            String divValue = "";
            String modValue = "";
			try {
				divmod = BigInt.divmod(a, b);
				divValue = divmod[0].toMesserString16(size);
				modValue = divmod[1].toMesserString16(size);
			} catch (DivideByZeroException e) {
				e.printStackTrace();
			}
            
            assertEquals(reduceHexString(cur.get("/")), reduceHexString(divValue),
                              "Division test /" + " lineNo=" + cur.get("Line"));
            assertEquals(reduceHexString(cur.get("%")), reduceHexString(modValue),
                              "Modulo test %" + " lineNo=" + cur.get("Line"));
        }
        tests= null;
    }
    
    @Test
    public void testDivisionBy0() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            HashMap<String, String> cur = ti.next();
//            cur.put("a", correctHex(cur.get("a")));
//            cur.put("b", correctHex(cur.get("b")));
//            cur.put("/", correctHex(cur.get("/")));
//            cur.put("%", correctHex(cur.get("%")));
            if(!cur.get("/").equals("")) {
                continue;
            }
//          System.out.println("####### test "+cur.get("t")+" Division by 0");
//          System.out.println(cur);
            
            String hexStringA = cur.get("a");
            char hexSignA = hexStringA.charAt(0);
            hexStringA = hexStringA.substring(1);
            
            String decStringA = cur.get("A");
            char decSignA = decStringA.charAt(0);
            decStringA = decStringA.substring(1);
            
            BigInt a = BigInt.fromString16(hexSignA, hexStringA);
            BigInt A = BigInt.fromString(decSignA, decStringA);
            
            assertEquals(a.toString16(), A.toString16(), "convert to dec test" + " lineNo=" + cur.get("Line"));
            
            String hexStringB = cur.get("b");
            char hexSignB = hexStringB.charAt(0);
            hexStringB = hexStringB.substring(1);
            
            String decStringB = cur.get("B");
            char decSignB = decStringB.charAt(0);
            decStringB = decStringB.substring(1);
            
            BigInt b = BigInt.fromString16(hexSignB, hexStringB);
            BigInt B = BigInt.fromString(decSignB, decStringB);
            
            assertEquals(b.toString16(), B.toString16(), "convert to dec test" + " lineNo=" + cur.get("Line"));
            
            BigInt div = new BigInt(Short.valueOf(cur.get("s")), 0);
            try {
            	// TODO: CHANGED FOR TRACKING, OLD = BACKUP
//                BigInt mod = BigInt.div(div, a, b);
            	// div:= a // b
                // return mod= a%b
                BigInt[] mod = BigInt.divmod(a, b);
//                fail("missing IllegalArgumentException");
                fail("missing DivideByZeroException");
//            } catch (IllegalArgumentException e) {
            } catch (DivideByZeroException e) {
                
            }
        }
        tests= null;
    }
}