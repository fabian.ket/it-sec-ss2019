/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe01.testcases;

import java.util.HashMap;
import java.util.Iterator;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe01.BigInt;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
@RunWith(JUnitPlatform.class)
public class ShiftLeftTest extends Lib4Tests {

    private final static String FILENAME = "Shift-Left-Tests.txt";

    private static GetTests tests = null;

    public ShiftLeftTest() {
        
    }
    @BeforeAll
    public static void setUp() {
        tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
    }
    @AfterEach
    public void tearDown() {
        tests = null;
    }

    @Test
    public void testShiftLeft() {
        Iterator<HashMap<String, String>> ti = tests.iterator();
        while (ti.hasNext()) {
            String result;
            HashMap<String, String> cur = ti.next();
//            if(!onlyTestCase(cur,"ShiftLeft-7")) {
//                continue;
//            }
//            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
//            System.out.println(cur);
//            BigInt a = new BigInt(Short.valueOf(cur.get("s")), cur.get("a"));
            
            int size = Integer.valueOf(cur.get("s"));
            int positionsToShift = 1;
            
            String hexA = cur.get("a");
            char signA = hexA.charAt(0);
            hexA = hexA.substring(1);
            
            BigInt a = BigInt.fromString16(signA, hexA);
            a = BigInt.shiftLeft(a, positionsToShift);
            result = a.toMesserString16(size);
            assertEquals(cur.get("b"), result, "ShiftLeft test b" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftLeft(a, positionsToShift);
            result = a.toMesserString16(size);
            assertEquals(cur.get("c"), result, "ShiftLeft test c" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftLeft(a, positionsToShift);
            result = a.toMesserString16(size);
            assertEquals(cur.get("d"), result, "ShiftLeft test d" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftLeft(a, positionsToShift);
            result = a.toMesserString16(size);
            assertEquals(cur.get("e"), result, "ShiftLeft test e" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftLeft(a, positionsToShift);
            result = a.toMesserString16(size);
            assertEquals(cur.get("f"), result, "ShiftLeft test f" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftLeft(a, positionsToShift);
            result = a.toMesserString16(size);
            assertEquals(cur.get("g"), result, "ShiftLeft test g" + " lineNo=" + cur.get("Line"));
            a = BigInt.shiftLeft(a, positionsToShift);
            result = a.toMesserString16(size);
            assertEquals(cur.get("h"), result, "ShiftLeft test h" + " lineNo=" + cur.get("Line"));
        }
    }

}
