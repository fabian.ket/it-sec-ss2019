package it.sec.ss2019.Aufgabe01;

import de.htw_berlin.f4.cb.io.IDataSource;
import it.sec.ss2019.Aufgabe01.plongIO.DataFileTest;
import it.sec.ss2019.Aufgabe01.plongIO.FileNameInPackageTest;

public class MainTest {

	public static void main(String[] args) {

		/*
		 	Dateinamen fuer die Tests:
		 	- Shift-Right-Tests.txt
		 	- Shift-Left-Tests
		 	- Mul10-Tests
		 	- Div10-Tests
		 	- Convert-Hex-Tests
		 	- Arithmetic-Tests
		 	- Compare-Tests
		 */
		
		 // Packagename sollte automatisch geholt werden...
		String packageName = "it.sec.ss2019.Aufgabe01";
		
		
		String filename = "Shift-Right-Tests.txt";
		
		FileNameInPackageTest fn = new FileNameInPackageTest(true, packageName);
		IDataSource ds = new DataFileTest(fn.getFileName(filename));
		
		while(!ds.eoi()) {
			String ln = ds.readLineAsString();
			System.out.print(ln + "\n");
		}

	}

}
