package it.sec.ss2019.Aufgabe01;

/*
 * Aufgabenblatt checken!!
 */
public interface IArithmeticable {

	public IArithmeticable add(IArithmeticable other);
	public IArithmeticable sub(IArithmeticable other);
	public IArithmeticable mul(IArithmeticable other);
	public IArithmeticable moddiv(IArithmeticable other);
	public void toBigInt(String other);
	public String toString();
	public void toBigInt16(String other);
	public String toString16(IArithmeticable other);
	public void reduce();
	public void resize(int sz);
	public IArithmeticable div10(IArithmeticable other);
	public IArithmeticable mul10(IArithmeticable other);
	public IArithmeticable mulCell(IArithmeticable other, int b);
	public IArithmeticable shiftLeft(IArithmeticable other, int cnt);
	public IArithmeticable addCell2(IArithmeticable other, int index, long b);
	public IArithmeticable copy(IArithmeticable other, int b);
	
}
