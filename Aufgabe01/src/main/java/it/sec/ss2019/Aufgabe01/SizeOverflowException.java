
package it.sec.ss2019.Aufgabe01;


public class SizeOverflowException extends RuntimeException {


    SizeOverflowException() { }

    public SizeOverflowException(final String msg) {
        super(msg);
    }

}
