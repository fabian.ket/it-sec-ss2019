package it.sec.ss2019.Aufgabe01;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.Random;

enum SIGN2 { pp, np, pn, nn }

public class BigInt {
	
	int spart = 0;
	boolean sign = true;
	private int[] value;
	
	// 2^(2^x) as BASE needed for even() and odd()
	private static final int BASE_OF_BASE = 2;
	// ARE YOU THE EXPONENT OF BASE? *UTZ UTZ UTZ UTZ UTZ*
	private static final int EXPONENT_OF_BASE = 16;
	
	// [1] 2^30  = 1073741824
	// [2] 2^60  = 1152921504606846976
	// [3] 2^90  = 1237940039285380274899124224
	// [4] 2^120 = 1329227995784915872903807060280344576
	// [5] 2^150 = 1427247692705959881058285969449495136382746624
	
	public static final int BASE = (int) Math.pow(BASE_OF_BASE, EXPONENT_OF_BASE);
	
	public static final BigInt MINUS_ONE = new BigInt(2, 1, false);
	public static final BigInt ZERO = new BigInt(2, 0);
	public static final BigInt ONE  = new BigInt(2, 1);
	public static final BigInt TWO  = new BigInt(2, 2);
	public static final BigInt THREE   = new BigInt(2, 3);
	public static final BigInt TEN     = new BigInt(2, 10);
	public static final BigInt SIXTEEN = new BigInt(2, 16);
	
	public BigInt(int size) {
		this(size, true);
	}
	
	public BigInt(int size, boolean sign) {
		size = checkSize(size);
		this.value = new int[size];
		this.sign = sign;
	}
	
	public BigInt(int size, int init) {
		this(size, init, true);
	}
	
	public BigInt(int size, int init, boolean sign) {
		size = checkSize(size);
		
		if (init < 0) {
			init *= -1;
		}
		
		if (init == 0) {
			sign = true;
		}
		
		value = new int[size];
		this.sign = sign;
		
		
		long tmp, over = 0;
		tmp = get(0) + init + over;
		set(0, (int)(tmp % BASE));
		over = tmp / BASE;
		
		if (over == 1) {
			set(1, (int)over);
		}
		
		reduce();
		checkZero();
	}
	
	public BigInt(int size, BigInt init) {
		this(size, init, true);
	}
	
	public BigInt(int size, BigInt init, boolean sign) {
		size = checkSize(size);
		
		value = new int[size];
		this.sign = sign;
		
		for (int i = 0; i < init.spart; i++) {
			this.set(i, init.get(i));
		}
		
		reduce();
		checkZero();
	}
	
	public BigInt(BigInt other) {
		this(other.size(), other, other.sign);
	}
	
	private static int checkSize(int size) {
		return (size <= 1) ? 2 : size;
	}
	
	// TODO: minus zero has to be checked
	public void checkZero() {
		for (int i = this.spart - 1; i >= 0; i--) {
			if (this.get(i) != 0) {
				return;
			}
		}
		this.sign = true;
	}
	
	public int size() { return this.value.length; }
	public int get(int index) { return value[index]; }
	private void set(int index, int number) { value[index] = number; }
	
	public void reduce() {
		for(int i = (size() - 1); i >= 0; i--) {
			if(value[i] != 0) {
				spart = i + 1;
				return;
			}
		}
		spart = 1;//(spart == 0) ? 1 : spart;
	}
	
	public void resize(int nsize) throws ResizeException {
		if (this.spart > nsize) {
			reduce();
			if (this.spart > nsize) {
				throw new ResizeException("cant resize..");
			}
		}
		if (this.spart < nsize) {
			expand(nsize);
		}
	}
	
	// erweitert auf die neue groesse, nicht um!!!
	public void expand(int nsize) {
		if(size() < nsize) {
			int[] tmp = new int[nsize];
			for(int i = 0; i < size(); i++) {
				tmp[i] = value[i];
			}
			this.value = tmp;
		} 
		this.spart = nsize;
	}
	
	public static void toSameSize(BigInt one, BigInt other) throws ResizeException {
		if(one.spart > other.spart) {
			other.resize(one.spart);
		}
		else if(one.spart < other.spart) {
			one.resize(other.spart);
		}
	}
	
	public static SIGN2 lookAtSign(BigInt a, BigInt b) {
		SIGN2 result = SIGN2.nn;
		
		if(a.sign && b.sign) {
			result = SIGN2.pp;
		}
		else if(!a.sign && b.sign) {
			result = SIGN2.np;
		}
		else if(a.sign && !b.sign) {
			result = SIGN2.pn;
		}
		
		return result;
	}
	
	private static BigInt _add(BigInt a, BigInt b, boolean algebraicSign) {
		long tmp, over = 0;

		try {
			toSameSize(a, b);
		} catch (ResizeException e) {
			System.out.println("Resize failed!");
			return null;
		}
		
		BigInt result = new BigInt(a.spart + 1, algebraicSign);
		for(int i = 0; i < a.spart; i++) {
			tmp = a.get(i) + b.get(i) + over;
			result.set(i, (int)(tmp % BASE));
			over = tmp / BASE;
		}
		result.set(a.spart, (int)over);
		
		result.sign = algebraicSign;
		result.reduce();
		result.checkZero();
		return result;
	}
	
	
	private static BigInt _sub(BigInt a, BigInt b, boolean algebraicSign) {
		long tmp = 0;
		int over = 0;

		try {
			toSameSize(a, b);
		} catch (ResizeException e) {
			System.out.println("Resize failed!");
			return null;
		}
		
		BigInt result = new BigInt(a.spart + 1, a, algebraicSign);
		for(int i = 0; i < a.spart; i++) {
			tmp = (long)a.get(i) - ((long)b.get(i) + over);
			
			if (tmp < 0) {
				tmp += BASE;
				over = 1;
			} else {
				over = 0;
			}
			
			result.set(i, (int)(tmp % BASE));
//			over = (int) (tmp / BASE);
//			System.out.println();
		}
		result.set(a.spart, over);
		
		result.sign = algebraicSign;
		result.reduce();
		result.checkZero();
		return result; 
	}

	private static BigInt _mul(BigInt a, BigInt b, boolean algebraicSign) {
		long tmp = 0;
		
		try {
			toSameSize(a, b);
		} catch (ResizeException e) {
			System.out.println("Resize failed!");
			return null;
		}
		
		BigInt result = new BigInt(2 * a.spart + 1, 0);
		for(int i = 0; i < b.spart; i++) {
			for(int j = 0; j < a.spart ; j++) {
				tmp = ((long)a.get(j)) * ((long)b.get(i));
				result = addCell2(result, i + j, tmp);
			}
		}
		
		// needed because -0 is not possible
		result.sign = algebraicSign;
		result.reduce();
		result.checkZero();
		return result;	
	}
	
//	private static BigInt[] _divmod_new(BigInt a, BigInt b, boolean algebraicSign) throws DivideByZeroException {
//
//		a = a.toAbsolute();
//		b = b.toAbsolute();
//
//		if(eq(b, new BigInt(2, 0))) {
//			throw new DivideByZeroException("div by zero");
//		}
//		
//		BigInt q = new BigInt(a.spart, 0, true);
//		BigInt r = new BigInt(a.spart, a, true);
//		
//		BigInt[] result = new BigInt[2];
//		
//		if(a.spart < b.spart) {
//			result[0] = q;
//			result[1] = r;
//			return result;			
//		}
//		if(a.spart == b.spart) {
//			if(lt(a, b)) {
//				result[0] = q;
//				result[1] = r;
//				return result;
//			}
//		}
//		
//		
//		BigInt mul = mul(b, q);
//		
//		while (lt(mul, a)) {
//			q = add(q, 1);
//			mul = mul(b, q);
//		}
//		q = sub(q, 1);
//		
//		mul = mul(b, q);
//		
//		r = sub(a, mul);
//
//		q.sign = algebraicSign;
//
//		q.reduce();
//		r.reduce();
//		result[0] = q;
//		result[1] = r;
//		
//		return result;
//	}

	private static BigInt[] _divmod(BigInt a, BigInt b, boolean algebraicSign) throws DivideByZeroException {
		
		// TODO: HACKY and not RIGHT
		BigInt _a = new BigInt(a).toAbsolute();
		BigInt _b = new BigInt(b).toAbsolute();
		
		BigInt q = new BigInt(_a.spart, 0, algebraicSign);
		BigInt r = new BigInt(_a.spart, _a, true);

		BigInt[] result = new BigInt[2];
		
		if(eq(_b, ZERO)) {
			throw new DivideByZeroException("div by zero");
		}
		
		if(_a.spart < _b.spart) {
//			if (lt(a, ZERO)) {
//				r.sign = false;
//				
//				while(lt(r, ZERO)) {
//					r = add(r, _b);
//				}
//			}
//			r.reduce();
//			r.checkZero();
//			
//			while (!formel(a, b, q, r)) { // && (lt(a, new BigInt(2, 0)) && gt(b, new BigInt(2, 0)) && !eq(r, new BigInt(2, 0)))) {
//				q = sub(q, ONE);
//			}
//			
//			q.reduce();
//			q.checkZero();
			
			q.sign = algebraicSign;
			q.reduce();
			r.reduce();
			q.checkZero();
			r.checkZero();
			
			if (lt(a, ZERO) && lt(b, ZERO) && !eq(r, ZERO)) {
				q = add(q, ONE);
			}
			else if (lt(a, ZERO) && gt(b, ZERO) && !eq(r, ZERO)) {
				q = sub(q, ONE);
			}
			
			if (lt(a, ZERO) && !eq(r, ZERO)) {
				BigInt _q = new BigInt(q).toAbsolute();
				r = sub(_a, mul(_b, _q));
				r = r.toAbsolute();
			}
			
			q.sign = algebraicSign;
			q.reduce();
			r.reduce();
			q.checkZero();
			r.checkZero();
			
			result[0] = q;
			result[1] = r;
			return result;			
		}
		if(_a.spart == _b.spart) {
			if(lt(_a, _b)) {
				q.sign = algebraicSign;
				q.reduce();
				r.reduce();
				q.checkZero();
				r.checkZero();
				
				if (lt(a, ZERO) && lt(b, ZERO) && !eq(r, ZERO)) {
					q = add(q, ONE);
				}
				else if (lt(a, ZERO) && gt(b, ZERO) && !eq(r, ZERO)) {
					q = sub(q, ONE);
				}
				
				if (lt(a, ZERO) && !eq(r, ZERO)) {
					BigInt _q = new BigInt(q).toAbsolute();
					r = sub(_a, mul(_b, _q));
					r = r.toAbsolute();
				}
				
				q.sign = algebraicSign;
				q.reduce();
				r.reduce();
				q.checkZero();
				r.checkZero();
				
				result[0] = q;
				result[1] = r;
				return result;
			}
		}

		r = new BigInt(_a.spart, 0, true);

		int k = _b.spart;
		int l = _a.spart - k;
		for(int i = 0; i < k; i++) {
			r.set(i, _a.get(l + i));
		}

		r.reduce();

		for(int i = l; i >= 0; i--) {

			int e;
			if(r.spart >= 2) {
				e = estimateAlter(r.get(r.spart-1), r.get(r.spart-2), _b.get(_b.spart-1));
			} else {
				e = estimateAlter(0, r.get(r.spart-1), _b.get(_b.spart-1));
			}
			
			BigInt tmp = mul(_b, new BigInt(2, e));
			
			if(!eq(tmp, r)) {
				while (gt(tmp, r)) {
					e--;
					tmp = sub(tmp, _b);
					
				}

				while (gt(sub(r, tmp), _b)) {
					e++;
					tmp = add(tmp, _b);
				}
			}
			
			q.set(i, e);
			r = sub(r, tmp);
			
			if(i != 0) {
				r = add(mul(r, BASE), _a.get(i - 1));
			}
		}
		
		q.sign = algebraicSign;
		q.reduce();
		r.reduce();
		q.checkZero();
		r.checkZero();
		
		if (lt(a, ZERO) && lt(b, ZERO) && !eq(r, ZERO)) {
			q = add(q, ONE);
		}
		else if (lt(a, ZERO) && gt(b, ZERO) && !eq(r, ZERO)) {
			q = sub(q, ONE);
		}
		
		if (lt(a, ZERO) && !eq(r, ZERO)) {
			BigInt _q = new BigInt(q).toAbsolute();
			r = sub(_a, mul(_b, _q));
			r = r.toAbsolute();
		}
		
		q.sign = algebraicSign;
		q.reduce();
		r.reduce();
		q.checkZero();
		r.checkZero();
		
		result[0] = q;
		result[1] = r;
		
		return result;
	}
	
//	public static int estimateKnuth(int cell1, int cell2, int divCell) {
//		long div = (cell1 * BASE) + cell2;
//		int est = (int) (div / divCell);
//		if((est / BASE) != 0) {
//			est = BASE - 1;
//		}
//		return est;
//	}
	
	public static int estimateAlter(int cell1, int cell2, int divCell) {
		long dividend = cell1;
		if (dividend < (long) divCell) {
			dividend = ((long) cell1 * BASE) + cell2;
		}
		int estimate = (int)(dividend / divCell);
		
		return estimate;
	}
	
	// TODO: test this stuff!!!
	public static BigInt mulCell(BigInt a, int b) {
		BigInt c = new BigInt(a.spart + 1, 0, a.sign);
		long tmp;
		for(int i = 0; i < a.spart; i++) {
			tmp = ((long)a.get(i)) * b;
			c = addCell2(c, i, tmp);
		}
		return c;
	}
	
	// 
	public static BigInt[] divmod10(BigInt a) {
		try {
			return _divmod(a, TEN, a.sign);
		} catch (DivideByZeroException e) {
			return null;
		}
	}
	
	
	public static boolean eq(BigInt a, BigInt b) {
		
		if (a == null && b == null) 
			return true;
		if (a == null || b == null) 
			return false;
		
		if (a.sign != b.sign)
			return false;
		
		a.reduce();
		b.reduce();
		
		if (a.spart != b.spart)
			return false;
		
		for (int i = a.spart - 1; i >= 0; i--) {
			if (a.get(i) != b.get(i))
				return false;
		}
		
		return true;
	}
	
	public static boolean gt(BigInt a, BigInt b) {
		if (a.sign == false && b.sign == true)
			return false;
		if (a.sign == true && b.sign == false)
			return true;
		
		a.reduce();
		b.reduce();
		
		if (a.spart > b.spart)
			return true;
		if (a.spart < b.spart)
			return false;
		
		for (int i = a.spart - 1; i >= 0; i--) {
			
			if(a.sign) {
				if (a.get(i) > b.get(i)) {
					return true;
				}
				else if (a.get(i) < b.get(i)) {
					return false;
				}
			} else {
				if (a.get(i) < b.get(i)) {
					return true;
				}
				else if (a.get(i) > b.get(i)) {
					return false;
				}
			}
		}
		
		return false;
	}
	
	public static boolean lt(BigInt a, BigInt b) {
		if (a.sign == false && b.sign == true)
			return true;
		if (a.sign == true && b.sign == false)
			return false;
		
		a.reduce();
		b.reduce();
		
		if (a.spart > b.spart)
			return false;
		if (a.spart < b.spart)
			return true;
		
		for (int i = a.spart - 1; i >= 0; i--) {
			
			if(a.sign) {
				if (a.get(i) < b.get(i)) {
					return true;
				}
				else if (a.get(i) > b.get(i)) {
					return false;
				}
			} else {
				if (a.get(i) > b.get(i)) {
					return true;
				}
				else if (a.get(i) < b.get(i)) {
					return false;
				}
			}
		}
		
		return false;
	}
	
	public static boolean ge(BigInt a, BigInt b) {
		return gt(a, b) || eq(a, b);
	}
	
	public static boolean le(BigInt a, BigInt b) {
		return lt(a, b) || eq(a, b);
	}
	
	public static BigInt add(BigInt a, int b) {
		boolean sign = true;
		
		if (b < 0) {
			sign = false;
			b *= -1;
		}
		
		BigInt bBig = new BigInt(2, b, sign);
		
		return add(a, bBig);
	}
	
	public static BigInt add(BigInt a, BigInt b) {
		switch(lookAtSign(a, b)) {
			case pp:
				return _add(a, b, true);
				
			case np:
				if(le(a.toAbsolute(), b.toAbsolute())) {
					return _sub(b, a, true);
				}
				return _sub(a, b, false);
				
			case pn:
				if(le(a.toAbsolute(), b.toAbsolute())) {
					return _sub(b, a, false);
				}
				return _sub(a, b, true); 
				
			default:
				return _add(a, b, false); 
		}
	}
	
	public static BigInt sub(BigInt a, int b) {
		boolean sign = true;
		
		if (b < 0) {
			sign = false;
			b *= -1;
		}
		
		BigInt bBig = new BigInt(2, b, sign);
		
		return sub(a, bBig);
	}
	
	public static BigInt sub(BigInt a, BigInt b) {
		switch(lookAtSign(a,b)) {
			case pp: 
				if(le(a.toAbsolute(), b.toAbsolute())) {
					return _sub(b, a, false);
				}
				return _sub(a, b, true);
				
			case np: 
				return _add(a, b, false);
			
			case pn: 
				return _add(a, b, true);
				
			default: 
				if(le(a.toAbsolute(), b.toAbsolute())) {
					return _sub(b, a, true);
				}
				return _sub(a, b, false); 
		}
	}
	
	public static BigInt mul(BigInt a, BigInt b){
		switch(lookAtSign(a, b)) {
			case pp: return _mul(a, b, true);
			case np: return _mul(a, b, false);
			case pn: return _mul(a, b, false);
			default: return _mul(a, b, true);
		}
	}
	
	public static BigInt mul(BigInt a, int b) {
		boolean sign = true;
		if (b < 0) {
			sign = false;
			b *= -1;
		}
		
		BigInt bBig = new BigInt(2, b, sign);
		return mul(a, bBig);
	}
	
	public static BigInt[] divmod(BigInt a, int b) throws DivideByZeroException {
		boolean sign = true;
		if (b < 0) {
			sign = false;
			b *= -1;
		}
		
		BigInt bBig = new BigInt(2, b, sign);
		return divmod(a, bBig);
	}
	
	public static BigInt[] divmod(BigInt a, BigInt b) throws DivideByZeroException {
//		switch (lookAtSign(a, b)) {
//			case pp:
//				return _divmod(a, b, true);
//			case np:
//				return _divmod(a, b, false);
//			case pn:
//				return _divmod(a, b, false);
//			default:
//				return _divmod(a, b, true);
//		}
		BigInteger[] test = new BigInteger(a.toString()).divideAndRemainder(new BigInteger(b.toString()));
		BigInt[] returner = new BigInt[2];
		
		returner[0] = BigInt.fromString(test[0].toString());
		returner[1] = BigInt.fromString(test[1].toString());
		
		return returner;
	}
	
	public static BigInt shiftLeft(BigInt a, int cnt) {
		int over = 0;
		long tmp = 0;
		
		// TODO: test with his cases
		cnt = cnt % EXPONENT_OF_BASE;
		
		BigInt result = new BigInt(a.spart + 1, a, a.sign);
		for(int i = 0; i < a.spart; i++) {
			tmp = (((long)a.get(i)) << cnt) + over;
			result.set(i, (int)(tmp % BASE));
			over = (int)(tmp / BASE);
		}
		result.set(a.spart, over);
		
		result.reduce();
		result.checkZero();
		return result;
	}
	
	
	
	public static BigInt shiftLeftMesser(BigInt a, int cnt, int size) {
		int over = 0;
		long tmp = 0;

		// TODO: test with his cases
		// cnt = 1 % 30 (= 1)
		cnt = cnt % EXPONENT_OF_BASE;
		
		// [1] 2^30  = 1073741824
		
		// int newSize = 0;
		// int maxUnsigendShort = 0xffff; // 65535

		BigInt result = new BigInt(a.spart + 1, a, a.sign);
		
		for(int i = 0; i < a.spart; i++) {
			
			tmp = (((long)a.get(i)) << cnt) + over;
			
			/*
			 * ATTEMPT OF A HACKY IMPLEMENTATION
			 * TO FIX THIS OVERFLOW PROBLEM!
			 * STILL NEEDS SOME WORK!
			 */
			
//			if(i == (size / EXPONENT_OF_BASE)) {
//				System.out.print(a.get(i));
//				System.out.print(" - END");
//				
//				if(a.get(i-1) != 0) {
//
//					if( ( (long)a.get(i) << cnt) >= maxUnsigendShort) {
//						System.out.print(" -1");
//						tmp = ((long)a.get(i) << cnt) - maxUnsigendShort;
//						System.out.print(" " + tmp + " \n");
//					} else {
//						System.out.print(" -2");
//					}
//					
//				} else {
//					tmp = 0;
//				}
//			}


			
			result.set(i, (int)(tmp % BASE));
			over = (int)(tmp / BASE);
		}

		result.set(a.spart, over);
		result.reduce();
		return result;
		

	}

	
	public static BigInt mul10(BigInt a) {
		BigInt result = shiftLeft(a, 2);
		result = add(result, a);
		result = shiftLeft(result, 1);
		
		return result;
	}
	
	public static BigInt addCell2(BigInt a, int index, long b) {
		
		BigInt result = new BigInt(a.spart + 1, a, a.sign);
		long tmp, over = b;
		while(over != 0) {
			if (index >= result.size()) {
				result.expand(2*index);
			}
			
			tmp = (long)result.get(index) + (over % BASE);
			result.set(index++, (int) (tmp % BASE));
			over = (tmp / BASE) + (over / BASE);
		}
		
		result.reduce();
		result.checkZero();
		return result;
	}
	
	public BigInt toAbsolute() {
		return new BigInt(this.size(), this, true);
	}
	
	public static BigInt fromString(String str) {
		return fromString('+', str);
	}
	
	public static BigInt fromString(char sign, String str) {
		
		boolean bSign = true;
		
		if (sign == '-') {
			bSign = false;
		}
		
		int size = str.length();
		
		BigInt result = new BigInt(size, Integer.parseInt(str.substring(0, 1)), true);
		BigInt mul10;
		
		for (int i = 1; i < size; i++) {
			mul10 = mul10(result); 
			int other = Integer.parseInt(str.substring(i, i + 1));
			result = add(mul10, other);
		}
		
		result.sign = bSign;
		
		result.reduce();
		result.checkZero();
		return result;
	}
	
	@Override
	public String toString() {
		String s = "";
		String t = "";
		
		if(this.sign == false) {
			t = "-";
		}

		BigInt a = new BigInt(this.size(), this);
		BigInt b = new BigInt(1, 0);

		BigInt[] res;

		if (eq(a, b)) {
			s = "0";
		} else {
			while (!eq(a, b)) {
				res = divmod10(a);
				a = res[0];
				s = String.valueOf(res[1].value[0]) + s;
			}
		}

		return t + s;
	}
	
	public String toMesserString() {
		String result = toString();
		
		if (result.startsWith("-")) {
			result = result.substring(1);
		}
		
		return ( (this.sign) ? "+" : "-") + result;
	}
	
	public String toMesserStringNumber() {
		String result = toString();
		String plus = "+";
		// Only if number is positive, add a "+"
		// negative numbers already have a "-" so no formatting needed
		if(this.sign) {
			result = plus.concat(result);
		}	
		return result;
	}

	public static BigInt fromString16(char cSign, String str) {

		boolean bSign = true;

		if (cSign == '-') {
			bSign = false;
		}

		if (str.length() >= 2 && str.contains("0x")) {
			str = str.substring(2);
		}

		int size = str.length();

		BigInt result = new BigInt(size, true);

		String tmp;
		int value;
		for (int i = 0; i < size; i++) {
			tmp = str.substring(i, i+1);
			value = fromHex16Str(tmp);

			if (value != -1) {
				result = add(result, value);
			} else {
				System.out.println(tmp + " wasn't a hex value!");
				return null;
			}

			if ( (i+1) != size) {
				result = shiftLeft(result, 4);
			}
		}

		result.sign = bSign;
		result.reduce();
		result.checkZero();
		return result;
	}

	private static int fromHex16Str(String str) {
		int result = -1;

		if (str.equals("0")) {
			result = 0;
		}
		else if (str.equals("1")) {
			result = 1;
		}
		else if (str.equals("2")) {
			result = 2;
		}
		else if (str.equals("3")) {
			result = 3;
		}
		else if (str.equals("4")) {
			result = 4;
		}
		else if (str.equals("5")) {
			result = 5;
		}
		else if (str.equals("6")) {
			result = 6;
		}
		else if (str.equals("7")) {
			result = 7;
		}
		else if (str.equals("8")) {
			result = 8;
		}
		else if (str.equals("9")) {
			result = 9;
		}
		else if (str.equals("a") || str.equals("A")) {
			result = 10;
		}
		else if (str.equals("b") || str.equals("B")) {
			result = 11;
		}
		else if (str.equals("c") || str.equals("C")) {
			result = 12;
		}
		else if (str.equals("d") || str.equals("D")) {
			result = 13;
		}
		else if (str.equals("e") || str.equals("E")) {
			result = 14;
		}
		else if (str.equals("f") || str.equals("F")) {
			result = 15;
		}

		return result;
	}
	
	private String toHex16Str(int i) {
		String result = null;
		
		if(i >= 0 && i < 10) {
			result = String.valueOf(i);
		}
		else if(i == 10) {
			result = "a";
		}
		else if(i == 11) {
			result = "b";
		}
		else if(i == 12) {
			result = "c";
		}
		else if(i == 13) {
			result = "d";
		}
		else if(i == 14) {
			result = "e";
		}
		else if(i == 15) {
			result = "f";
		}
		
		return result;
	}

	public String toString16() {

		String s = "";
		String t = "";
		
		if(this.sign == false) {
			t = "-";
		}

		BigInt a = new BigInt(this.size(), this);
		BigInt b = new BigInt(1, 0);

		BigInt[] res;

		if (eq(a, b)) {
			s = "0";
		} else {
			while (!eq(a, b)) {
				try {
					res = divmod(a, SIXTEEN);
					a = res[0];
					s = toHex16Str(res[1].value[0]) + s;
				} catch (DivideByZeroException e) {
					return null;
				}
				
			}
		}

		return t + s;
	}
	
	public String toMesserString16() {
		
		String result = toString16();
				
		if (result.startsWith("-")) {
			result = result.substring(1);
		}
		
		return ( (this.sign) ? "+" : "-") + "0x" + result;
	}
	
	public String toMesserString16(int size) {
		
		int lenghOfHexValues = size / 4;
		
		String result = toString16();
				
		if (result.startsWith("-")) {
			result = result.substring(1);
		}
		
		int length = result.length();
		
		int zeros = (size / 4) - length;
		
		if(length < lenghOfHexValues) {
			for (int i = 0; i < zeros; i++) {
				result = "0" + result;
			}
		} else {
			int start = length - lenghOfHexValues;
			result = result.substring(start, result.length());
		}
		
//		for (int i = 0; i < zeros; i++) {
//			result = "0" + result;
//		}
		
		return ( (this.sign) ? "+" : "-") + "0x" + result;
	}
	
	public static boolean formel(BigInt a, BigInt b, BigInt div, BigInt mod) {
//		a = (a div b)*b+(a mod b)		
		BigInt mul = mul(div, b);
		BigInt sum = add(mul, mod);
		
		return eq(a, sum);
	}
	

	
	public static BigInt messerDecHexOverflow(BigInt inty, String strtoBigInt) throws SizeOverflowException {
		
        char sign = strtoBigInt.charAt(0);		
		BigInt biggy = BigInt.fromString(sign, strtoBigInt.substring(1));
		System.out.println(biggy.size());
		System.out.println(inty.size());
		
		if(inty.size() < biggy.size()) {
			throw new SizeOverflowException();
		}
		
		return inty;
	}
	
	
	/***
	 * Additions for Prime
	 */
	public static BigInt getRandomOdd(int bits) {
		int size = bits / EXPONENT_OF_BASE;
		
		int rest = bits % EXPONENT_OF_BASE; 
		if ( rest != 0 ) {
			size += 1;
		}
		
		BigInt result = new BigInt(size);
		
		Random rand = new Random(Calendar.getInstance().getTimeInMillis());
		
		for (int i = 0; i < (size-1); ++i) {
			result.set(i, rand.nextInt(BASE));
		}
		
		final int LAST = size - 1;
		final int MOST_BIT;
		
		if (rest == 0) {
			MOST_BIT = BASE >>> 1;
			result.set(LAST, rand.nextInt(BASE));
		} else {
			MOST_BIT = 0x1 << (rest-1);
			result.set(LAST, rand.nextInt((int)Math.pow(2, rest)));
		}
		
		// set least significant bit
		if ( (result.get(0) & 0x1) == 0) {
			result.set(0, result.get(0) | 0x1);
		}
		// set most significant bit
		if ( (result.get(LAST) & MOST_BIT ) == 0) {
			result.set(LAST, result.get(LAST) | MOST_BIT);
		}
		
		result.reduce();
		result.checkZero();
		return result;
	}
	
	public static BigInt power(BigInt x, int y) {
		return power(x, new BigInt(2, y));
	}
	
	public static BigInt power(BigInt x, BigInt y) {
		boolean sign = true;
		
		if (!x.sign && odd(y)) {
			sign = false;
		}
		
		BigInt result = new BigInt(2, 1);
		BigInt _x = x.toAbsolute();
		
		if (eq(y, ZERO)) {
			return result;
		}
		
		int bitExpSet = 0;
		int bitExp = 0;
		
		for (int i = 0; i < y.spart; ++i) {
			for (int j = 0; j < EXPONENT_OF_BASE; ++j) {
				bitExp = (0x1 << j);
				
				if (bitExp > y.get(i)) {
					break;
				}
				
				bitExpSet = y.get(i) & bitExp; 
				
				if (bitExpSet != 0) {
					result = mul(result, _x);
				}
				
				_x = mul(_x, _x);
			}
		}
		
		result.sign = sign;
		result.reduce();
		result.checkZero();
		return result;
	}
	
	public static BigInt powerMod(BigInt x, BigInt y, BigInt m) throws DivideByZeroException {
		// return divmod(power(x, y), m)[1];
		
		BigInt result = new BigInt(2, 1);
		BigInt tmp = new BigInt(x);
		
		while (gt(y, ZERO)) {
			if (odd(y)) {
				result = divmod(mul(result, tmp), m)[1];
			}
			
			tmp = divmod(square(tmp), m)[1];
			y = shiftRight(y);
		}
		
		return result;
		
	}
	
	public static BigInt square(BigInt x) {
		return power(x, TWO);
	}
	
	public int zeroBits() {
		int result = 0;
		
		// TODO: check if this is the wanted behavior
		if ( eq(this, ZERO) ) {
			return 1;
		}
		
		int tmp = -1;
		for (int i = 0; i < spart; ++i) {
			for (int j = 0; j < EXPONENT_OF_BASE; ++j) {
				tmp = get(i) & (0x1 << j);
				
				if(tmp != 0) {
					return result;
				}
				
				result++;
			}
		}
		
		return result;
	}
	
	// TODO: needs testing
	public static boolean even(BigInt a) {
		return (a.get(0) & 0x1) == 0;
	}
	
	// TODO: needs testing	
	public static boolean odd(BigInt a) {
		return !even(a);
	}
	
	public static BigInt shiftRight(BigInt a) {
		return shiftRight(a, 1);
	}
	
	public static BigInt shiftRight(BigInt a, int cnt) {
		
		BigInt result = new BigInt(a);
		
		if (cnt <= 0) {
			return result;
		}
		
		// TODO: test with his cases, 16 % 16 = 0 not really what you want
		cnt = cnt % EXPONENT_OF_BASE;
		
		int MASK = 0;
		
		for (int i = 0; i < cnt; ++i) {
			MASK = MASK << 1;
			MASK = MASK + 1;
		}
		
		int current;
		int over;
		
		for (int i = 0; i < a.spart - 1; ++i) {
			current = a.get(i) >>> cnt;
			over = (a.get(i + 1) & MASK) << (EXPONENT_OF_BASE - cnt);
			result.set(i, (current | over));
		}
		result.set(a.spart - 1, a.get(a.spart - 1) >>> cnt);
		
		result.reduce();
		result.checkZero();
		return result;
	}
	
	public static BigInt gcdBin(BigInt a, BigInt b) {
		BigInt _a = a.toAbsolute();
		BigInt _b = b.toAbsolute();
		
		BigInt result;
		
		int k = 0;
		for (; even(_a) && even(_b); ++k) {
			_a = shiftRight(_a); // a = a / 2
			_b = shiftRight(_b); // b = b / 2
		}
		
		BigInt tmp;
		
		while ( !eq(_a, ZERO) ) {
			while ( even(_a) ) {
				_a = shiftRight(_a);
			}
			while ( even(_b) ) {
				_b = shiftRight(_b);
			}
			
			if ( lt(_a, _b) ) {
				tmp = _a;
				_a = _b;
				_b = tmp;
			}
			_a = sub(_a, _b);
		}
		
		result = mul(_b, power(TWO, k) );
		
		result.reduce();
		result.checkZero();
		return result;
	}
	
	public static BigInt gcdSimple(BigInt a, BigInt b) {
		BigInt _a = a.toAbsolute();
		BigInt _b = b.toAbsolute();
		
		while (true) {
			if (eq(_b, ZERO))
				return _a;
			try {
				_a = divmod(_a, _b)[1];
			}
			catch(DivideByZeroException e) {
				
			}
			
			if (eq(_a, ZERO))
				return _b;
			
			try {
				_b = divmod(_b, _a)[1];
			}
			catch (DivideByZeroException e) {
				
			}
		}
	}
	
	public int getBitCount() {
		int bitsLastCell = 15;
		int lastCell = get(spart - 1);
		
		for (; bitsLastCell >= 0; bitsLastCell--) {
			if (lastCell >= (int)Math.pow(2, bitsLastCell)) {
				break;
			}
		}
		bitsLastCell += 1;
		
		return (spart - 1) * EXPONENT_OF_BASE + bitsLastCell;
	}
	
	
	
	public static BigInt invers(BigInt x, BigInt m) throws DivideByZeroException {
		
		// Source:
		// https://www.di-mgt.com.au/euclidean.html#code-exteuc
		// Erste Implementierung, wird verbessert
		
		// Rechner zur seperaten Prüfung
		// https://planetcalc.com/3311/
		
	    BigInt result;
	    BigInt u;
	    BigInt ux;
	    BigInt v;
	    BigInt vm;
	    BigInt tmp1;
	    BigInt tmp2;
		BigInt q;
		BigInt[] arrTmp;
	    int iter;
	    
	    u = ONE;
	    ux = new BigInt(x);
	    v = ZERO;
	    vm = new BigInt(m);
	    iter = 1;
	    
    	while (!eq(vm, ZERO)) {
    		arrTmp = BigInt.divmod(ux, vm);
	        q = arrTmp[0];
	        tmp2 = arrTmp[1];
	        tmp1 = BigInt.add(u, BigInt.mul(q, v));

	        u = v;
	        v = tmp1;
	        ux = vm;
	        vm = tmp2;
	        
	        iter = -iter;
	    }
    	if(!eq(ux, ONE)) { return ZERO; }

	    if (iter < 0) { 
	    	result = BigInt.sub(m, u);
	    }
	    else { result = u; }
	    
	    return result;
	}
	
	public static BigInt[] egcdSimple(BigInt x, BigInt m) throws DivideByZeroException {
		// Sources:
		// https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
		// https://www.di-mgt.com.au/euclidean.html#code-exteuc
		// Erste Implementierung, wird verbessert
	
		
		// z ist 'this'
		BigInt tempX = new BigInt(x);
		BigInt tempY = new BigInt(m);
		BigInt[] q = new BigInt[2];
		BigInt[] result = new BigInt[3];
		BigInt x0 = ONE;
		BigInt x1 = ZERO;
		BigInt y0 = ZERO;
		BigInt y1 = ONE;
		while(true) {
			q = BigInt.divmod(tempX, tempY);
			tempX = q[1];
			x0 = BigInt.sub(x0, BigInt.mul(y0, q[0]));
			x1 = BigInt.sub(x1, BigInt.mul(y1, q[0]));
			if(eq(tempX, ZERO)) {
				result[0] = tempY;
				result[1] = y0;
				result[2] = y1;
				return result;
			}
			
			q = BigInt.divmod(tempY, tempX);
			tempY = q[1];
			y0 = BigInt.sub(y0, BigInt.mul(x0, q[0]));
			y1 = BigInt.sub(y1, BigInt.mul(x1, q[0]));
			if(eq(tempY, ZERO)) {
				result[0] = tempX;
				result[1] = x0;
				result[2] = x1;
				return result;
			}
		}

	}
	
	public static BigInt egcdBin(BigInt a, BigInt b) {
		
		return null;
	}
	
	public static BigInt max(BigInt a, BigInt b) {
		if (ge(a, b))
			return new BigInt(a);
		else
			return new BigInt(b);
	}
	
	public static BigInt min(BigInt a, BigInt b) {
		if (le(a, b))
			return new BigInt(a);
		else
			return new BigInt(b);
	}
	
	
	public static byte bit(BigInt x, int y) {
		byte bit = 0;
		if(y <= 0) {
			bit = lastBit(x);
		} else {
			// TODO:
			// liefert das Bit and der Position "y", von 0
			// angezaehlt, "odd()" ist daher bit(x, 0)
		}

		return bit;
	}
	
	
	private static byte lastBit(BigInt x) {
		byte last = 0;
		if(BigInt.odd(x)) {
			last = 1;
		}

		
		return last;

	}
	
	
	
}
