package it.sec.ss2019.Aufgabe01;

public class ResizeException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public ResizeException(String text) {
		super(text);
	}
}
