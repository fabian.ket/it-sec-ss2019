package it.sec.ss2019.Aufgabe01;

public class DivideByZeroException extends Exception {
	private static final long serialVersionUID = 1L;
	public DivideByZeroException(String text) {
		super(text);
	}
}
