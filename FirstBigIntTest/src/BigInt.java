import java.util.ArrayList;
import java.util.List;

public class BigInt {

    private List<Integer> values;
    private static final Integer BASE = 1000000000; // should currently be 10^x
    private static final Integer NUMBERS_PER_BLOCK = String.valueOf(BASE - 1).length();

    private BigInt() {
        values = new ArrayList<>();
    }

    public BigInt(String number) {
        values = BigInt.fromString(number).values;
    }

    public static BigInt add(BigInt x, BigInt y) {
        BigInt result = new BigInt();

        Integer carry = 0;
        int size;

        if (x.values.size() > y.values.size()) {
            size = x.values.size();
        } else {
            size = y.values.size();
        }

        Integer resultValue;
        Integer xValue;
        Integer yValue;

        for (int i = 0; i < size; i++) {
            try {
                xValue = x.values.get(i);
            } catch (IndexOutOfBoundsException e) {
                xValue = 0;
            }

            try {
                yValue = y.values.get(i);
            } catch (IndexOutOfBoundsException e) {
                yValue = 0;
            }

            resultValue = (xValue + yValue + carry) % BigInt.BASE;
            carry = (xValue + yValue + carry) / BigInt.BASE;

            result.values.add(resultValue);
        }

        if (carry != 0) {
            result.values.add(carry);
        }

        return result;
    }

    public static BigInt sub(BigInt x, BigInt y) {
        BigInt result = new BigInt();

        Integer carry = 0;
        int size;

        if (x.values.size() > y.values.size()) {
            size = x.values.size();
        } else {
            size = y.values.size();
        }

        Integer resultValue;
        Integer xValue;
        Integer yValue;

        for (int i = 0; i < size; i++) {
            try {
                xValue = x.values.get(i);
            } catch (IndexOutOfBoundsException e) {
                xValue = 0;
            }

            try {
                yValue = y.values.get(i);
            } catch (IndexOutOfBoundsException e) {
                yValue = 0;
            }

            // TODO: result negative
            resultValue = (((xValue < yValue) ? xValue + BigInt.BASE : xValue) - (yValue + carry)) % BigInt.BASE;
            carry = (((xValue < yValue) ? xValue + BigInt.BASE : xValue) - (yValue + carry)) / BigInt.BASE;

            result.values.add(resultValue);
        }

        if (carry != 0) {
            result.values.add(carry);
        }

        return result;
    }

    public static BigInt mul(BigInt x, BigInt y) {
        return null;
    }

    public static BigInt mulKara(BigInt x, BigInt y) {
        return null;
    }

    public static BigIntMod moddiv(BigInt x, BigInt y) {
        return null;
    }

    public static BigInt fromString(String number) {
        BigInt big = new BigInt();

        for (int i = number.length(); i > 0; i -= NUMBERS_PER_BLOCK) {
            int beginIndex = ( (i-NUMBERS_PER_BLOCK) >= 0 ? i-NUMBERS_PER_BLOCK : 0 );
            String block = number.substring(beginIndex, i);
            big.values.add(Integer.parseInt(block));
        }

        return big;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        for (Integer block : values) {
            stringBuilder.insert(0, String.format("%0" + NUMBERS_PER_BLOCK + "d", block));
        }

        return stringBuilder.toString();
    }
}
