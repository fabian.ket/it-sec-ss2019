public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!");
        
        
        System.out.println("Good Night World!");

        BigInt big1 = new BigInt("612");
        BigInt big2 = new BigInt("292");

        System.out.println(big1);
        System.out.println(big2);
        System.out.println(BigInt.add(big1, big2));
        System.out.println(BigInt.sub(big1, big2));

    }
}
