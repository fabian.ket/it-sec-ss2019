package it.sec.ss2019.Aufgabe06;



/*
   Source for this Algorithm
   URLs:
   	1: 	https://csrc.nist.gov/csrc/media/publications/fips/197/final/documents/fips-197.pdf
  	2:	https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/AES_Core128.pdf
	3:	https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/AES_Core256.pdf
	4:	https://www.hanewin.net/encrypt/aes/aes-test.htm
	5:	https://dxr.mozilla.org/mozilla-beta/source/security/nss/cmd/bltest/tests/aes_cts/aes-cts-type-1-vectors.txt
	6:	https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
 */

public class AES192 extends AES {
	
	protected int NB = 4;	// Key Length
	protected int NK = 6;	// Block Size
	protected int NR = 12;	// Anzahl der Runden
	
	public AES192() {
		super.NK = NK;
		super.NR = NR;
	}
	
	
}
