package it.sec.ss2019.Aufgabe06;

public class AES {
	
	// Standardmaessig - AES 128 
	protected final int NB = 4;		// Key Length
	protected int NK = 4;		// Block Size
	protected int NR = 10;	// Anzahl der Runden
	
	private int[] exps = genExp(3);
	private int[] logs = genLog(exps);
	private int[] sBox = generateSBox();
	private int[] invSBox = generateInvSBox(sBox);
	private int[] rcon = generateRCON();
	int wIndex = 0;

	
	public int[] encrypt(int[] key, int[] input) {
		int[] w = keyExpansion(key);
		int[][] state = readState(input);
		this.wIndex = addRoundKey(state, w, 0);
		for(int round = 1; round <= (NR - 1); round++) {
			state = subBytes(state);
			state = shiftRows(state);
			state = mixColumns(state);
			this.wIndex = addRoundKey(state, w, this.wIndex);
		}
		state = subBytes(state);
		state = shiftRows(state);
		this.wIndex = addRoundKey(state, w,  this.wIndex);
		return writeState(state);
	}
	
	
	public int[] decrypt(int[] key, int[] input) {
		int[] w = keyExpansion(key);
		int[][] state = readState(input);
		this.wIndex = invAddRoundKey(state, w, this.wIndex);
		for(int round = (NR - 1); round >= 1; round--) {
			state = invShiftRows(state);
			state = invSubBytes(state);
			this.wIndex = invAddRoundKey(state, w,  this.wIndex);
			state = invMixColumns(state);
		}
		state = invShiftRows(state);
		state = invSubBytes(state);
		this.wIndex = invAddRoundKey(state, w,  this.wIndex);		
		return writeState(state);
	}
	
	// ROUTINEN (METHODEN) DER REIHENFOLGE DER VERSCHLUESSELUNG
	
	public int[] keyExpansionTest(int[] key, int index) {
		int[] testKey = keyExpansion(key);
		int[] result = new int[16];
		for(int i = 0; i < result.length; i++) {
			result[i] = testKey[i + (16 * index) ];
		}
		return result;
	}
	
	public int[] keyExpansion(int[] key) {
		
		int[] temp = new int[4];
		int[] w = new int[16 * (NR + 1)]; // 176
		
		int j = 0;
		while(j < (4 * NK)) {
			w[j] = key[j++];
		}

		j = 4 * NK;	
		
		int i = 0;
		
		while(j < 16 * (NR + 1)) { 
			
			i = j / 4; 	
			for(int k = 0; k < 4; k++) {
				temp[k] = w[j - 4 + k];
			}
			
			if((i % NK) == 0) {
				temp = subWord(rotWord(temp));

				temp[0] = temp[0] ^ rcon[i / NK - 1];
				
				
//				int oldTemp = temp[0];
//				for(int k = 0; k < 4; k++) {
//					int ttemp = k == 3 ? oldTemp : temp[k + 1];
//					int tRcon = k == 0 ? rcon[i / NK - 1] : 0;					
//					temp[k] = sBox[ttemp] ^ tRcon;
//				}
			}
			else {
				if(NK > 6 && (i % NK) == 4) {
					temp = subWord(temp);
//					for(int k = 0; k < 4; k++) {
//						temp[k] = sBox[temp[k]];
//					}
				}
			}
			for(int k = 0; k < 4; k++) {
				w[j + k] = w[j - 4 * NK + k] ^ temp[k];
			}
			
			j = j + 4;
		}
		
		return w;
	}
	
	public void printDebug1D(int[] array) {
		String str = "";
		for(int i = 0; i < array.length; i++) {
			str += String.format("%02x ", array[i]);
		}
		System.out.println(str);
	}
	
	public void printDebug2D(int[][] array) {
		String str = "";
		for(int i = 0; i < array.length; i++) {
			for(int j = 0; j < array.length; j++) {
				str += String.format("%02x ", array[j][i]);
			}
		}
		System.out.println(str);
	}

	
	public int[][] subBytes(int[][] state) {
		int[][] rueck = new int[state.length][state[0].length];
		for(int r = 0; r < 4; r++) {
			for(int c = 0; c < 4; c++) {
				rueck[r][c] = sBox[state[r][c]];
			}
		}
		return rueck;
	}
	
	public int[][] invSubBytes(int[][] state) {
		int[][] rueck = new int[state.length][state[0].length];
		for(int r = 0; r < 4; r++) {
			for(int c = 0; c < 4; c++) {
				rueck[r][c] = invSBox[state[r][c]];
			}
		}
		return rueck;
	}
	
	public int[][] shiftRows(int[][] s) {
		int[][] state = new int[s.length][s[0].length];
		state[0] = s[0]; // <- Notwendig, da Arrays
		// r = 1, first Row will not be shifted!
        for (int row = 1; row < s.length; row++) {
            for (int col = 0; col < s[0].length; col++) {
            	state[row][col] = s[row][(col + row) % NB];
            }
        }
        return state;
	}
	
	public int[][] invShiftRows(int[][] s) {
		int[][] state = new int[s.length][s[0].length];
		state[0] = s[0]; // <- Notwendig, da Arrays
		// r = 1, first Row will not be shifted!
        for (int row = 1; row < s.length; row++) {
            for (int col = 0; col < s[0].length; col++) {
            	state[row][(col + row) % NB] = s[row][col];
            }
        }
        return state;
	}
	
	public int[][] mixColumns(int[][] s) {
		int[][] state = new int[s.length][s[0].length];
		
		for(int col = 0; col < state.length; col++) {
			state[0][col] = gfMul(2, s[0][col]) ^ gfMul(3, s[1][col]) ^ s[2][col] ^ s[3][col];
			state[1][col] = s[0][col] ^ gfMul(2, s[1][col]) ^ gfMul(3, s[2][col]) ^ s[3][col];
			state[2][col] = s[0][col] ^ s[1][col] ^ gfMul(2, s[2][col]) ^ gfMul(3, s[3][col]);
			state[3][col] = gfMul(3, s[0][col]) ^ s[1][col] ^ s[2][col] ^ gfMul(2, s[3][col]);
		}
		
		return state;
	}
	
	public int[][] invMixColumns(int[][] s) {
		int[][] state = new int[s.length][s[0].length];
		
		for(int col = 0; col < state.length; col++) {
			state[0][col] = gfMul(0x0e, s[0][col]) ^ gfMul(0x0b, s[1][col]) ^ gfMul(0x0d, s[2][col]) ^ gfMul(0x09, s[3][col]);
			state[1][col] = gfMul(0x09, s[0][col]) ^ gfMul(0x0e, s[1][col]) ^ gfMul(0x0b, s[2][col]) ^ gfMul(0x0d, s[3][col]);
			state[2][col] = gfMul(0x0d, s[0][col]) ^ gfMul(0x09, s[1][col]) ^ gfMul(0x0e, s[2][col]) ^ gfMul(0x0b, s[3][col]);
			state[3][col] = gfMul(0x0b, s[0][col]) ^ gfMul(0x0d, s[1][col]) ^ gfMul(0x09, s[2][col]) ^ gfMul(0x0e, s[3][col]);

		}
		
		return state;
	}
	
	
	// TODO: Anders als in den Folien
	public int addRoundKey(int[][] state, int[] w, int wIndex) {
		for(int c = 0; c < 4; c++) {
			for(int r = 0; r < 4; r++) {
				state[r][c] = state[r][c] ^ w[wIndex];
				wIndex++;
			}
		}
		return wIndex;
	}
	
//	// TODO: Anders als in den Folien
//	public int[][] addRoundKey(int[][] state, int[] w, int wIndex) {
//		int[][] rueck = new int[state.length][state[0].length];
//		rueck = state.clone();
//		for(int c = 0; c < 4; c++) {
//			for(int r = 0; r < 4; r++) {
////				rueck[r][c] = state[r][c] ^ w[wIndex];
//				rueck[r][c] = state[r][c] ^ w[wIndex];
//				wIndex++;
//			}
//		}
//		this.wIndex = wIndex;
//		return rueck;
//	}
	
	
	
	public int invAddRoundKey(int[][] state, int[] w, int wIndex) {
		for(int c = 3; c >= 0; c--) {
			for(int r = 3; r >= 0; r--) {
				wIndex--;
				state[r][c] = state[r][c] ^ w[wIndex];
			}
		}
		return wIndex;
	}
	
//	public int[][] invAddRoundKey(int[][] state, int[] w, int wIndex) {
//		int[][] rueck = new int[state.length][state[0].length];
//		rueck = state.clone();
//		for(int c = 0; c < 4; c++) {
//			for(int r = 0; r < 4; r++) {
//				wIndex--;
//				rueck[r][c] = state[r][c] ^ w[wIndex];
//			}
//		}
//		this.wIndex = wIndex;
//		return rueck;
//	}
	
	
	
	
	// HELFER METHODEN, WELCHE IN DEN ROUTINEN GENUTZT WERDEN
	
	
	public int[] generateSBox() {
		int[] sBox = new int[256];
		for(int i = 0; i < 256; i++) {
			sBox[i] = subByte(i);
		}
		return sBox;
	}
	
	public int[] generateInvSBox(int[] sBoxy) {
		int[] invSBox = new int[256];
		for(int i = 0; i < 256; i++) {
			invSBox[sBoxy[i]] = i;
		}
		return invSBox;
	}
	
//	// TODO: Vielleicht brauche ich mehr (UNOPTIMIERT)
//	public int[] generateRCON() {
//		int[] rcon = new int[20];
//		int val = 1;
//		rcon[0] = shiftLeft(1, 24);
//		for(int i = 1; i < 20; i++) {
//			val = gfMul(val, 2);
//			rcon[i] = shiftLeft(val, 24);
//		}
//		return rcon;
//	}
	
	public int[] generateRCON() {
		int[] rcon = new int[20];
		int val = 1;
		rcon[0] = 0x01;
		for(int i = 1; i < 20; i++) {
			val = gfMul(val, 2);
			rcon[i] = val;
		}
		return rcon;
	}
	
	public int[][] readState(int[] input) {
		int[][] state = new int[4][4];
		for(int c = 0; c < 4; c++) {
			for(int r = 0; r < 4; r++) {
				state[r][c] = input[c * 4 + r];
			}
		}
		return state;
	}
	
	public int[] writeState(int[][] state) {
		int[] output = new int[16];
		for(int c = 0; c < 4; c++) {
			for(int r = 0; r < 4; r++) {
				output[c * 4 + r] = state[r][c];
			}
		}
		return output;
	}
		
	public int[] subWord(int[] word) {
		int[] newWord = new int[word.length];
		for(int i = 0; i < newWord.length; i++) {
			newWord[i] = subByte(word[i]);
		}
		return newWord;
	}
	


	
	public int[] rotWord(int[] word) {
		int[] newWord = new int[word.length];
		newWord[0] = word[1];
		newWord[1] = word[2];
		newWord[2] = word[3];
		newWord[3] = word[0];		
		return newWord;
	}
	
	
	public int[] genExp(int val) {
		int element = 1;
		int[] result = new int[256];
		result[0] = 1;
		for(int i = 1; i < 256; i++) {
			element = gfMul_slow(element, val);
			result[i] = element;
			if(element == 1) {
				return result;
			}
		}
		// Nur damit Eclipse nicht meckert
		return result;
	}
	
	public int[] genLog(int[] exps) {
		int[] logs = new int[256];
		logs[0] = 0;
		for(int i = 0; i < 255; i++) {
			logs[exps[i]] = i;
		}
		return logs;
	}
	
	// FUER DIE RECHNUNGEN
	
	public int gfAdd(int a, int b) {
		return a ^ b;
	}
	
//	private int gfSub(int a, int b) {
//		return a ^ b;
//	}
	
	public int gfMul(int a, int b) {
		if(a == 0 || b == 0) {
			return 0;
		}
		int t = logs[a] + logs[b];
		if(t > 255) {
			t -= 255;
		}
		
		return exps[t];
	}
	
	public int gfMulInverse(int a) {
		int t = 0xff - logs[a];
		return exps[t];
	}
	
	public int gfMul_slow(int a, int b) {
		int result = 0;
		while(a != 0) {
			if(bit(a, 0) == 1) {
				result = result ^ b;
			}
			b = xtimes(b);
			a = shiftRight(a, 1);
		}
		return result;

	}
	
	public int xtimes(int x) {
		// irreducible polynomial
		int irrpoly = 0x011B;
		boolean over = bit(x, 7) == 1;
		x = shiftLeft(x, 1);
		if(over) {
			x = x ^ irrpoly;
		}
		return x;
	}
	
	public int subByte(int b) {
		int t = 0;
		int res = 0;
		final int c = 0x63;
		if(b != 0) {
			b = gfMulInverse(b);
		}
		for(int i = 0; i <= 7; i++) {
			t = bit(b, i) ^ bit(b, (i+4) % 8) ^ bit(b, (i+5) % 8)
					^ bit(b, (i+6) % 8) ^ bit(b, (i+7) % 8) ^ bit(c, i);
			res = res ^ shiftLeft(t, i);
		}
		return res;
	}
	
	// BITSPIELEREI
	
	public int shiftLeft(int value, int toShift) {
		return value << toShift;
	}
	
	public int shiftRight(int value, int toShift) {
		return value >> toShift;
	}
	
	public int bit(int number, int index) {
		return (number >>> index) & 0x1;
	}
	

    public int[] stringToHex(String str) {
    	
    	int size = str.length() / 2;
    	
    	int[] hexBytes = new int[size];
    	for (int i = 0; i < hexBytes.length - 1; i++) {
    		hexBytes[i] = Integer.parseInt(str.substring((i * 2), (i * 2) + 2), 16);
		}
    	int i = hexBytes.length - 1;
    	hexBytes[i] = ( (str.length() % 2) != 0) ? Integer.parseInt(str.substring(i * 2, i * 2 + 1), 16) 
    			: Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16); 
    	// hexBytes[i] = (byte) Integer.parseInt(str.substring((i * 2), (i * 2) + 2), 16);
    	
    	return hexBytes;
    }
    
    public String hexToString(int[] hex) {
    	String str = "";
    	for(int i = 0; i < hex.length; i++) {
    		str += String.format("%02x", hex[i]);
    	}
    	
    	return str;
    }
	
	

}
