package it.sec.ss2019.Aufgabe06;



/*
   Source for this Algorithm
   URLs:
   	1: 	https://csrc.nist.gov/csrc/media/publications/fips/197/final/documents/fips-197.pdf
  	2:	https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/AES_Core128.pdf
	3:	https://csrc.nist.gov/CSRC/media/Projects/Cryptographic-Standards-and-Guidelines/documents/examples/AES_Core256.pdf
	4:	https://www.hanewin.net/encrypt/aes/aes-test.htm
	5:	https://dxr.mozilla.org/mozilla-beta/source/security/nss/cmd/bltest/tests/aes_cts/aes-cts-type-1-vectors.txt
	6:	https://en.wikipedia.org/wiki/Advanced_Encryption_Standard
 */

public class AES256 extends AES {

	
	public static final int BIT_LENGTH_AES_256 = 256;
	public static final int BYTE_LENGTH_AES_256 = BIT_LENGTH_AES_256 / 8;
	
	private final byte NB = 4;	// Key Length
	private final byte NK = 8;	// Block Size
	private final byte NR = 14;	// Anzahl der Runden
	
	
	public AES256() {
		super.NK = NK;
		super.NR = NR;
	}
	


	
	
}
