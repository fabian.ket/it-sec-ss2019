/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.sec.ss2019.Aufgabe06.testcases;

import de.htw_berlin.f4.cb.io.DataFile;
import de.htw_berlin.f4.cb.io.FileNameInPackage;
import de.htw_berlin.f4.cb.io.IDataSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Burkhard Messer <burkhard.messer@htw-berlin.de>
 */
public class GetTests implements Iterable<HashMap<String, List<String>>>{

    private TestFileIO tfile= null;
    
    public GetTests(String packageNameParam, String dir, String fileName) {
        FileNameInPackage fn;
        IDataSource df;
        fn= new FileNameInPackage(true, packageNameParam,dir);
        fn.setFileName(fileName);
        df= new DataFile(fn.getFileName());
        tfile= new TestFileIO(df);
    }
    
    @Override
    public Iterator<HashMap<String, List<String>>> iterator() { 
        return new TestIterator(this);
    }
    
    private class TestIterator implements Iterator<HashMap<String, List<String>>> { 
        final static String DEFAULT_BITSIZE= "256";
        TestLine tl;
        TestLine defaultSize= new TestLine("s", DEFAULT_BITSIZE,0);;
      
        TestIterator(GetTests obj) { 
            initialization();
        } 
        TestIterator() { 
            initialization();
        } 

        private void initialization() {
            if(tfile.hasNext()) {
                tl= tfile.next();  // possible s=number
                if(tl.name.equals("s")) {
                    defaultSize.data= tl.data;
                    if(tfile.hasNext()) {
                        tl= tfile.next();
                    }
                } 
                // skip to title
                while(!tl.name.equals("t")) {
                    if(tfile.hasNext()) {
                        tl= tfile.next();
                    } else {
                        tl= null;
                        break;
                    }
                }
            } else {
                tl= null;
            }
        }
        @Override
        public boolean hasNext() { 
            return tfile.hasNext();
        } 

        @Override
        public HashMap<String, List<String>> next() { 
            HashMap<String, List<String>> testCase= new HashMap<>();
            
            List<String> list = new ArrayList<>();
            list.add(defaultSize.data);
            
            testCase.put(defaultSize.name, list);
            if(tl!=null) {
            	list = new ArrayList<>();
            	list.add(Integer.toString(tl.lineNo));
                testCase.put("Line", list);
//                System.out.println("******************|"+tl.name+"|=|"+tl.data+"|");
                
                list = new ArrayList<>();
                list.add(tl.data);
                
                testCase.put(tl.name, list); // title
                while (tfile.hasNext()) {
                    tl= tfile.next();
                    if(tl.name.equals("t")) {
                        return testCase;
                    } else {
                    	
                    	list = testCase.getOrDefault(tl.name, new ArrayList<>());
                    	list.add(tl.data);
                    	
                        testCase.put(tl.name, list);
//                        System.out.println("|"+tl.name+"|=|"+tl.data+"|"); 
                    }
                } 
                tl= null;
                return testCase;    // last test
            }
            return null;        // no test
        } 

        @Override
        public void remove() { 
            throw new UnsupportedOperationException();
        } 
    }
} 