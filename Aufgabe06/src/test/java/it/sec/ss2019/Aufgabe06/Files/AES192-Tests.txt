# Testdaten f�r AES-192
#
# # Kommentar bis Zeilenende
# t is the title of the test
# k Key   192 bit (24 Byte)
# p plain 128 bit (16 byte)
# Es gibt kein Padding und keine Blockmodi. Die Daten sind
# immer 16 byte lang.
# Alle Zahlen sind in Hex angegeben. Es gibt immer eine
# gerade Anzahl von Hexzahlen, so dass alles immer in Bytes passt.
# Es werden dieselben Bezeichnungen wie in FIPS verwendet.
#
# encryption
#
# Legend for CIPHER (ENCRYPT) (round number r = 0 to 10, 12 or 14):
#     input: cipher input
#     start: state at start of round[r]
#     s_box: state after SubBytes()
#     s_row: state after ShiftRows()
#     m_col: state after MixColumns()
#     k_sch: key schedule value for round[r]
#     output: cipher output
# Die "Messpunkte" sind bei der Verschluesselung die folgenden:
#   --> "input"
#   --> "k_sch"
#   addRoundKey();
#   for(i = 1; i <= loops; ++i) {
#      -->"start"
#      byteSub();
#      -->"s_box"
#      shiftRow();
#      -->"s_row"
#      // the last iteration does not use mixColumn
#      if(i < loops) {
#          mixColumn();
#          -->"m_col"
#      }
#   -->"k_sch"
#   addRoundKey();
#   }
#   -->"output"
#
# decryption
#
# Legend for INVERSE CIPHER (DECRYPT) (round number r = 0 to 10, 12 or 14):
#     iinput: inverse cipher input
#     istart: state at start of round[r]
#     is_box: state after InvSubBytes()
#     is_row: state after InvShiftRows()
#     ik_sch: key schedule value for round[r]
#     ik_add: state after AddRoundKey()
#     ioutput: inverse cipher output
# Die "Messpunkte" sind bei der Entschluesselung die folgenden:
#  -->"iinput"
#  -->ik_sch"
#  addRoundKey();
#  for(i = 1; i <= loops; ++i) {
#     -->"istart"
#     shiftRow();
#     -->"is_row"
#     byteSub();
#     -->"is_box"
#     -->"ik_sch"
#     addRoundKey();
#     // the last iteration does not use mixColumn
#     if(i < loops) {
#        -->"ik_add"
#        mixColumn();
#     } else {
#        -->"ioutput"
#     }
#  }
#
# Der erste Test stammt aus FIPS-197 (ist also offiziell).
#
# Version 2 22/02/2019
#
t=AES192-1
k=+0x000102030405060708090a0b0c0d0e0f1011121314151617
p=+0x00112233445566778899aabbccddeeff
# encryption
round[00].input  =+0x00112233445566778899aabbccddeeff
round[00].k_sch  =+0x000102030405060708090a0b0c0d0e0f
round[01].start  =+0x00102030405060708090a0b0c0d0e0f0
round[01].s_box  =+0x63cab7040953d051cd60e0e7ba70e18c
round[01].s_row  =+0x6353e08c0960e104cd70b751bacad0e7
round[01].m_col  =+0x5f72641557f5bc92f7be3b291db9f91a
round[01].k_sch  =+0x10111213141516175846f2f95c43f4fe
round[02].start  =+0x4f63760643e0aa85aff8c9d041fa0de4
round[02].s_box  =+0x84fb386f1ae1ac977941dd70832dd769
round[02].s_row  =+0x84e1dd691a41d76f792d389783fbac70
round[02].m_col  =+0x9f487f794f955f662afc86abd7f1ab29
round[02].k_sch  =+0x544afef55847f0fa4856e2e95c43f4fe
round[03].start  =+0xcb02818c17d2af9c62aa64428bb25fd7
round[03].s_box  =+0x1f770c64f0b579deaaac432c3d37cf0e
round[03].s_row  =+0x1fb5430ef0accf64aa370cde3d77792c
round[03].m_col  =+0xb7a53ecbbf9d75a0c40efc79b674cc11
round[03].k_sch  =+0x40f949b31cbabd4d48f043b810b7b342
round[04].start  =+0xf75c7778a327c8ed8cfebfc1a6c37f53
round[04].s_box  =+0x684af5bc0acce85564bb0878242ed2ed
round[04].s_row  =+0x68cc08ed0abbd2bc642ef555244ae878
round[04].m_col  =+0x7a1e98bdacb6d1141a6944dd06eb2d3e
round[04].k_sch  =+0x58e151ab04a2a5557effb5416245080c
round[05].start  =+0x22ffc916a81474416496f19c64ae2532
round[05].s_box  =+0x9316dd47c2fa92834390a1de43e43f23
round[05].s_row  =+0x93faa123c2903f4743e4dd83431692de
round[05].m_col  =+0xaaa755b34cffe57cef6f98e1f01c13e6
round[05].k_sch  =+0x2ab54bb43a02f8f662e3a95d66410c08
round[06].start  =+0x80121e0776fd1d8a8d8c31bc965d1fee
round[06].s_box  =+0xcdc972c53854a47e5d64c765904cc028
round[06].s_row  =+0xcd54c7283864c0c55d4c727e90c9a465
round[06].m_col  =+0x921f748fd96e937d622d7725ba8ba50c
round[06].k_sch  =+0xf501857297448d7ebdf1c6ca87f33e3c
round[07].start  =+0x671ef1fd4e2a1e03dfdcb1ef3d789b30
round[07].s_box  =+0x8572a1542fe5727b9e86c8df27bc1404
round[07].s_row  =+0x85e5c8042f8614549ebca17b277272df
round[07].m_col  =+0xe913e7b18f507d4b227ef652758acbcc
round[07].k_sch  =+0xe510976183519b6934157c9ea351f1e0
round[08].start  =+0x0c0370d00c01e622166b8accd6db3a2c
round[08].s_box  =+0xfe7b5170fe7c8e93477f7e4bf6b98071
round[08].s_row  =+0xfe7c7e71fe7f807047b95193f67b8e4b
round[08].m_col  =+0x6cf5edf996eb0a069c4ef21cbfc25762
round[08].k_sch  =+0x1ea0372a995309167c439e77ff12051e
round[09].start  =+0x7255dad30fb80310e00d6c6b40d0527c
round[09].s_box  =+0x40fc5766766c7bcae1d7507f09700010
round[09].s_row  =+0x406c501076d70066e17057ca09fc7b7f
round[09].m_col  =+0x7478bcdce8a50b81d4327a9009188262
round[09].k_sch  =+0xdd7e0e887e2fff68608fc842f9dcc154
round[10].start  =+0xa906b254968af4e9b4bdb2d2f0c44336
round[10].s_box  =+0xd36f3720907ebf1e8d7a37b58c1c1a05
round[10].s_row  =+0xd37e3705907a1a208d1c371e8c6fbfb5
round[10].m_col  =+0x0d73cc2d8f6abe8b0cf2dd9bb83d422e
round[10].k_sch  =+0x859f5f237a8d5a3dc0c02952beefd63a
round[11].start  =+0x88ec930ef5e7e4b6cc32f4c906d29414
round[11].s_box  =+0xc4cedcabe694694e4b23bfdd6fb522fa
round[11].s_row  =+0xc494bffae62322ab4bb5dc4e6fce69dd
round[11].m_col  =+0x71d720933b6d677dc00b8f28238e0fb7
round[11].k_sch  =+0xde601e7827bcdf2ca223800fd8aeda32
round[12].start  =+0xafb73eeb1cd1b85162280f27fb20d585
round[12].s_box  =+0x79a9b2e99c3e6cd1aa3476cc0fb70397
round[12].s_row  =+0x793e76979c3403e9aab7b2d10fa96ccc
round[12].k_sch  =+0xa4970a331a78dc09c418c271e3a41d5d
round[12].output =+0xdda97ca4864cdfe06eaf70a0ec0d7191
# decryption
round[00].iinput =+0xdda97ca4864cdfe06eaf70a0ec0d7191
round[00].ik_sch =+0xa4970a331a78dc09c418c271e3a41d5d
round[01].istart =+0x793e76979c3403e9aab7b2d10fa96ccc
round[01].is_row =+0x79a9b2e99c3e6cd1aa3476cc0fb70397
round[01].is_box =+0xafb73eeb1cd1b85162280f27fb20d585
round[01].ik_sch =+0xde601e7827bcdf2ca223800fd8aeda32
round[01].ik_add =+0x71d720933b6d677dc00b8f28238e0fb7
round[02].istart =+0xc494bffae62322ab4bb5dc4e6fce69dd
round[02].is_row =+0xc4cedcabe694694e4b23bfdd6fb522fa
round[02].is_box =+0x88ec930ef5e7e4b6cc32f4c906d29414
round[02].ik_sch =+0x859f5f237a8d5a3dc0c02952beefd63a
round[02].ik_add =+0x0d73cc2d8f6abe8b0cf2dd9bb83d422e
round[03].istart =+0xd37e3705907a1a208d1c371e8c6fbfb5
round[03].is_row =+0xd36f3720907ebf1e8d7a37b58c1c1a05
round[03].is_box =+0xa906b254968af4e9b4bdb2d2f0c44336
round[03].ik_sch =+0xdd7e0e887e2fff68608fc842f9dcc154
round[03].ik_add =+0x7478bcdce8a50b81d4327a9009188262
round[04].istart =+0x406c501076d70066e17057ca09fc7b7f
round[04].is_row =+0x40fc5766766c7bcae1d7507f09700010
round[04].is_box =+0x7255dad30fb80310e00d6c6b40d0527c
round[04].ik_sch =+0x1ea0372a995309167c439e77ff12051e
round[04].ik_add =+0x6cf5edf996eb0a069c4ef21cbfc25762
round[05].istart =+0xfe7c7e71fe7f807047b95193f67b8e4b
round[05].is_row =+0xfe7b5170fe7c8e93477f7e4bf6b98071
round[05].is_box =+0x0c0370d00c01e622166b8accd6db3a2c
round[05].ik_sch =+0xe510976183519b6934157c9ea351f1e0
round[05].ik_add =+0xe913e7b18f507d4b227ef652758acbcc
round[06].istart =+0x85e5c8042f8614549ebca17b277272df
round[06].is_row =+0x8572a1542fe5727b9e86c8df27bc1404
round[06].is_box =+0x671ef1fd4e2a1e03dfdcb1ef3d789b30
round[06].ik_sch =+0xf501857297448d7ebdf1c6ca87f33e3c
round[06].ik_add =+0x921f748fd96e937d622d7725ba8ba50c
round[07].istart =+0xcd54c7283864c0c55d4c727e90c9a465
round[07].is_row =+0xcdc972c53854a47e5d64c765904cc028
round[07].is_box =+0x80121e0776fd1d8a8d8c31bc965d1fee
round[07].ik_sch =+0x2ab54bb43a02f8f662e3a95d66410c08
round[07].ik_add =+0xaaa755b34cffe57cef6f98e1f01c13e6
round[08].istart =+0x93faa123c2903f4743e4dd83431692de
round[08].is_row =+0x9316dd47c2fa92834390a1de43e43f23
round[08].is_box =+0x22ffc916a81474416496f19c64ae2532
round[08].ik_sch =+0x58e151ab04a2a5557effb5416245080c
round[08].ik_add =+0x7a1e98bdacb6d1141a6944dd06eb2d3e
round[09].istart =+0x68cc08ed0abbd2bc642ef555244ae878
round[09].is_row =+0x684af5bc0acce85564bb0878242ed2ed
round[09].is_box =+0xf75c7778a327c8ed8cfebfc1a6c37f53
round[09].ik_sch =+0x40f949b31cbabd4d48f043b810b7b342
round[09].ik_add =+0xb7a53ecbbf9d75a0c40efc79b674cc11
round[10].istart =+0x1fb5430ef0accf64aa370cde3d77792c
round[10].is_row =+0x1f770c64f0b579deaaac432c3d37cf0e
round[10].is_box =+0xcb02818c17d2af9c62aa64428bb25fd7
round[10].ik_sch =+0x544afef55847f0fa4856e2e95c43f4fe
round[10].ik_add =+0x9f487f794f955f662afc86abd7f1ab29
round[11].istart =+0x84e1dd691a41d76f792d389783fbac70
round[11].is_row =+0x84fb386f1ae1ac977941dd70832dd769
round[11].is_box =+0x4f63760643e0aa85aff8c9d041fa0de4
round[11].ik_sch =+0x10111213141516175846f2f95c43f4fe
round[11].ik_add =+0x5f72641557f5bc92f7be3b291db9f91a
round[12].istart =+0x6353e08c0960e104cd70b751bacad0e7
round[12].is_row =+0x63cab7040953d051cd60e0e7ba70e18c
round[12].is_box =+0x00102030405060708090a0b0c0d0e0f0
round[12].ik_sch =+0x000102030405060708090a0b0c0d0e0f
round[12].ioutput=+0x00112233445566778899aabbccddeeff
#--------------------------
t=AES192-2
k=+0x8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b
p=+0x6bc1bee22e409f96e93d7e117393172a
# encryption
round[00].input  =+0x6bc1bee22e409f96e93d7e117393172a
round[00].k_sch  =+0x8e73b0f7da0e6452c810f32b809079e5
round[01].start  =+0xe5b20e15f44efbc4212d8d3af3036ecf
round[01].s_box  =+0xd937ab59bf2f0f1cfdd85d800d7b9f8a
round[01].s_row  =+0xd92f5d8abfd89f59fd7bab1c0d370f80
round[01].m_col  =+0x0feac90dd0f7a92fdbf1eff4ccf2bf34
round[01].k_sch  =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[02].start  =+0x6d1223df82dbc25425fd7e03e8f04a91
round[02].s_box  =+0x3cc9269e13b925203f54f37b9b8cd681
round[02].s_row  =+0x3cb9f3811354d69e3f8c26209bc9257b
round[02].m_col  =+0xdadae01792444990f7769fab330695ac
round[02].k_sch  =+0xec12068e6c827f6b0e7a95b95c56fec2
round[03].start  =+0x36c8e699fec636fbf90c0a126f506b6e
round[03].s_box  =+0x05e88eeebbb4050f99fe67c9a8537f9f
round[03].s_row  =+0x05b4679fbbfe7fee99538e0fa8e805c9
round[03].m_col  =+0x3540c5f9e53392905db9dc73a4a50a87
round[03].k_sch  =+0x4db7b4bd69b5411885a74796e92538fd
round[04].start  =+0x78f771448c86d388d81e9be54d80327a
round[04].s_box  =+0xbc68a31b644466c4617214d9e3cd23da
round[04].s_row  =+0xbc4414da6472231b61cda3c4e36866d9
round[04].m_col  =+0x61d2a52066fe7dcbe9daa65eda403799
round[04].k_sch  =+0xe75fad44bb095386485af05721efb14f
round[05].start  =+0x868d0864ddf72e4da1805609fbaf86d6
round[05].s_box  =+0x445d3043c16831e332cdb1010f7944f6
round[05].s_row  =+0x4468b1f6c1cd4443327930e30f5d3101
round[05].m_col  =+0x77aa54e2d2cf41573c7315c2c9e7337f
round[05].k_sch  =+0xa448f6d94d6dce24aa326360113b30e6
round[06].start  =+0xd3e2a23b9fa28f73964176a2d8dc0399
round[06].s_box  =+0x66983ae2db3a738f9083383a61867bee
round[06].s_row  =+0x663a38eedb837be290863a8f6198733a
round[06].m_col  =+0x54b4056faaa993511f46e81238e5513c
round[06].k_sch  =+0xa25e7ed583b1cf9a27f939436a94f767
round[07].start  =+0xf6ea7bba29185ccb38bfd1515271a65b
round[07].s_box  =+0x428721f4a5ad4a1f07083ed100a32439
round[07].s_row  =+0x42ad3e39a50824f407a3211f00874ad1
round[07].m_col  =+0x6f78d827992de22bce26c7b5091a7b74
round[07].k_sch  =+0xc0a69407d19da4e1ec1786eb6fa64971
round[08].start  =+0xafde4c2048b046ca2231415e66bc3205
round[08].s_box  =+0x791d29b752e75a7493c783583365236b
round[08].s_row  =+0x79e7836b52c723b793652974331d5a58
round[08].m_col  =+0x28593e3962151167cf56380a43bf72a2
round[08].k_sch  =+0x485f703222cb8755e26d135233f0b7b3
round[09].start  =+0x60064e0b40de96322d3b2b58704fc511
round[09].s_box  =+0xd06f2f2b091d9023d8e2f16a5184a682
round[09].s_row  =+0xd01df18209e2a62bd8842f23516f906a
round[09].m_col  =+0xef60a998a20cc1093099679ee94ebbd8
round[09].k_sch  =+0x40beeb282f18a2596747d26b458c553e
round[10].start  =+0xafde42b08d14635057deb5f5acc2eee6
round[10].s_box  =+0x791d2ce75dfafb535b1dd5e69125288e
round[10].s_row  =+0x79fad58e5d1d28e75b252c53911dfbe6
round[10].m_col  =+0xbc7cbba352f82207a636d342035b5099
round[10].k_sch  =+0xa7e1466c9411f1df821f750aad07d753
round[11].start  =+0x1b9dfdcfc6e9d3d82429a648ae5c87ca
round[11].s_box  =+0xaf5e548ab41e666136a52452e44a1774
round[11].s_row  =+0xaf1e2474b4a5178a364a5461e45e6652
round[11].m_col  =+0x378b65381a56ba7a873f778605a080ab
round[11].k_sch  =+0xca4005388fcc5006282d166abc3ce7b5
round[12].start  =+0xfdcb6000959aea7caf1261ecb99c671e
round[12].s_box  =+0x541fd0632ab8871079c9efce56de8572
round[12].s_row  =+0x54b8ef722ac9856379ded010561f87ce
round[12].k_sch  =+0xe98ba06f448c773c8ecc720401002202
round[12].output =+0xbd334f1d6e45f25ff712a214571fa5cc
# decryption
round[00].iinput =+0xbd334f1d6e45f25ff712a214571fa5cc
round[00].ik_sch =+0xe98ba06f448c773c8ecc720401002202
round[01].istart =+0x54b8ef722ac9856379ded010561f87ce
round[01].is_row =+0x541fd0632ab8871079c9efce56de8572
round[01].is_box =+0xfdcb6000959aea7caf1261ecb99c671e
round[01].ik_sch =+0xca4005388fcc5006282d166abc3ce7b5
round[01].ik_add =+0x378b65381a56ba7a873f778605a080ab
round[02].istart =+0xaf1e2474b4a5178a364a5461e45e6652
round[02].is_row =+0xaf5e548ab41e666136a52452e44a1774
round[02].is_box =+0x1b9dfdcfc6e9d3d82429a648ae5c87ca
round[02].ik_sch =+0xa7e1466c9411f1df821f750aad07d753
round[02].ik_add =+0xbc7cbba352f82207a636d342035b5099
round[03].istart =+0x79fad58e5d1d28e75b252c53911dfbe6
round[03].is_row =+0x791d2ce75dfafb535b1dd5e69125288e
round[03].is_box =+0xafde42b08d14635057deb5f5acc2eee6
round[03].ik_sch =+0x40beeb282f18a2596747d26b458c553e
round[03].ik_add =+0xef60a998a20cc1093099679ee94ebbd8
round[04].istart =+0xd01df18209e2a62bd8842f23516f906a
round[04].is_row =+0xd06f2f2b091d9023d8e2f16a5184a682
round[04].is_box =+0x60064e0b40de96322d3b2b58704fc511
round[04].ik_sch =+0x485f703222cb8755e26d135233f0b7b3
round[04].ik_add =+0x28593e3962151167cf56380a43bf72a2
round[05].istart =+0x79e7836b52c723b793652974331d5a58
round[05].is_row =+0x791d29b752e75a7493c783583365236b
round[05].is_box =+0xafde4c2048b046ca2231415e66bc3205
round[05].ik_sch =+0xc0a69407d19da4e1ec1786eb6fa64971
round[05].ik_add =+0x6f78d827992de22bce26c7b5091a7b74
round[06].istart =+0x42ad3e39a50824f407a3211f00874ad1
round[06].is_row =+0x428721f4a5ad4a1f07083ed100a32439
round[06].is_box =+0xf6ea7bba29185ccb38bfd1515271a65b
round[06].ik_sch =+0xa25e7ed583b1cf9a27f939436a94f767
round[06].ik_add =+0x54b4056faaa993511f46e81238e5513c
round[07].istart =+0x663a38eedb837be290863a8f6198733a
round[07].is_row =+0x66983ae2db3a738f9083383a61867bee
round[07].is_box =+0xd3e2a23b9fa28f73964176a2d8dc0399
round[07].ik_sch =+0xa448f6d94d6dce24aa326360113b30e6
round[07].ik_add =+0x77aa54e2d2cf41573c7315c2c9e7337f
round[08].istart =+0x4468b1f6c1cd4443327930e30f5d3101
round[08].is_row =+0x445d3043c16831e332cdb1010f7944f6
round[08].is_box =+0x868d0864ddf72e4da1805609fbaf86d6
round[08].ik_sch =+0xe75fad44bb095386485af05721efb14f
round[08].ik_add =+0x61d2a52066fe7dcbe9daa65eda403799
round[09].istart =+0xbc4414da6472231b61cda3c4e36866d9
round[09].is_row =+0xbc68a31b644466c4617214d9e3cd23da
round[09].is_box =+0x78f771448c86d388d81e9be54d80327a
round[09].ik_sch =+0x4db7b4bd69b5411885a74796e92538fd
round[09].ik_add =+0x3540c5f9e53392905db9dc73a4a50a87
round[10].istart =+0x05b4679fbbfe7fee99538e0fa8e805c9
round[10].is_row =+0x05e88eeebbb4050f99fe67c9a8537f9f
round[10].is_box =+0x36c8e699fec636fbf90c0a126f506b6e
round[10].ik_sch =+0xec12068e6c827f6b0e7a95b95c56fec2
round[10].ik_add =+0xdadae01792444990f7769fab330695ac
round[11].istart =+0x3cb9f3811354d69e3f8c26209bc9257b
round[11].is_row =+0x3cc9269e13b925203f54f37b9b8cd681
round[11].is_box =+0x6d1223df82dbc25425fd7e03e8f04a91
round[11].ik_sch =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[11].ik_add =+0x0feac90dd0f7a92fdbf1eff4ccf2bf34
round[12].istart =+0xd92f5d8abfd89f59fd7bab1c0d370f80
round[12].is_row =+0xd937ab59bf2f0f1cfdd85d800d7b9f8a
round[12].is_box =+0xe5b20e15f44efbc4212d8d3af3036ecf
round[12].ik_sch =+0x8e73b0f7da0e6452c810f32b809079e5
round[12].ioutput=+0x6bc1bee22e409f96e93d7e117393172a
#--------------------------
t=AES192-3
k=+0x8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b
p=+0xae2d8a571e03ac9c9eb76fac45af8e51
# encryption
round[00].input  =+0xae2d8a571e03ac9c9eb76fac45af8e51
round[00].k_sch  =+0x8e73b0f7da0e6452c810f32b809079e5
round[01].start  =+0x205e3aa0c40dc8ce56a79c87c53ff7b4
round[01].s_box  =+0xb75880e01cd7e88bb15cde17a675688d
round[01].s_row  =+0xb7d7de8d1c5c68e0b175808ba658e817
round[01].m_col  =+0x44f64bca54fcabcbed4b593040220c6f
round[01].k_sch  =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[02].start  =+0x260ea11806d0c0b01347c8c76420f9ca
round[02].s_box  =+0xf7ab32ad6f70bae77da0e8c643b79974
round[02].s_row  =+0xf770e8746fa099ad7db732e743abbac6
round[02].m_col  =+0xf940d07211290ac9edb99cd71c1dd643
round[02].k_sch  =+0xec12068e6c827f6b0e7a95b95c56fec2
round[03].start  =+0x1552d6fc7dab75a2e3c3096e404b2881
round[03].s_box  =+0x5900f6b0ff629d3a112e019f09b3340c
round[03].s_row  =+0x5962010cff2e34b011b3f63a09009d9f
round[03].m_col  =+0x19922d90134f727b20571b02102a92a3
round[03].k_sch  =+0x4db7b4bd69b5411885a74796e92538fd
round[04].start  =+0x5425992d7afa3363a5f05c94f90faa5e
round[04].s_box  =+0x203feed8da2dc3fb068c4a229976ac58
round[04].s_row  =+0x202d4a58da8cacd80676eefb993fc322
round[04].m_col  =+0x25fc71b754ee66fe8338a17f899b5d08
round[04].k_sch  =+0xe75fad44bb095386485af05721efb14f
round[05].start  =+0xc2a3dcf3efe73578cb625128a874ec47
round[05].s_box  =+0x250a860ddf9496bc1faad134c292cea0
round[05].s_row  =+0x2594d1a0dfaace0d1f9286bcc20a9634
round[05].m_col  =+0x9cdef37183d4e504a90d45562343a3a9
round[05].k_sch  =+0xa448f6d94d6dce24aa326360113b30e6
round[06].start  =+0x389605a8ceb92b20033f26363278934f
round[06].s_box  =+0x07906bc28b56f1b77b75f70523bcdc84
round[06].s_row  =+0x0756f7848b75dcc27bbc6bb72390f105
round[06].m_col  =+0x872d33bb8cdc00b0f512d32f1915450e
round[06].k_sch  =+0xa25e7ed583b1cf9a27f939436a94f767
round[07].start  =+0x25734d6e0f6dcf2ad2ebea6c7381b269
round[07].s_box  =+0x3f8fe39f763c8ae5b5e987508f0c37f9
round[07].s_row  =+0x3f3c87f976e9379fb50ce3e58f8f8a50
round[07].m_col  =+0x442c061364794b61637650fa555fff2f
round[07].k_sch  =+0xc0a69407d19da4e1ec1786eb6fa64971
round[08].start  =+0x848a9214b5e4ef808f61d6113af9b65e
round[08].s_box  =+0x5f7e4ffad569dfcd73eff68280994e58
round[08].s_row  =+0x5f69f658d5ef4efa73994fcd807edf82
round[08].m_col  =+0xabd429ce2f38b32ad44638c2c484c625
round[08].k_sch  =+0x485f703222cb8755e26d135233f0b7b3
round[09].start  =+0xe38b59fc0df3347f362b2b90f7747196
round[09].s_box  =+0x113dcbb0d70d18d205f1f1606892a390
round[09].s_row  =+0x110df190d7f1a3b00592cbd2683d1860
round[09].m_col  =+0x54934ef4ae60b04bbeae77e9ef5ac55d
round[09].k_sch  =+0x40beeb282f18a2596747d26b458c553e
round[10].start  =+0x142da5dc81781212d9e9a582aad69063
round[10].s_box  =+0xfad806860cbcc9c9351e0613acf660fb
round[10].s_row  =+0xfabc06fb0c1e608635f606c9acd8c913
round[10].m_col  =+0xcd685c42dc16437da4018f26ea54c8d8
round[10].k_sch  =+0xa7e1466c9411f1df821f750aad07d753
round[11].start  =+0x6a891a2e4807b2a2261efa2c47531f8b
round[11].s_box  =+0x02a7a23152c5373af7722d71a0edc03d
round[11].s_row  =+0x02c52d3d5272c031f7eda23aa0a73771
round[11].m_col  =+0x40d9da94c3dce82641f10b39efddfa89
round[11].k_sch  =+0xca4005388fcc5006282d166abc3ce7b5
round[12].start  =+0x8a99dfac4c10b82069dc1d5353e11d3c
round[12].s_box  =+0x7eee9e9129ca6cb7f986a4ededf8a4eb
round[12].s_row  =+0x7ecaa4eb2986a491f9f89eb7edee6ced
round[12].k_sch  =+0xe98ba06f448c773c8ecc720401002202
round[12].output =+0x974104846d0ad3ad7734ecb3ecee4eef
# decryption
round[00].iinput =+0x974104846d0ad3ad7734ecb3ecee4eef
round[00].ik_sch =+0xe98ba06f448c773c8ecc720401002202
round[01].istart =+0x7ecaa4eb2986a491f9f89eb7edee6ced
round[01].is_row =+0x7eee9e9129ca6cb7f986a4ededf8a4eb
round[01].is_box =+0x8a99dfac4c10b82069dc1d5353e11d3c
round[01].ik_sch =+0xca4005388fcc5006282d166abc3ce7b5
round[01].ik_add =+0x40d9da94c3dce82641f10b39efddfa89
round[02].istart =+0x02c52d3d5272c031f7eda23aa0a73771
round[02].is_row =+0x02a7a23152c5373af7722d71a0edc03d
round[02].is_box =+0x6a891a2e4807b2a2261efa2c47531f8b
round[02].ik_sch =+0xa7e1466c9411f1df821f750aad07d753
round[02].ik_add =+0xcd685c42dc16437da4018f26ea54c8d8
round[03].istart =+0xfabc06fb0c1e608635f606c9acd8c913
round[03].is_row =+0xfad806860cbcc9c9351e0613acf660fb
round[03].is_box =+0x142da5dc81781212d9e9a582aad69063
round[03].ik_sch =+0x40beeb282f18a2596747d26b458c553e
round[03].ik_add =+0x54934ef4ae60b04bbeae77e9ef5ac55d
round[04].istart =+0x110df190d7f1a3b00592cbd2683d1860
round[04].is_row =+0x113dcbb0d70d18d205f1f1606892a390
round[04].is_box =+0xe38b59fc0df3347f362b2b90f7747196
round[04].ik_sch =+0x485f703222cb8755e26d135233f0b7b3
round[04].ik_add =+0xabd429ce2f38b32ad44638c2c484c625
round[05].istart =+0x5f69f658d5ef4efa73994fcd807edf82
round[05].is_row =+0x5f7e4ffad569dfcd73eff68280994e58
round[05].is_box =+0x848a9214b5e4ef808f61d6113af9b65e
round[05].ik_sch =+0xc0a69407d19da4e1ec1786eb6fa64971
round[05].ik_add =+0x442c061364794b61637650fa555fff2f
round[06].istart =+0x3f3c87f976e9379fb50ce3e58f8f8a50
round[06].is_row =+0x3f8fe39f763c8ae5b5e987508f0c37f9
round[06].is_box =+0x25734d6e0f6dcf2ad2ebea6c7381b269
round[06].ik_sch =+0xa25e7ed583b1cf9a27f939436a94f767
round[06].ik_add =+0x872d33bb8cdc00b0f512d32f1915450e
round[07].istart =+0x0756f7848b75dcc27bbc6bb72390f105
round[07].is_row =+0x07906bc28b56f1b77b75f70523bcdc84
round[07].is_box =+0x389605a8ceb92b20033f26363278934f
round[07].ik_sch =+0xa448f6d94d6dce24aa326360113b30e6
round[07].ik_add =+0x9cdef37183d4e504a90d45562343a3a9
round[08].istart =+0x2594d1a0dfaace0d1f9286bcc20a9634
round[08].is_row =+0x250a860ddf9496bc1faad134c292cea0
round[08].is_box =+0xc2a3dcf3efe73578cb625128a874ec47
round[08].ik_sch =+0xe75fad44bb095386485af05721efb14f
round[08].ik_add =+0x25fc71b754ee66fe8338a17f899b5d08
round[09].istart =+0x202d4a58da8cacd80676eefb993fc322
round[09].is_row =+0x203feed8da2dc3fb068c4a229976ac58
round[09].is_box =+0x5425992d7afa3363a5f05c94f90faa5e
round[09].ik_sch =+0x4db7b4bd69b5411885a74796e92538fd
round[09].ik_add =+0x19922d90134f727b20571b02102a92a3
round[10].istart =+0x5962010cff2e34b011b3f63a09009d9f
round[10].is_row =+0x5900f6b0ff629d3a112e019f09b3340c
round[10].is_box =+0x1552d6fc7dab75a2e3c3096e404b2881
round[10].ik_sch =+0xec12068e6c827f6b0e7a95b95c56fec2
round[10].ik_add =+0xf940d07211290ac9edb99cd71c1dd643
round[11].istart =+0xf770e8746fa099ad7db732e743abbac6
round[11].is_row =+0xf7ab32ad6f70bae77da0e8c643b79974
round[11].is_box =+0x260ea11806d0c0b01347c8c76420f9ca
round[11].ik_sch =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[11].ik_add =+0x44f64bca54fcabcbed4b593040220c6f
round[12].istart =+0xb7d7de8d1c5c68e0b175808ba658e817
round[12].is_row =+0xb75880e01cd7e88bb15cde17a675688d
round[12].is_box =+0x205e3aa0c40dc8ce56a79c87c53ff7b4
round[12].ik_sch =+0x8e73b0f7da0e6452c810f32b809079e5
round[12].ioutput=+0xae2d8a571e03ac9c9eb76fac45af8e51
#--------------------------
t=AES192-4
k=+0x8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b
p=+0x30c81c46a35ce411e5fbc1191a0a52ef
# encryption
round[00].input  =+0x30c81c46a35ce411e5fbc1191a0a52ef
round[00].k_sch  =+0x8e73b0f7da0e6452c810f32b809079e5
round[01].start  =+0xbebbacb1795280432deb32329a9a2b0a
round[01].s_box  =+0xaeea91c8b600cd1ad8e92323b8b8f167
round[01].s_row  =+0xae002367b6e9f1c8d8b8911ab8eacd23
round[01].m_col  =+0x03ac41046ebfe552f301776ea018b6b2
round[01].k_sch  =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[02].start  =+0x6154abd63c938e290d0de699841a4317
round[02].s_box  =+0xef2062f6ebdc19a5d7d78eee5fa21af0
round[02].s_row  =+0xefdc8ef0ebd71af6d7a262a55f2019ee
round[02].m_col  =+0xc4353f834386091c8f8b45f329da641f
round[02].k_sch  =+0xec12068e6c827f6b0e7a95b95c56fec2
round[03].start  =+0x2827390d2f04767781f1d04a758c9add
round[03].s_box  =+0x34cc12d715f238f50ca170d69d64b8c1
round[03].s_row  =+0x34f270c115a1b8d70c6412f59dcc38d6
round[03].m_col  =+0xd49a7e47bd48bd9353074893808040ff
round[03].k_sch  =+0x4db7b4bd69b5411885a74796e92538fd
round[04].start  =+0x992dcafad4fdfc8bd6a00f0569a57802
round[04].s_box  =+0xeed8742d4854b03df6e0766bf906bc77
round[04].s_row  =+0xee54767748e0bc2df606743df9d8b06b
round[04].m_col  =+0x3aabcfe53a61bcdeb45b5f0941f2e7ae
round[04].k_sch  =+0xe75fad44bb095386485af05721efb14f
round[05].start  =+0xddf462a18168ef58fc01af5e601d56e1
round[05].s_box  =+0xc1bfaa320c45df6ab07c7958d0a4b1f8
round[05].s_row  =+0xc14579f80c7cb132b0a4aa6ad0bfdf58
round[05].m_col  =+0xd738658f1f0e5fbd4c6ce511e69722bb
round[05].k_sch  =+0xa448f6d94d6dce24aa326360113b30e6
round[06].start  =+0x7370935652639199e65e8671f7ac125d
round[06].s_box  =+0x8f51dcb100fb81ee8e5844a36891c94c
round[06].s_row  =+0x8ffb444c0058c9b18e91dcee685181a3
round[06].m_col  =+0x1be228ad904119e89d26950301f1de35
round[06].k_sch  =+0xa25e7ed583b1cf9a27f939436a94f767
round[07].start  =+0xb9bc567813f0d672badfac406b652952
round[07].s_box  =+0x5665b1bc7d8cf640f49e91097f4da500
round[07].s_row  =+0x568c91007d9ea5bcf44db1407f65f609
round[07].m_col  =+0xb2fde3e75a126ddfd5e6007baebdf600
round[07].k_sch  =+0xc0a69407d19da4e1ec1786eb6fa64971
round[08].start  =+0x725b77e08b8fc93e39f18690c11bbf71
round[08].s_box  =+0x4039f5e13d73ddb212a1446078af08a3
round[08].s_row  =+0x407344a33da108e112aff5b27839dd60
round[08].m_col  =+0xf2c945aa6b9db43789e18113061640ac
round[08].k_sch  =+0x485f703222cb8755e26d135233f0b7b3
round[09].start  =+0xba963598495633626b8c924135e6f71f
round[09].s_box  =+0xf49096463bb1c3aa7f644f83968e68c0
round[09].s_row  =+0xf4b14fc03b6468467f8e96aa9690c383
round[09].m_col  =+0xb49c8062f40d45cd4b7323d6dc7005ef
round[09].k_sch  =+0x40beeb282f18a2596747d26b458c553e
round[10].start  =+0xf4226b4adb15e7942c34f1bd99fc50d1
round[10].s_box  =+0xbf937fd6b95994227118a17aeeb0533e
round[10].s_row  =+0xbf59a13eb91853d671b07f22ee93947a
round[10].m_col  =+0x11cbfd5ec4aa662c74a95918870ec0da
round[10].k_sch  =+0xa7e1466c9411f1df821f750aad07d753
round[11].start  =+0xb62abb3250bb97f3f6b62c122a091789
round[11].s_box  =+0x4ee5ea2353ea880d424e71c9e501f0a7
round[11].s_row  =+0x4eea71a7534ef0234201ea0de5e588c9
round[11].m_col  =+0x6fb5b41ca7e7830d60689b37a47e4bd0
round[11].k_sch  =+0xca4005388fcc5006282d166abc3ce7b5
round[12].start  =+0xa5f5b124282bd30b48458d5d1842ac65
round[12].s_box  =+0x06e6c83634f1662b526e5d4cad2c914d
round[12].s_row  =+0x06f15d4d346e9136522cc82bade6664c
round[12].k_sch  =+0xe98ba06f448c773c8ecc720401002202
round[12].output =+0xef7afd2270e2e60adce0ba2face6444e
# decryption
round[00].iinput =+0xef7afd2270e2e60adce0ba2face6444e
round[00].ik_sch =+0xe98ba06f448c773c8ecc720401002202
round[01].istart =+0x06f15d4d346e9136522cc82bade6664c
round[01].is_row =+0x06e6c83634f1662b526e5d4cad2c914d
round[01].is_box =+0xa5f5b124282bd30b48458d5d1842ac65
round[01].ik_sch =+0xca4005388fcc5006282d166abc3ce7b5
round[01].ik_add =+0x6fb5b41ca7e7830d60689b37a47e4bd0
round[02].istart =+0x4eea71a7534ef0234201ea0de5e588c9
round[02].is_row =+0x4ee5ea2353ea880d424e71c9e501f0a7
round[02].is_box =+0xb62abb3250bb97f3f6b62c122a091789
round[02].ik_sch =+0xa7e1466c9411f1df821f750aad07d753
round[02].ik_add =+0x11cbfd5ec4aa662c74a95918870ec0da
round[03].istart =+0xbf59a13eb91853d671b07f22ee93947a
round[03].is_row =+0xbf937fd6b95994227118a17aeeb0533e
round[03].is_box =+0xf4226b4adb15e7942c34f1bd99fc50d1
round[03].ik_sch =+0x40beeb282f18a2596747d26b458c553e
round[03].ik_add =+0xb49c8062f40d45cd4b7323d6dc7005ef
round[04].istart =+0xf4b14fc03b6468467f8e96aa9690c383
round[04].is_row =+0xf49096463bb1c3aa7f644f83968e68c0
round[04].is_box =+0xba963598495633626b8c924135e6f71f
round[04].ik_sch =+0x485f703222cb8755e26d135233f0b7b3
round[04].ik_add =+0xf2c945aa6b9db43789e18113061640ac
round[05].istart =+0x407344a33da108e112aff5b27839dd60
round[05].is_row =+0x4039f5e13d73ddb212a1446078af08a3
round[05].is_box =+0x725b77e08b8fc93e39f18690c11bbf71
round[05].ik_sch =+0xc0a69407d19da4e1ec1786eb6fa64971
round[05].ik_add =+0xb2fde3e75a126ddfd5e6007baebdf600
round[06].istart =+0x568c91007d9ea5bcf44db1407f65f609
round[06].is_row =+0x5665b1bc7d8cf640f49e91097f4da500
round[06].is_box =+0xb9bc567813f0d672badfac406b652952
round[06].ik_sch =+0xa25e7ed583b1cf9a27f939436a94f767
round[06].ik_add =+0x1be228ad904119e89d26950301f1de35
round[07].istart =+0x8ffb444c0058c9b18e91dcee685181a3
round[07].is_row =+0x8f51dcb100fb81ee8e5844a36891c94c
round[07].is_box =+0x7370935652639199e65e8671f7ac125d
round[07].ik_sch =+0xa448f6d94d6dce24aa326360113b30e6
round[07].ik_add =+0xd738658f1f0e5fbd4c6ce511e69722bb
round[08].istart =+0xc14579f80c7cb132b0a4aa6ad0bfdf58
round[08].is_row =+0xc1bfaa320c45df6ab07c7958d0a4b1f8
round[08].is_box =+0xddf462a18168ef58fc01af5e601d56e1
round[08].ik_sch =+0xe75fad44bb095386485af05721efb14f
round[08].ik_add =+0x3aabcfe53a61bcdeb45b5f0941f2e7ae
round[09].istart =+0xee54767748e0bc2df606743df9d8b06b
round[09].is_row =+0xeed8742d4854b03df6e0766bf906bc77
round[09].is_box =+0x992dcafad4fdfc8bd6a00f0569a57802
round[09].ik_sch =+0x4db7b4bd69b5411885a74796e92538fd
round[09].ik_add =+0xd49a7e47bd48bd9353074893808040ff
round[10].istart =+0x34f270c115a1b8d70c6412f59dcc38d6
round[10].is_row =+0x34cc12d715f238f50ca170d69d64b8c1
round[10].is_box =+0x2827390d2f04767781f1d04a758c9add
round[10].ik_sch =+0xec12068e6c827f6b0e7a95b95c56fec2
round[10].ik_add =+0xc4353f834386091c8f8b45f329da641f
round[11].istart =+0xefdc8ef0ebd71af6d7a262a55f2019ee
round[11].is_row =+0xef2062f6ebdc19a5d7d78eee5fa21af0
round[11].is_box =+0x6154abd63c938e290d0de699841a4317
round[11].ik_sch =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[11].ik_add =+0x03ac41046ebfe552f301776ea018b6b2
round[12].istart =+0xae002367b6e9f1c8d8b8911ab8eacd23
round[12].is_row =+0xaeea91c8b600cd1ad8e92323b8b8f167
round[12].is_box =+0xbebbacb1795280432deb32329a9a2b0a
round[12].ik_sch =+0x8e73b0f7da0e6452c810f32b809079e5
round[12].ioutput=+0x30c81c46a35ce411e5fbc1191a0a52ef
#--------------------------
t=AES192-5
k=+0x8e73b0f7da0e6452c810f32b809079e562f8ead2522c6b7b
p=+0xf69f2445df4f9b17ad2b417be66c3710
# encryption
round[00].input  =+0xf69f2445df4f9b17ad2b417be66c3710
round[00].k_sch  =+0x8e73b0f7da0e6452c810f32b809079e5
round[01].start  =+0x78ec94b20541ff45653bb25066fc4ef5
round[01].s_box  =+0xbcce22376b83166e4de2375333b02fe6
round[01].s_row  =+0xbc8337e66be22f374db0226e33ce1653
round[01].m_col  =+0x2c1e60bcf3f28e1e1d3e0b996add242b
round[01].k_sch  =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[02].start  =+0x4ee68a6ea1dee565e3329a6e4edfd18e
round[02].s_box  =+0x2f8e7e9f321dd94d1123b89f2f9e3e19
round[02].s_row  =+0x2f1db81932233e9f119e7e4d2f8ed99f
round[02].m_col  =+0xd8df72e6a0a9d76ea8f9a44991c7b203
round[02].k_sch  =+0xec12068e6c827f6b0e7a95b95c56fec2
round[03].start  =+0x34cd7468cc2ba805a68331f0cd914cc1
round[03].s_box  =+0x18bd92454bf1c26b24ecc78cbd812978
round[03].s_row  =+0x18f1c7784bec29452481926bbdbdc28c
round[03].m_col  =+0x87cbf4eed5b63a9229fb27a9f30d10a0
round[03].k_sch  =+0x4db7b4bd69b5411885a74796e92538fd
round[04].start  =+0xca7c4053bc037b8aac5c603f1a28285d
round[04].s_box  =+0x741009ed657b217e914ad075a234344c
round[04].s_row  =+0x747bd04c654a34ed9134097ea2102175
round[04].m_col  =+0xf9a560afcd406b10129c35693b946f26
round[04].k_sch  =+0xe75fad44bb095386485af05721efb14f
round[05].start  =+0x1efacdeb764938965ac6c53e1a7bde69
round[05].s_box  =+0x722dbde9383b0790beb4a6b2a2211df9
round[05].s_row  =+0x723ba6f938b41de9be21bd90a22d07b2
round[05].m_col  =+0xf60c0ee24385962829b0557e9d434ca8
round[05].k_sch  =+0xa448f6d94d6dce24aa326360113b30e6
round[06].start  =+0x5244f83b0ee8580c8382361e8c787c4e
round[06].s_box  =+0x001b41e2ab9b6afeec13057264bc102f
round[06].s_row  =+0x009b052fab1310e2ecbc41fe641b6a72
round[06].m_col  =+0x9c0de0c08a5fa53aa3b2cb35fd9e3d39
round[06].k_sch  =+0xa25e7ed583b1cf9a27f939436a94f767
round[07].start  =+0x3e539e1509ee6aa0844bf276970aca5e
round[07].s_box  =+0xb2ed0b59012802e05fb3893888677458
round[07].s_row  =+0xb228895801b374595f670be088ed0238
round[07].m_col  =+0xd63a7bdce1b9b176fc6c15561d77291c
round[07].k_sch  =+0xc0a69407d19da4e1ec1786eb6fa64971
round[08].start  =+0x169cefdb30241597107b93bd72d1606d
round[08].s_box  =+0x47dedfb904365988ca21dc7a403ed03c
round[08].s_row  =+0x4736dc3c0421d0b9ca3edf8840de597a
round[08].m_col  =+0x3468965b02944e949a44d2afda76a2b3
round[08].k_sch  =+0x485f703222cb8755e26d135233f0b7b3
round[09].start  =+0x7c37e669205fc9c17829c1fde9861500
round[09].s_box  =+0x109a8ef9b7cfdd78bca578541e445963
round[09].s_row  =+0x10cf7863b7a559f9bc448e781e9add54
round[09].m_col  =+0x717e8a4121f4b0d759c577e50019d9cd
round[09].k_sch  =+0x40beeb282f18a2596747d26b458c553e
round[10].start  =+0x31c061690eec128e3e82a58e45958cf3
round[10].s_box  =+0xc7baeff9abcec919b21306196e2a640d
round[10].s_row  =+0xc7ce060dab1364f9b22aef196ebac919
round[10].m_col  =+0xd7471280e5d86078f7d5763ad95876f3
round[10].k_sch  =+0xa7e1466c9411f1df821f750aad07d753
round[11].start  =+0x70a654ec71c991a775ca0330745fa1a0
round[11].s_box  =+0x512420cea3dd815c9d747b0492cf32e0
round[11].s_row  =+0x51dd7be0a37432ce9dcf205c92248104
round[11].m_col  =+0x459d418e3dd3fa3f1724f6ebd646a300
round[11].k_sch  =+0xca4005388fcc5006282d166abc3ce7b5
round[12].start  =+0x8fdd44b6b21faa393f09e0816a7a44b5
round[12].s_box  =+0x73c11b4e37c0ac127501e10c02da1bd5
round[12].s_row  =+0x73c0e1d537011b4e75da1b1202c1ac0c
round[12].k_sch  =+0xe98ba06f448c773c8ecc720401002202
round[12].output =+0x9a4b41ba738d6c72fb16691603c18e0e
# decryption
round[00].iinput =+0x9a4b41ba738d6c72fb16691603c18e0e
round[00].ik_sch =+0xe98ba06f448c773c8ecc720401002202
round[01].istart =+0x73c0e1d537011b4e75da1b1202c1ac0c
round[01].is_row =+0x73c11b4e37c0ac127501e10c02da1bd5
round[01].is_box =+0x8fdd44b6b21faa393f09e0816a7a44b5
round[01].ik_sch =+0xca4005388fcc5006282d166abc3ce7b5
round[01].ik_add =+0x459d418e3dd3fa3f1724f6ebd646a300
round[02].istart =+0x51dd7be0a37432ce9dcf205c92248104
round[02].is_row =+0x512420cea3dd815c9d747b0492cf32e0
round[02].is_box =+0x70a654ec71c991a775ca0330745fa1a0
round[02].ik_sch =+0xa7e1466c9411f1df821f750aad07d753
round[02].ik_add =+0xd7471280e5d86078f7d5763ad95876f3
round[03].istart =+0xc7ce060dab1364f9b22aef196ebac919
round[03].is_row =+0xc7baeff9abcec919b21306196e2a640d
round[03].is_box =+0x31c061690eec128e3e82a58e45958cf3
round[03].ik_sch =+0x40beeb282f18a2596747d26b458c553e
round[03].ik_add =+0x717e8a4121f4b0d759c577e50019d9cd
round[04].istart =+0x10cf7863b7a559f9bc448e781e9add54
round[04].is_row =+0x109a8ef9b7cfdd78bca578541e445963
round[04].is_box =+0x7c37e669205fc9c17829c1fde9861500
round[04].ik_sch =+0x485f703222cb8755e26d135233f0b7b3
round[04].ik_add =+0x3468965b02944e949a44d2afda76a2b3
round[05].istart =+0x4736dc3c0421d0b9ca3edf8840de597a
round[05].is_row =+0x47dedfb904365988ca21dc7a403ed03c
round[05].is_box =+0x169cefdb30241597107b93bd72d1606d
round[05].ik_sch =+0xc0a69407d19da4e1ec1786eb6fa64971
round[05].ik_add =+0xd63a7bdce1b9b176fc6c15561d77291c
round[06].istart =+0xb228895801b374595f670be088ed0238
round[06].is_row =+0xb2ed0b59012802e05fb3893888677458
round[06].is_box =+0x3e539e1509ee6aa0844bf276970aca5e
round[06].ik_sch =+0xa25e7ed583b1cf9a27f939436a94f767
round[06].ik_add =+0x9c0de0c08a5fa53aa3b2cb35fd9e3d39
round[07].istart =+0x009b052fab1310e2ecbc41fe641b6a72
round[07].is_row =+0x001b41e2ab9b6afeec13057264bc102f
round[07].is_box =+0x5244f83b0ee8580c8382361e8c787c4e
round[07].ik_sch =+0xa448f6d94d6dce24aa326360113b30e6
round[07].ik_add =+0xf60c0ee24385962829b0557e9d434ca8
round[08].istart =+0x723ba6f938b41de9be21bd90a22d07b2
round[08].is_row =+0x722dbde9383b0790beb4a6b2a2211df9
round[08].is_box =+0x1efacdeb764938965ac6c53e1a7bde69
round[08].ik_sch =+0xe75fad44bb095386485af05721efb14f
round[08].ik_add =+0xf9a560afcd406b10129c35693b946f26
round[09].istart =+0x747bd04c654a34ed9134097ea2102175
round[09].is_row =+0x741009ed657b217e914ad075a234344c
round[09].is_box =+0xca7c4053bc037b8aac5c603f1a28285d
round[09].ik_sch =+0x4db7b4bd69b5411885a74796e92538fd
round[09].ik_add =+0x87cbf4eed5b63a9229fb27a9f30d10a0
round[10].istart =+0x18f1c7784bec29452481926bbdbdc28c
round[10].is_row =+0x18bd92454bf1c26b24ecc78cbd812978
round[10].is_box =+0x34cd7468cc2ba805a68331f0cd914cc1
round[10].ik_sch =+0xec12068e6c827f6b0e7a95b95c56fec2
round[10].ik_add =+0xd8df72e6a0a9d76ea8f9a44991c7b203
round[11].istart =+0x2f1db81932233e9f119e7e4d2f8ed99f
round[11].is_row =+0x2f8e7e9f321dd94d1123b89f2f9e3e19
round[11].is_box =+0x4ee68a6ea1dee565e3329a6e4edfd18e
round[11].ik_sch =+0x62f8ead2522c6b7bfe0c91f72402f5a5
round[11].ik_add =+0x2c1e60bcf3f28e1e1d3e0b996add242b
round[12].istart =+0xbc8337e66be22f374db0226e33ce1653
round[12].is_row =+0xbcce22376b83166e4de2375333b02fe6
round[12].is_box =+0x78ec94b20541ff45653bb25066fc4ef5
round[12].ik_sch =+0x8e73b0f7da0e6452c810f32b809079e5
round[12].ioutput=+0xf69f2445df4f9b17ad2b417be66c3710
#--------------------------