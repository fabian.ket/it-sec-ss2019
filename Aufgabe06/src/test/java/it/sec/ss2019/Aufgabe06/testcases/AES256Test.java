package it.sec.ss2019.Aufgabe06.testcases;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import it.sec.ss2019.Aufgabe06.AES192;
import it.sec.ss2019.Aufgabe06.AES256;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@RunWith(JUnitPlatform.class)
public class AES256Test extends Lib4Tests {
	
    final private String FILENAME = "AES256-Tests.txt";
    final private String FILENAME_KEY = "AES256-Key-Tests.txt";

    private GetTests tests = null;
    
    @Test 
    public void testAES256() {

    	tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
        HashMap<String, List<String>> cur = null;
    	
    	int[] key = null;
    	int[] plain = null;
    	
    	String cypherRes = "";
    	String icypherRes = "";
    	String result = "";
    	
    	
    	AES256 aes = new AES256();
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	System.out.println(cur.get("t"));
        	
        	key		= aes.stringToHex(cur.get("k").get(0).substring(3));
			plain	= aes.stringToHex(cur.get("p").get(0).substring(3));
			
			cypherRes = cur.get("round[14].output").get(0);
			icypherRes = cur.get("round[14].ioutput").get(0);
			
			int[] encrypt = aes.encrypt(key, plain);
			result = "+0x" + aes.hexToString(encrypt);
			
			assertEquals(cypherRes, result, "AES 128 ENCRYTION Test" + " lineNo=" + cur.get("Line"));

			int[] decrypt = aes.decrypt(key, encrypt);
			result = "+0x" + aes.hexToString(decrypt);

        	assertEquals(icypherRes, result, "AES 128 DECRYPTION Test" + " lineNo=" + cur.get("Line"));

        }
        System.out.println("TEST FINISHED");
    	
    }
    
    @Test
    public void testAES256Key() {

    	tests = new GetTests(PACKAGENAME, DIRECTORY, FILENAME_KEY);
        Iterator<HashMap<String, List<String>>> ti = tests.iterator();
    	
        HashMap<String, List<String>> cur = null;
    	
    	int[] key = null;
    	int[] plain = null;
    	int[] tp = null;
    	
    	String cypherRes = "";
    	String icypherRes = "";
    	String result = "";
    	
    	
    	AES256 aes = new AES256();
    	
        while (ti.hasNext()) {
        	
        	cur = ti.next();
        	
        	System.out.println(cur.get("t"));
        	
        	key	= aes.stringToHex(cur.get("k").get(0).substring(3));
        	tp = aes.keyExpansion(key);        	
        	for(int i = 0; i <= 14; i++) {
        		key = aes.keyExpansionTest(tp, i);
        		result = "+0x" + aes.hexToString(key);
        		cypherRes = cur.get(String.format("key[%02d]", i)).get(0);
        		assertEquals(cypherRes, result, "AES 128 ENCRYTION KEY Test" + " lineNo=" + cur.get("Line").get(0));
        	}
        	
        	for(int i = 14; i >= 0; i--) {
        		key = aes.keyExpansionTest(tp, i);
        		result = "+0x" + aes.hexToString(key);
        		cypherRes = cur.get(String.format("key[%02d]", (14 - i))).get(1);
        		assertEquals(cypherRes, result, "AES 128 DECRYPTION KEY Test" + " lineNo=" + cur.get("Line").get(0));
        	}

        }
    	System.out.println("TEST FINISHED");
    	
    }
    
}
